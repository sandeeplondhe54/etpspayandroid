package com.smart.paywallet.adapter.mall;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.smart.paywallet.R;
import com.smart.paywallet.activity.mall.ProductsActivity;
import com.smart.paywallet.activity.wallet.GiftVoucherCategoryAll;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.models.output.GetBrandsOutput;
import com.smart.paywallet.models.output.GiftVoucherCategories;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class GiftVoucherCategoriesAdapter extends BaseAdapter {

    ArrayList<GiftVoucherCategories.Category> bean;
    AppCompatActivity main;


    public GiftVoucherCategoriesAdapter(AppCompatActivity activity, ArrayList<GiftVoucherCategories.Category> bean) {
        this.main = activity;
        this.bean = bean;
    }

    @Override
    public int getCount() {
        return bean.size();
    }

    @Override
    public Object getItem(int position) {
        return bean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        final ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) main.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.gift_voucher_category_list_item, null);


            viewHolder = new ViewHolder();

            viewHolder.cat_image = (CircleImageView) convertView.findViewById(R.id.cat_image);
            viewHolder.cat_title = (MyTextView) convertView.findViewById(R.id.cat_title);


            convertView.setTag(viewHolder);


        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }


        final GiftVoucherCategories.Category bean = (GiftVoucherCategories.Category) getItem(position);


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //    Toast.makeText(main, "item clicked" + bean.getId(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(main, GiftVoucherCategoryAll.class);

                i.putExtra("category_name", bean.getCategoryName());
                i.putExtra("category_id", bean.getCategoryId());

                main.startActivity(i);
            }
        });


        Picasso.with(main)
                .load(bean.getCategoryIcon())
                .placeholder(R.drawable.placeholder)
                .into(viewHolder.cat_image);

        viewHolder.cat_title.setText(bean.getCategoryName());

        return convertView;


    }

    private class ViewHolder {
        CircleImageView cat_image;
        MyTextView cat_title;


    }


}
