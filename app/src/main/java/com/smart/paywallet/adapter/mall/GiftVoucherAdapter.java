package com.smart.paywallet.adapter.mall;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.smart.paywallet.R;
import com.smart.paywallet.activity.mall.ProductsActivity;
import com.smart.paywallet.activity.wallet.GiftVoucherDetail;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.models.output.GetAllGiftVouchers;
import com.smart.paywallet.models.output.GetTopShoppingOffersModel;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GiftVoucherAdapter extends BaseAdapter {

    ArrayList<GetAllGiftVouchers.GiftVoucher> bean;
    AppCompatActivity main;


    public GiftVoucherAdapter(AppCompatActivity activity, ArrayList<GetAllGiftVouchers.GiftVoucher> bean) {
        this.main = activity;
        this.bean = bean;
    }

    @Override
    public int getCount() {
        return bean.size();
    }

    @Override
    public Object getItem(int position) {
        return bean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        final ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) main.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.gift_voucher_item, null);


            viewHolder = new ViewHolder();

            viewHolder.voucherImage = (ImageView) convertView.findViewById(R.id.voucherImage);
            viewHolder.voucherName = (MyTextView) convertView.findViewById(R.id.voucherName);


            convertView.setTag(viewHolder);


        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }


        final GetAllGiftVouchers.GiftVoucher bean = (GetAllGiftVouchers.GiftVoucher) getItem(position);

//        viewHolder.image.setImageResource(bean.getImage());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Toast.makeText(main, "item clicked" + bean.getId(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(main, GiftVoucherDetail.class);
                i.putExtra("voucher_name", bean.getVoucherName());
                i.putExtra("voucher_code", bean.getOperatorCode());
                i.putExtra("voucher_id", bean.getVoucherId());
                i.putExtra("voucher_image", bean.getVoucherImage());
                main.startActivity(i);

            }
        });


        Picasso.with(main)
                .load(Constants.GIFT_VOUCHER_IMAGE_PATH + bean.getVoucherImage())
                .placeholder(R.drawable.placeholder)
                .into(viewHolder.voucherImage);


        viewHolder.voucherName.setText(bean.getVoucherName());


        return convertView;


    }

    private class ViewHolder {
        ImageView voucherImage;
        MyTextView voucherName;


    }

}

