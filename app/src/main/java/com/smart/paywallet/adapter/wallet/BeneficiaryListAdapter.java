package com.smart.paywallet.adapter.wallet;


import android.app.Activity;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.activity.wallet.SendMoneyToBankAccount;
import com.smart.paywallet.models.output.GetWalletTransactionsOutput;
import com.smart.paywallet.models.output.MoneyTransferCustomerDetails;
import com.smart.paywallet.views.MyTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Sandeep Londhe on 18-02-2019.
 *
 * @Email :  sandeeplondhe54@gmail.com
 * @Author :  https://twitter.com/mesandeeplondhe
 * @Skype :  sandeeplondhe54
 */
public class BeneficiaryListAdapter extends RecyclerView.Adapter<BeneficiaryListAdapter.MyViewHolder> {

    ArrayList<MoneyTransferCustomerDetails.BeneficiaryList> beneficiaryListArrayList;
    Activity activity;

    public BeneficiaryListAdapter(ArrayList<MoneyTransferCustomerDetails.BeneficiaryList> beneficiaryListArrayList, Activity activity) {

        this.beneficiaryListArrayList = beneficiaryListArrayList;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyTextView beneficiary_name;
        public MyTextView beneficiary_mobile;
        public MyTextView bank_account_number;
        public MyTextView beneficiary_ifsc_code;
        public ImageView deleteBeneficiary;
        public LinearLayout llBnLayout;

        public MyViewHolder(View view) {
            super(view);

            beneficiary_name = (MyTextView) view.findViewById(R.id.beneficiary_name);
            beneficiary_mobile = (MyTextView) view.findViewById(R.id.beneficiary_mobile);
            bank_account_number = (MyTextView) view.findViewById(R.id.bank_account_number);
            beneficiary_ifsc_code = (MyTextView) view.findViewById(R.id.beneficiary_ifsc_code);
            deleteBeneficiary = (ImageView) view.findViewById(R.id.deleteBeneficiary);
            llBnLayout = (LinearLayout) view.findViewById(R.id.llBnLayout);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.beneficiary_list_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MoneyTransferCustomerDetails.BeneficiaryList beneficiaryList = beneficiaryListArrayList.get(position);


        holder.beneficiary_name.setText(beneficiaryList.getBeneficiaryName());
        holder.beneficiary_mobile.setText(beneficiaryList.getBeneficiaryMobileNumber());
        holder.bank_account_number.setText(beneficiaryList.getBeneficiaryAccountNumber());
        holder.beneficiary_ifsc_code.setText("IFSC Code:- " + beneficiaryList.getIfscCode());

        holder.deleteBeneficiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        holder.llBnLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strBeneficiaryId = beneficiaryList.getBeneficiaryId();

                System.out.println("strBeneficiaryId==" + strBeneficiaryId);
                Intent sendMoney = new Intent(activity, SendMoneyToBankAccount.class);
                sendMoney.putExtra("strBeneficiaryId", strBeneficiaryId);
                sendMoney.putExtra("strBeneficiaryName", beneficiaryList.getBeneficiaryName());
                sendMoney.putExtra("strBeneficiaryAccountNo", beneficiaryList.getBeneficiaryAccountNumber());
                sendMoney.putExtra("strIfscCode", beneficiaryList.getIfscCode());

                activity.startActivity(sendMoney);

            }
        });

    }


    @Override
    public int getItemCount() {
        return beneficiaryListArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
