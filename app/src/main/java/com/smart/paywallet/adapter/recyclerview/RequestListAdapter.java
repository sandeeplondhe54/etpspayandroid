package com.smart.paywallet.adapter.recyclerview;


import android.app.Activity;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.activity.wallet.RequestMoneyDetailActivity;
import com.smart.paywallet.models.output.GetWalletTransactionsOutput;
import com.smart.paywallet.models.output.RequestListOutput;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.smart.paywallet.utils.DataVaultManager.KEY_MOBILE;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;

/**
 * Created by Sandeep Londhe on 18-02-2019.
 *
 * @Email :  sandeeplondhe54@gmail.com
 * @Author :  https://twitter.com/mesandeeplondhe
 * @Skype :  sandeeplondhe54
 */
public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.MyViewHolder> {

    ArrayList<RequestListOutput.Request> requestArrayList;
    Activity activity;

    public RequestListAdapter(ArrayList<RequestListOutput.Request> requestArrayList, Activity activity) {

        this.requestArrayList = requestArrayList;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyTextView creditedAmount;
        public MyTextView creditedDate;
        public MyTextView walletHeading;
        public CircleImageView sentIcon;


        public MyViewHolder(View view) {
            super(view);

            creditedAmount = (MyTextView) view.findViewById(R.id.creditedAmount);
            creditedDate = (MyTextView) view.findViewById(R.id.creditedDate);
            walletHeading = (MyTextView) view.findViewById(R.id.walletHeading);
            sentIcon = (CircleImageView) view.findViewById(R.id.sentIcon);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_request_list_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final RequestListOutput.Request request = requestArrayList.get(position);
        String strCustomerMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        double value = Double.valueOf(request.getRequestedAmount());

        String strAmount = String.format("%.2f", value);

        if (request.getSenderMobile().equalsIgnoreCase(strCustomerMobile)) {
            holder.walletHeading.setText("Sent to " + request.getReceiverName());
            holder.creditedAmount.setText(SmartPayApplication.CURRENCY_SYMBOL + strAmount);
            holder.creditedAmount.setTextColor(activity.getColor(R.color.Black));
            holder.sentIcon.setImageResource(R.mipmap.ic_send_money);

        } else if (request.getReceiverMobile().equalsIgnoreCase(strCustomerMobile)) {
            holder.walletHeading.setText("Received from " + request.getSenderName());
            // holder.trans_status.setTextColor(activity.getColor(R.color.colorRedDark));
            // holder.walletHeading.setTextColor(activity.getColor(R.color.colorRedDark));
            holder.creditedAmount.setText("+" + SmartPayApplication.CURRENCY_SYMBOL + strAmount);
            holder.creditedAmount.setTextColor(activity.getColor(R.color.colorGreenLight));
            holder.sentIcon.setImageResource(R.mipmap.ic_recive_money);

        }

//            String selectedDate = getDate(transaction.getAddedDate());
//            String selectedMonth = getMonth(transaction.getAddedDate());

        holder.creditedDate.setText(request.getAddedDate());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detail = new Intent(activity, RequestMoneyDetailActivity.class);
                detail.putExtra("request_id", request.getRequestId());
                detail.putExtra("senderMobile", request.getSenderMobile());
                detail.putExtra("receiverMobile", request.getReceiverMobile());
                detail.putExtra("amount", request.getRequestedAmount());
                detail.putExtra("senderName", request.getSenderName());
                detail.putExtra("receiverName", request.getReceiverName());
                detail.putExtra("added_date", request.getAddedDate());
                activity.startActivity(detail);


            }
        });

    }


//    private static String getMonth(String date) throws ParseException {
//        Date d = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.ENGLISH).parse(date);
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(d);
//        String monthName = new SimpleDateFormat("MMM").format(cal.getTime());
//        return monthName;
//    }


//    private static String getDate(String date) throws ParseException {
//        Date d = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.ENGLISH).parse(date);
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(d);
//        String monthName = new SimpleDateFormat("dd").format(cal.getTime());
//        return monthName;
//    }


    @Override
    public int getItemCount() {
        return requestArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
