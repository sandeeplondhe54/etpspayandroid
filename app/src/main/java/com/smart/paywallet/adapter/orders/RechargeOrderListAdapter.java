package com.smart.paywallet.adapter.orders;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.smart.paywallet.R;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.models.output.RechargeOrderListOutput;
import com.smart.paywallet.views.MyTextView;
import com.smart.paywallet.views.MyTextViewBold;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RechargeOrderListAdapter extends RecyclerView.Adapter<RechargeOrderListAdapter.MyViewHolder> {


    public ArrayList<RechargeOrderListOutput.Recharge> rechargeOrderListOutputArrayList;
    public AppCompatActivity appCompatActivity;

    public RechargeOrderListAdapter(ArrayList<RechargeOrderListOutput.Recharge> rechargeOrderListOutputArrayList, AppCompatActivity appCompatActivity) {
        this.rechargeOrderListOutputArrayList = rechargeOrderListOutputArrayList;
        this.appCompatActivity = appCompatActivity;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public MyTextView orderDate, orderIsRepeat;

        public MyTextViewBold orderId, orderStatus, orderPrice;

        public ImageView orderImage;

        public MyViewHolder(View view) {
            super(view);
            orderId = (MyTextViewBold) view.findViewById(R.id.orderId);
            orderDate = (MyTextView) view.findViewById(R.id.orderDate);
            orderPrice = (MyTextViewBold) view.findViewById(R.id.orderPrice);
            orderStatus = (MyTextViewBold) view.findViewById(R.id.orderStatus);
            orderIsRepeat = (MyTextView) view.findViewById(R.id.orderIsRepeat);
            orderImage = (ImageView) view.findViewById(R.id.orderImage);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recharge_order_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        RechargeOrderListOutput.Recharge rechargeOrderListOutput = rechargeOrderListOutputArrayList.get(position);

        holder.orderId.setText("Order No " + rechargeOrderListOutput.getOrderId());
        holder.orderDate.setText(rechargeOrderListOutput.getAddedDate());
        Typeface typeFace_Rupee = Typeface.createFromAsset(appCompatActivity.getAssets(), "fonts/Rupee_Foradian.ttf");
        holder.orderPrice.setTypeface(typeFace_Rupee);

        holder.orderPrice.setText("`" + rechargeOrderListOutput.getAmount());

        if (rechargeOrderListOutput.getRechargeStatus().equalsIgnoreCase("SUCCESS")) {
            holder.orderStatus.setText("Your order is successful");

        } else {
            holder.orderStatus.setText("Your order is failed");
        }

        Picasso.with(appCompatActivity)
                .load(Constants.OPERATORS_IMAGE_PATH + rechargeOrderListOutput.getOperatorImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.orderImage);


    }

    @Override
    public int getItemCount() {
        return rechargeOrderListOutputArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
