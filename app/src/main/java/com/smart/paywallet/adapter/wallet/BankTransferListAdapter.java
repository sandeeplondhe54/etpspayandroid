package com.smart.paywallet.adapter.wallet;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.activity.wallet.SendMoneyToBankAccount;
import com.smart.paywallet.models.output.AllBankTransfers;
import com.smart.paywallet.models.output.MoneyTransferCustomerDetails;
import com.smart.paywallet.views.MyTextView;

import java.util.ArrayList;

public class BankTransferListAdapter extends RecyclerView.Adapter<BankTransferListAdapter.MyViewHolder> {

    ArrayList<AllBankTransfers.Transfer> transferArrayList;
    Activity activity;

    public BankTransferListAdapter(ArrayList<AllBankTransfers.Transfer> transferArrayList, Activity activity) {

        this.transferArrayList = transferArrayList;
        this.activity = activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyTextView trasnferTo;
        public MyTextView debitedAmount;
        public MyTextView trans_status;
        public MyTextView addedDate;


        public MyViewHolder(View view) {
            super(view);

            trasnferTo = (MyTextView) view.findViewById(R.id.trasnferTo);
            debitedAmount = (MyTextView) view.findViewById(R.id.debitedAmount);
            addedDate = (MyTextView) view.findViewById(R.id.addedDate);
            trans_status = (MyTextView) view.findViewById(R.id.trans_status);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bank_transfer_list_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final AllBankTransfers.Transfer transfer = transferArrayList.get(position);

        holder.trasnferTo.setText(transfer.getBeneficiaryName());
        holder.debitedAmount.setText(SmartPayApplication.CURRENCY_SYMBOL + transfer.getAmount());
        holder.addedDate.setText(transfer.getAddedDate());

        if (transfer.getStatus().equalsIgnoreCase("1")) {

            holder.trans_status.setText("Transfer Successfull");
            holder.trans_status.setTextColor(activity.getResources().getColor(R.color.colorGreenLight));
            holder.debitedAmount.setTextColor(activity.getResources().getColor(R.color.colorGreenLight));
        } else {
            holder.trans_status.setText("Transfer Failed");
            holder.trans_status.setTextColor(activity.getResources().getColor(R.color.colorRedDark));
            holder.debitedAmount.setTextColor(activity.getResources().getColor(R.color.colorRedDark));
        }

    }


    @Override
    public int getItemCount() {
        return transferArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}

