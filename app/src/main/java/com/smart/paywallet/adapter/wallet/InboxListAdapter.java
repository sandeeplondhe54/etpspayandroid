package com.smart.paywallet.adapter.wallet;

import android.content.Intent;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.smart.paywallet.R;
import com.smart.paywallet.activity.wallet.BlogDetailActivity;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.models.output.InboxBlogPost;
import com.smart.paywallet.models.output.InboxOutputModel;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

public class InboxListAdapter extends RecyclerView.Adapter<InboxListAdapter.MyViewHolder> {

    public ArrayList<InboxBlogPost.Post> inboxOutputModelArrayList;
    AppCompatActivity appCompatActivity;

    public InboxListAdapter(ArrayList<InboxBlogPost.Post> inboxOutputModelArrayList, AppCompatActivity appCompatActivity) {
        this.inboxOutputModelArrayList = inboxOutputModelArrayList;
        this.appCompatActivity = appCompatActivity;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyTextView inbox_title;
        public ImageView inbox_image;
        public TextView txt_article_desc;
        public TextView message_time;
        public LinearLayout ll_row_blog;

        public MyViewHolder(View view) {
            super(view);
            inbox_title = (MyTextView) view.findViewById(R.id.inbox_title);
            inbox_image = (ImageView) view.findViewById(R.id.inbox_image);
            txt_article_desc = (TextView) view.findViewById(R.id.txt_article_desc);
            message_time = (TextView) view.findViewById(R.id.message_time);
            ll_row_blog =(LinearLayout)view.findViewById(R.id.ll_row_blog);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.inbox_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        InboxBlogPost.Post inboxOutputModel = inboxOutputModelArrayList.get(position);
        holder.inbox_title.setText(inboxOutputModel.getTitle());
        try {
            Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(inboxOutputModel.getAddedDate().toString());
            holder.message_time.setText(date1.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // holder.txt_article_desc.setText(inboxOutputModel.getDescription());
        holder.txt_article_desc.setText(Html.fromHtml(inboxOutputModel.getDescription()),TextView.BufferType.SPANNABLE);
        Picasso.with(appCompatActivity)
                .load(Constants.UPLOAD_IMAGE_FOLDER + inboxOutputModel.getPostImage())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.inbox_image);
        holder.ll_row_blog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(appCompatActivity, BlogDetailActivity.class);
                appCompatActivity.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return inboxOutputModelArrayList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
