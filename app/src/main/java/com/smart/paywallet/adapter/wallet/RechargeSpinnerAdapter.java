package com.smart.paywallet.adapter.wallet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.smart.paywallet.R;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.models.output.GetAllOperators;
import com.smart.paywallet.models.output.RechargeOutput;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RechargeSpinnerAdapter extends ArrayAdapter<GetAllOperators.Operator> {

    private AppCompatActivity ctx;
    private ArrayList<GetAllOperators.Operator> operatorArrayList;


    public RechargeSpinnerAdapter(AppCompatActivity context, int textViewResourceId, ArrayList<GetAllOperators.Operator> operatorArrayList) {
        super(context, R.layout.recharge_spinner_layout, operatorArrayList);
        this.ctx = context;
        this.operatorArrayList = operatorArrayList;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        GetAllOperators.Operator operator = operatorArrayList.get(position);

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.recharge_spinner_layout, parent, false);

        MyTextView operator_name = (MyTextView) row.findViewById(R.id.operator_name);
        operator_name.setText(operator.getOperatorName());

        ImageView operator_image = (ImageView) row.findViewById(R.id.operator_image);

        Picasso.with(getContext())
                .load(Constants.OPERATORS_IMAGE_PATH + operator.getOperator_image())
                .placeholder(R.drawable.placeholder)
                .into(operator_image);


        return row;
    }
}
