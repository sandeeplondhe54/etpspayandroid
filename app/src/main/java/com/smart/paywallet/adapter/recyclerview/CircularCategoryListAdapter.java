package com.smart.paywallet.adapter.recyclerview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.smart.paywallet.R;
import com.smart.paywallet.adapter.mall.FrontCategoryListAdapter;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.models.output.CategoryListModel;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import java.util.List;


import de.hdodenhof.circleimageview.CircleImageView;

public class CircularCategoryListAdapter extends RecyclerView.Adapter<CircularCategoryListAdapter.MyViewHolder> {


    private List<CategoryListModel.Cat> deliveryResponseList;
    Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyTextView mall_cat_name2;
        public CircleImageView mall_cat_image;

        public MyViewHolder(View view) {
            super(view);
            mall_cat_name2 = (MyTextView) view.findViewById(R.id.mall_cat_name2);
            mall_cat_image = (CircleImageView) view.findViewById(R.id.mall_cat_image);

        }
    }


    public CircularCategoryListAdapter(List<CategoryListModel.Cat> deliveryResponseList, Activity activity) {
        this.deliveryResponseList = deliveryResponseList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mall_category_circle_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CategoryListModel.Cat deliveryResponse = deliveryResponseList.get(position);
        holder.mall_cat_name2.setText(deliveryResponse.getServiceName());

        Picasso.with(activity)
                .load(Constants.UPLOAD_IMAGE_FOLDER + deliveryResponse.getServiceImage())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.mall_cat_image);

    }

    @Override
    public int getItemCount() {
        return deliveryResponseList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

