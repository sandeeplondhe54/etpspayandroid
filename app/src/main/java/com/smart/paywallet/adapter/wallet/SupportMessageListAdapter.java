package com.smart.paywallet.adapter.wallet;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.smart.paywallet.R;
import com.smart.paywallet.models.output.SupportTicketMessagesList;
import com.smart.paywallet.views.MyTextView;

import java.util.ArrayList;

public class SupportMessageListAdapter extends RecyclerView.Adapter<SupportMessageListAdapter.MyViewHolder> {

    AppCompatActivity appCompatActivity;
    ArrayList<SupportTicketMessagesList.Ticket> ticketArrayList;


    public SupportMessageListAdapter(ArrayList<SupportTicketMessagesList.Ticket> ticketArrayList, AppCompatActivity appCompatActivity) {
        this.ticketArrayList = ticketArrayList;
        this.appCompatActivity = appCompatActivity;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public MyTextView sender_name;
        public MyTextView message_text;
        public MyTextView date_message;
        public LinearLayout bubbleBg;


        public MyViewHolder(View view) {
            super(view);

            sender_name = (MyTextView) view.findViewById(R.id.sender_name);
            message_text = (MyTextView) view.findViewById(R.id.message_text);
            date_message = (MyTextView) view.findViewById(R.id.date_message);
            bubbleBg = (LinearLayout) view.findViewById(R.id.bubbleBg);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.support_message_list_item, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final SupportTicketMessagesList.Ticket ticket = ticketArrayList.get(position);

        if (ticket.getAdminId().equalsIgnoreCase("0")) {

            holder.sender_name.setText(ticket.getCustomerName());

            holder.bubbleBg.setBackgroundResource(R.drawable.bubble_left_rect);
            holder.bubbleBg.setGravity(Gravity.LEFT);


        } else if (ticket.getAdminId().equalsIgnoreCase("1")) {
            holder.sender_name.setText("Admin");
            holder.bubbleBg.setBackgroundResource(R.drawable.bubble_right_rect);
            holder.bubbleBg.setGravity(Gravity.RIGHT);

        }

        holder.message_text.setText(ticket.getMessageText());

        holder.date_message.setText(ticket.getAddedDate());

    }

    @Override
    public int getItemCount() {
        return ticketArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
