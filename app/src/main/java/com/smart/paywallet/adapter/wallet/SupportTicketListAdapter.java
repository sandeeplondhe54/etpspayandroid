package com.smart.paywallet.adapter.wallet;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.smart.paywallet.R;
import com.smart.paywallet.activity.wallet.SupportTicketDetails;
import com.smart.paywallet.models.output.SupportTicketListOutput;
import com.smart.paywallet.views.MyTextView;

import java.util.ArrayList;

public class SupportTicketListAdapter extends RecyclerView.Adapter<SupportTicketListAdapter.MyViewHolder> {

    public ArrayList<SupportTicketListOutput.Ticket> ticketArrayList;
    public AppCompatActivity appCompatActivity;

    public SupportTicketListAdapter(ArrayList<SupportTicketListOutput.Ticket> ticketArrayList, AppCompatActivity appCompatActivity) {
        this.ticketArrayList = ticketArrayList;
        this.appCompatActivity = appCompatActivity;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public MyTextView txtSubject;
        public MyTextView txtTicketId;
        public MyTextView txtAddedDate;
        public MyTextView txtStatus;

        public MyViewHolder(View view) {
            super(view);

            txtSubject = (MyTextView) view.findViewById(R.id.txtSubject);
            txtTicketId = (MyTextView) view.findViewById(R.id.txtTicketId);
            txtAddedDate = (MyTextView) view.findViewById(R.id.txtAddedDate);
            txtStatus = (MyTextView) view.findViewById(R.id.txtStatus);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.support_ticket_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder viewHolder, final int position) {

        final SupportTicketListOutput.Ticket ticket = ticketArrayList.get(position);

        viewHolder.txtSubject.setText(ticket.getSubject());
        viewHolder.txtTicketId.setText("TICKET ID:- " + ticket.getTicketId());
        viewHolder.txtAddedDate.setText(ticket.getSubmittedDate());

        if (ticket.getStatus().equalsIgnoreCase("0")) {

            viewHolder.txtStatus.setText("PENDING");
            viewHolder.txtStatus.setBackgroundResource(R.drawable.red_rounded_bg);
        } else if (ticket.getStatus().equalsIgnoreCase("1")) {

            viewHolder.txtStatus.setText("SOLVED");
            viewHolder.txtStatus.setBackgroundResource(R.drawable.blue_rounded_bg);

        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent newIntent = new Intent(appCompatActivity, SupportTicketDetails.class);
                newIntent.putExtra("ticket_id", ticket.getTicketId());
                appCompatActivity.startActivity(newIntent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return ticketArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}


