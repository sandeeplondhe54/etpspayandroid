package com.smart.paywallet.adapter.grids;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.appcompat.app.AppCompatActivity;

import com.smart.paywallet.R;
import com.smart.paywallet.adapter.mall.GiftVoucherCategoriesAdapter;
import com.smart.paywallet.models.output.GetAllOperators;
import com.smart.paywallet.models.output.GiftVoucherCategories;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.smart.paywallet.R;
import com.smart.paywallet.activity.mall.ProductsActivity;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.models.output.GetBrandsOutput;
import com.smart.paywallet.models.output.GiftVoucherCategories;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class OperatorsGridViewAdapter extends BaseAdapter {

    ArrayList<GetAllOperators.Operator> bean;
    AppCompatActivity main;


    public OperatorsGridViewAdapter(AppCompatActivity activity, ArrayList<GetAllOperators.Operator> bean) {
        this.main = activity;
        this.bean = bean;
    }

    @Override
    public int getCount() {
        return bean.size();
    }

    @Override
    public Object getItem(int position) {
        return bean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        final ViewHolder viewHolder;

        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) main.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.operator_grid_list_item, null);


            viewHolder = new ViewHolder();

            viewHolder.operator_grid_image = (ImageView) convertView.findViewById(R.id.operator_grid_image);
            viewHolder.operator_grid_title = (MyTextView) convertView.findViewById(R.id.operator_grid_title);


            convertView.setTag(viewHolder);


        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }


        final GetAllOperators.Operator bean = (GetAllOperators.Operator) getItem(position);


//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                //       Toast.makeText(main, "item clicked" + bean.getId(), Toast.LENGTH_SHORT).show();
////                Intent i = new Intent(main, ProductsActivity.class);
////
////                i.putExtra("subcat_id", "null");
////                i.putExtra("subcat_name", bean.getBrandName());
////                i.putExtra("brand_id", bean.getBrandId());
////
////                main.startActivity(i);
//
//            }
//        });


        Picasso.with(main)
                .load(Constants.OPERATORS_IMAGE_PATH + bean.getOperator_image())
                .placeholder(R.drawable.placeholder)
                .into(viewHolder.operator_grid_image);

        viewHolder.operator_grid_title.setText(bean.getOperatorName());

        return convertView;


    }

    private class ViewHolder {
        ImageView operator_grid_image;
        MyTextView operator_grid_title;


    }


}

