package com.smart.paywallet.models.output;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GiftVoucherOrders {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("orders")
    @Expose
    private ArrayList<Order> orders = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }


    public class Order {

        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("reachapi_order_id")
        @Expose
        private String reachapiOrderId;
        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("operator_id")
        @Expose
        private String operatorId;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("urid")
        @Expose
        private String urid;
        @SerializedName("sender_name")
        @Expose
        private String senderName;
        @SerializedName("receiver_name")
        @Expose
        private String receiverName;
        @SerializedName("receiver_email")
        @Expose
        private String receiverEmail;
        @SerializedName("sender_mobile")
        @Expose
        private String senderMobile;
        @SerializedName("receiver_mobile")
        @Expose
        private String receiverMobile;
        @SerializedName("gift_message")
        @Expose
        private String giftMessage;
        @SerializedName("wallet_id")
        @Expose
        private String walletId;
        @SerializedName("recharge_status")
        @Expose
        private String rechargeStatus;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("voucher_id")
        @Expose
        private String voucher_id;
        @SerializedName("voucher_name")
        @Expose
        private String voucher_name;
        @SerializedName("operator_code")
        @Expose
        private String operator_code;
        @SerializedName("instructions")
        @Expose
        private String instructions;
        @SerializedName("voucher_image")
        @Expose
        private String voucher_image;
        @SerializedName("category_id")
        @Expose
        private String category_id;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getReachapiOrderId() {
            return reachapiOrderId;
        }

        public void setReachapiOrderId(String reachapiOrderId) {
            this.reachapiOrderId = reachapiOrderId;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(String operatorId) {
            this.operatorId = operatorId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getUrid() {
            return urid;
        }

        public void setUrid(String urid) {
            this.urid = urid;
        }

        public String getSenderName() {
            return senderName;
        }

        public void setSenderName(String senderName) {
            this.senderName = senderName;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public String getReceiverEmail() {
            return receiverEmail;
        }

        public void setReceiverEmail(String receiverEmail) {
            this.receiverEmail = receiverEmail;
        }

        public String getSenderMobile() {
            return senderMobile;
        }

        public void setSenderMobile(String senderMobile) {
            this.senderMobile = senderMobile;
        }

        public String getReceiverMobile() {
            return receiverMobile;
        }

        public void setReceiverMobile(String receiverMobile) {
            this.receiverMobile = receiverMobile;
        }

        public String getGiftMessage() {
            return giftMessage;
        }

        public void setGiftMessage(String giftMessage) {
            this.giftMessage = giftMessage;
        }

        public String getWalletId() {
            return walletId;
        }

        public void setWalletId(String walletId) {
            this.walletId = walletId;
        }

        public String getRechargeStatus() {
            return rechargeStatus;
        }

        public void setRechargeStatus(String rechargeStatus) {
            this.rechargeStatus = rechargeStatus;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getVoucher_id() {
            return voucher_id;
        }

        public void setVoucher_id(String voucher_id) {
            this.voucher_id = voucher_id;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getInstructions() {
            return instructions;
        }

        public void setInstructions(String instructions) {
            this.instructions = instructions;
        }

        public String getOperator_code() {
            return operator_code;
        }

        public void setOperator_code(String operator_code) {
            this.operator_code = operator_code;
        }

        public String getVoucher_image() {
            return voucher_image;
        }

        public void setVoucher_image(String voucher_image) {
            this.voucher_image = voucher_image;
        }

        public String getVoucher_name() {
            return voucher_name;
        }

        public void setVoucher_name(String voucher_name) {
            this.voucher_name = voucher_name;
        }

    }
}
