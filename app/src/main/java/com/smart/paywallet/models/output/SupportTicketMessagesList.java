package com.smart.paywallet.models.output;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SupportTicketMessagesList {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ticket")
    @Expose
    private ArrayList<Ticket> ticket = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Ticket> getTicket() {
        return ticket;
    }

    public void setTicket(ArrayList<Ticket> ticket) {
        this.ticket = ticket;
    }


    public class Ticket {

        @SerializedName("message_id")
        @Expose
        private String messageId;
        @SerializedName("ticket_id")
        @Expose
        private String ticketId;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("customer_name")
        @Expose
        private String customerName;
        @SerializedName("message_text")
        @Expose
        private String messageText;
        @SerializedName("message_attachment")
        @Expose
        private String messageAttachment;
        @SerializedName("admin_id")
        @Expose
        private String adminId;

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(String messageId) {
            this.messageId = messageId;
        }

        public String getTicketId() {
            return ticketId;
        }

        public void setTicketId(String ticketId) {
            this.ticketId = ticketId;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getMessageText() {
            return messageText;
        }

        public void setMessageText(String messageText) {
            this.messageText = messageText;
        }

        public String getMessageAttachment() {
            return messageAttachment;
        }

        public void setMessageAttachment(String messageAttachment) {
            this.messageAttachment = messageAttachment;
        }

        public String getAdminId() {
            return adminId;
        }

        public void setAdminId(String adminId) {
            this.adminId = adminId;
        }


    }
}