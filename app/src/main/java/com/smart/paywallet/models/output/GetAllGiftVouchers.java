package com.smart.paywallet.models.output;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAllGiftVouchers {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("vouchers")
    @Expose
    private ArrayList<GiftVoucher> vouchers = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<GiftVoucher> getVouchers() {
        return vouchers;
    }

    public void setVouchers(ArrayList<GiftVoucher> vouchers) {
        this.vouchers = vouchers;
    }


    public class GiftVoucher {

        @SerializedName("voucher_id")
        @Expose
        private String voucherId;
        @SerializedName("voucher_name")
        @Expose
        private String voucherName;
        @SerializedName("operator_code")
        @Expose
        private String operatorCode;
        @SerializedName("instructions")
        @Expose
        private String instructions;
        @SerializedName("voucher_image")
        @Expose
        private String voucherImage;

        public String getVoucherId() {
            return voucherId;
        }

        public void setVoucherId(String voucherId) {
            this.voucherId = voucherId;
        }

        public String getVoucherName() {
            return voucherName;
        }

        public void setVoucherName(String voucherName) {
            this.voucherName = voucherName;
        }

        public String getOperatorCode() {
            return operatorCode;
        }

        public void setOperatorCode(String operatorCode) {
            this.operatorCode = operatorCode;
        }

        public String getInstructions() {
            return instructions;
        }

        public void setInstructions(String instructions) {
            this.instructions = instructions;
        }

        public String getVoucherImage() {
            return voucherImage;
        }

        public void setVoucherImage(String voucherImage) {
            this.voucherImage = voucherImage;
        }

    }
}
