package com.smart.paywallet.models.output;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RechargeOrderListOutput {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("recharges")
    @Expose
    private ArrayList<Recharge> recharges = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Recharge> getRecharges() {
        return recharges;
    }

    public void setRecharges(ArrayList<Recharge> recharges) {
        this.recharges = recharges;
    }


    public class Recharge {

        @SerializedName("recharge_id")
        @Expose
        private String rechargeId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("recharge_number")
        @Expose
        private String rechargeNumber;
        @SerializedName("recharge_type")
        @Expose
        private String rechargeType;
        @SerializedName("operator_code")
        @Expose
        private Object operatorCode;
        @SerializedName("operator_name")
        @Expose
        private Object operatorName;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("plan_id")
        @Expose
        private String planId;
        @SerializedName("payment_type")
        @Expose
        private String paymentType;
        @SerializedName("payment_status")
        @Expose
        private String paymentStatus;
        @SerializedName("recharge_status")
        @Expose
        private String rechargeStatus;
        @SerializedName("added_date")
        @Expose
        private String addedDate;
        @SerializedName("id")
        @Expose
        private Object id;
        @SerializedName("operator_image")
        @Expose
        private Object operatorImage;
        @SerializedName("instructions")
        @Expose
        private Object instructions;
        @SerializedName("state")
        @Expose
        private Object state;
        @SerializedName("type")
        @Expose
        private Object type;

        public String getRechargeId() {
            return rechargeId;
        }

        public void setRechargeId(String rechargeId) {
            this.rechargeId = rechargeId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getRechargeNumber() {
            return rechargeNumber;
        }

        public void setRechargeNumber(String rechargeNumber) {
            this.rechargeNumber = rechargeNumber;
        }

        public String getRechargeType() {
            return rechargeType;
        }

        public void setRechargeType(String rechargeType) {
            this.rechargeType = rechargeType;
        }

        public Object getOperatorCode() {
            return operatorCode;
        }

        public void setOperatorCode(Object operatorCode) {
            this.operatorCode = operatorCode;
        }

        public Object getOperatorName() {
            return operatorName;
        }

        public void setOperatorName(Object operatorName) {
            this.operatorName = operatorName;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getPlanId() {
            return planId;
        }

        public void setPlanId(String planId) {
            this.planId = planId;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getRechargeStatus() {
            return rechargeStatus;
        }

        public void setRechargeStatus(String rechargeStatus) {
            this.rechargeStatus = rechargeStatus;
        }

        public String getAddedDate() {
            return addedDate;
        }

        public void setAddedDate(String addedDate) {
            this.addedDate = addedDate;
        }

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public Object getOperatorImage() {
            return operatorImage;
        }

        public void setOperatorImage(Object operatorImage) {
            this.operatorImage = operatorImage;
        }

        public Object getInstructions() {
            return instructions;
        }

        public void setInstructions(Object instructions) {
            this.instructions = instructions;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }

        public Object getType() {
            return type;
        }

        public void setType(Object type) {
            this.type = type;
        }

    }

}
