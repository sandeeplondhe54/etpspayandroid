package com.smart.paywallet.models.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AllPlans {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private ArrayList<Plans> data = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Plans> getData() {
        return data;
    }

    public void setData(ArrayList<Plans> data) {
        this.data = data;
    }


    public class Plans {

        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("detail")
        @Expose
        private String detail;
        @SerializedName("validity")
        @Expose
        private String validity;
        @SerializedName("talktime")
        @Expose
        private String talktime;

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getValidity() {
            return validity;
        }

        public void setValidity(String validity) {
            this.validity = validity;
        }

        public String getTalktime() {
            return talktime;
        }

        public void setTalktime(String talktime) {
            this.talktime = talktime;
        }

    }


}
