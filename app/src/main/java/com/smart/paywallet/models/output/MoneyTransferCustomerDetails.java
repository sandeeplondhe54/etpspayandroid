package com.smart.paywallet.models.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class MoneyTransferCustomerDetails {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("details")
    @Expose
    private Details details;
    @SerializedName("beneficiaryList")
    @Expose
    private ArrayList<BeneficiaryList> beneficiaryList = null;
    @SerializedName("message")
    @Expose
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public ArrayList<BeneficiaryList> getBeneficiaryList() {
        return beneficiaryList;
    }

    public void setBeneficiaryList(ArrayList<BeneficiaryList> beneficiaryList) {
        this.beneficiaryList = beneficiaryList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public class BeneficiaryList {

        @SerializedName("beneficiaryName")
        @Expose
        private String beneficiaryName;
        @SerializedName("beneficiaryMobileNumber")
        @Expose
        private String beneficiaryMobileNumber;
        @SerializedName("beneficiaryAccountNumber")
        @Expose
        private String beneficiaryAccountNumber;
        @SerializedName("ifscCode")
        @Expose
        private String ifscCode;
        @SerializedName("beneficiaryId")
        @Expose
        private String beneficiaryId;

        public String getBeneficiaryName() {
            return beneficiaryName;
        }

        public void setBeneficiaryName(String beneficiaryName) {
            this.beneficiaryName = beneficiaryName;
        }

        public String getBeneficiaryMobileNumber() {
            return beneficiaryMobileNumber;
        }

        public void setBeneficiaryMobileNumber(String beneficiaryMobileNumber) {
            this.beneficiaryMobileNumber = beneficiaryMobileNumber;
        }

        public String getBeneficiaryAccountNumber() {
            return beneficiaryAccountNumber;
        }

        public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
            this.beneficiaryAccountNumber = beneficiaryAccountNumber;
        }

        public String getIfscCode() {
            return ifscCode;
        }

        public void setIfscCode(String ifscCode) {
            this.ifscCode = ifscCode;
        }

        public String getBeneficiaryId() {
            return beneficiaryId;
        }

        public void setBeneficiaryId(String beneficiaryId) {
            this.beneficiaryId = beneficiaryId;
        }

    }

    public class Details {

        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("otpVerified")
        @Expose
        private Integer otpVerified;
        @SerializedName("limit")
        @Expose
        private String limit;
        @SerializedName("balance")
        @Expose
        private Integer balance;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getOtpVerified() {
            return otpVerified;
        }

        public void setOtpVerified(Integer otpVerified) {
            this.otpVerified = otpVerified;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public Integer getBalance() {
            return balance;
        }

        public void setBalance(Integer balance) {
            this.balance = balance;
        }

    }
}
