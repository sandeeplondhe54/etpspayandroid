package com.smart.paywallet.models.output;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AddBeneficiaryOutput {


    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("beneficiaryId")
    @Expose
    private Integer beneficiaryId;
    @SerializedName("message")
    @Expose
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Integer getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(Integer beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
