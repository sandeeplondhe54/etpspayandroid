package com.smart.paywallet.fragments.orders;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.mall.GiftVoucherAdapter;
import com.smart.paywallet.adapter.orders.RechargeOrderListAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.GetAllGiftVouchers;
import com.smart.paywallet.models.output.RechargeOrderListOutput;
import com.smart.paywallet.utils.DataVaultManager;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;

public class FragmentRechargeOrders extends Fragment {

    ArrayList<RechargeOrderListOutput.Recharge> rechargeOrderListOutputArrayList;
    RechargeOrderListAdapter rechargeOrderListAdapter;


    View mView;

    RecyclerView rechargeOrdersList;

    ProgressBar rechargeOrderProgressBar;

    MainAPIInterface mainAPIInterface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.recharge_orders_fragment, container, false);

        mainAPIInterface = ApiUtils.getAPIService();

        rechargeOrdersList = (RecyclerView) mView.findViewById(R.id.rechargeOrdersList);
        rechargeOrderProgressBar = (ProgressBar) mView.findViewById(R.id.rechargeOrderProgressBar);
        getAllRechargeRequest();
        return mView;
    }

    private void getAllRechargeRequest() {

        String xAccessToken = "mykey";

        rechargeOrderProgressBar.setVisibility(View.VISIBLE);
        rechargeOrdersList.setVisibility(View.GONE);

        String strUserId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);


        MultipartBody.Part user_id_body = MultipartBody.Part.createFormData("user_id", strUserId);


        mainAPIInterface.getAllUserRecharges(xAccessToken, user_id_body).enqueue(new Callback<RechargeOrderListOutput>() {
            @Override
            public void onResponse(Call<RechargeOrderListOutput> call, Response<RechargeOrderListOutput> response) {


                if (response.isSuccessful()) {

                    rechargeOrderProgressBar.setVisibility(View.GONE);
                    rechargeOrdersList.setVisibility(View.VISIBLE);

                    rechargeOrderListOutputArrayList = response.body().getRecharges();


                    rechargeOrderListAdapter = new RechargeOrderListAdapter(rechargeOrderListOutputArrayList, (AppCompatActivity) getActivity());


                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);

                    rechargeOrdersList.setLayoutManager(layoutManager);

                    rechargeOrdersList.setItemAnimator(new DefaultItemAnimator());
                    rechargeOrdersList.setAdapter(rechargeOrderListAdapter);

                    rechargeOrderListAdapter.notifyDataSetChanged();


                }
            }

            @Override
            public void onFailure(Call<RechargeOrderListOutput> call, Throwable t) {
                rechargeOrderProgressBar.setVisibility(View.GONE);
                rechargeOrdersList.setVisibility(View.VISIBLE);
                Log.i("tag", t.getMessage().toString());
            }
        });


    }
}
