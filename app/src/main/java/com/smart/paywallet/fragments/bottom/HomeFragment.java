package com.smart.paywallet.fragments.bottom;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.activity.mall.CategoryDetailActivity;
import com.smart.paywallet.activity.mall.HelplineActivity;
import com.smart.paywallet.activity.wallet.AddMoneyToWallet;
import com.smart.paywallet.activity.wallet.BroadbandRechargeActivity;
import com.smart.paywallet.activity.wallet.DTHRechargeActivity;
import com.smart.paywallet.activity.wallet.ElectricityBillPaymentActivity;
import com.smart.paywallet.activity.wallet.GasBillPaymentActivity;
import com.smart.paywallet.activity.wallet.GiftVoucherAll;
import com.smart.paywallet.activity.wallet.HotelSearchActivity;
import com.smart.paywallet.activity.wallet.InsuranceBillPaymentActivity;
import com.smart.paywallet.activity.wallet.LandlineBillPayment;
import com.smart.paywallet.activity.wallet.MetroCardRechargeActivity;
import com.smart.paywallet.activity.wallet.MobileRechargeActivity;
import com.smart.paywallet.activity.wallet.MovieTicketsActivity;
import com.smart.paywallet.activity.wallet.PayNowActivity;
import com.smart.paywallet.activity.wallet.PaymentsActivity;
import com.smart.paywallet.activity.wallet.RequestMoneyToWallet;
import com.smart.paywallet.activity.wallet.TravelActivity;
import com.smart.paywallet.activity.wallet.WaterBillPaymentActivity;
import com.smart.paywallet.adapter.mall.FrontCategoryListAdapter;
import com.smart.paywallet.adapter.mall.GiftVoucherAdapter;
import com.smart.paywallet.adapter.mall.TopShoppingOfferAdapter;
import com.smart.paywallet.adapter.recyclerview.CircularCategoryListAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.CategoryListModel;
import com.smart.paywallet.models.output.GetAllGiftVouchers;
import com.smart.paywallet.models.output.GetTopShoppingOffersModel;
import com.smart.paywallet.utils.RecyclerTouchListener;
import com.smart.paywallet.views.ChildAnimationExample;
import com.smart.paywallet.views.ExpandableHeightGridView;
import com.smart.paywallet.views.MyTextView;
import com.smart.paywallet.views.SliderLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    SliderLayout slider_home, slider_home2;

    PagerIndicator custom_indicator_home, custom_indicator_home2;

    View mView;

    HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
    HashMap<String, Integer> file_maps2 = new HashMap<String, Integer>();

    LinearLayout llRecharge, llElectricity, llDth, llMetro,
            llInsurance, llGas, llWater, llBroadband, llLandline, llTrainTicket, llFlightTicket, llBus, llMovieTicket, llHotels;
    LinearLayout llPayments, llPayNow, llAddMoney, llRequestMoney;

    ExpandableHeightGridView topShoppingOffersGridView;

    ExpandableHeightGridView brandVoucherGridView;
    ProgressBar brandVoucherProgressBar;

    GiftVoucherAdapter giftVoucherAdapter;

    TopShoppingOfferAdapter topShoppingOfferAdapter;
    ProgressBar topOfferProgressBar;

    MainAPIInterface mainAPIInterface;

    ArrayList<GetTopShoppingOffersModel.Offer> offerArrayList;
    ArrayList<GetAllGiftVouchers.GiftVoucher> giftVoucherArrayList;

    MyTextView btnViewAllVouchers;
    TextView txtMessageUs, txtHelpCenter;

    CircularCategoryListAdapter circularCategoryListAdapter;
    RecyclerView categoryList;
    ProgressBar categoriesProgressBar1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.home_fragment, container, false);

        mainAPIInterface = ApiUtils.getAPIService();

        slider_home = mView.findViewById(R.id.slider_home);
        custom_indicator_home = mView.findViewById(R.id.custom_indicator_home);

        slider_home2 = mView.findViewById(R.id.slider_home2);
        custom_indicator_home2 = mView.findViewById(R.id.custom_indicator_home2);

        llRecharge = mView.findViewById(R.id.llRecharge);
        llElectricity = mView.findViewById(R.id.llElectricity);
        llDth = mView.findViewById(R.id.llDth);
        llBroadband = mView.findViewById(R.id.llBroadband);
        llLandline = mView.findViewById(R.id.llLandline);
        llInsurance = mView.findViewById(R.id.llInsurance);
        llGas = mView.findViewById(R.id.llGas);
        llWater = mView.findViewById(R.id.llWater);
        llMetro = mView.findViewById(R.id.llMetro);
        llPayments = mView.findViewById(R.id.llPayments);
        llPayNow = mView.findViewById(R.id.llPayNow);
        llAddMoney = mView.findViewById(R.id.llAddMoney);
        llRequestMoney = mView.findViewById(R.id.llRequestMoney);

        llTrainTicket = mView.findViewById(R.id.llTrainTicket);
        llFlightTicket = mView.findViewById(R.id.llFlightTicket);
        llBus = mView.findViewById(R.id.llBus);
        llMovieTicket = mView.findViewById(R.id.llMovieTicket);
        llHotels = mView.findViewById(R.id.llHotels);
        categoryList = mView.findViewById(R.id.categoryList);
        categoriesProgressBar1 = mView.findViewById(R.id.categoriesProgressBar1);


        topShoppingOffersGridView = (ExpandableHeightGridView) mView.findViewById(R.id.topShoppingOffersGridView);
        topOfferProgressBar = (ProgressBar) mView.findViewById(R.id.topOfferProgressBar);
        brandVoucherGridView = (ExpandableHeightGridView) mView.findViewById(R.id.brandVoucherGridView);
        brandVoucherProgressBar = (ProgressBar) mView.findViewById(R.id.brandVoucherProgressBar);
        btnViewAllVouchers = (MyTextView) mView.findViewById(R.id.btnViewAllVouchers);

        txtMessageUs = (TextView) mView.findViewById(R.id.txtMessageUs);
        txtHelpCenter = (TextView) mView.findViewById(R.id.txtHelpCenter);


        file_maps.put("Sale banner 1", R.drawable.small_banner1);
        file_maps.put("Sale banner 2", R.drawable.small_banner1);
        file_maps.put("Sale banner 3", R.drawable.small_banner1);

        file_maps2.put("Sale banner 1", R.mipmap.bb1);
        file_maps2.put("Sale banner 2", R.mipmap.bb2);
        file_maps2.put("Sale banner 3", R.mipmap.bb3);

        txtMessageUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, SmartPayApplication.APP_SUPPORT_EMAIL);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Customer Inquiry");

                emailIntent.setType("message/rfc822");

                try {
                    startActivity(Intent.createChooser(emailIntent,
                            "Send email using..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "No email clients installed.",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });


        txtHelpCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intentHelpine = new Intent(getActivity(), HelplineActivity.class);
                startActivity(intentHelpine);


            }
        });


        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {


                        }
                    });

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);

            slider_home.addSlider(textSliderView);

            textSliderView.bundle(new Bundle());

            slider_home.addSlider(textSliderView);


        }

        slider_home.setPresetTransformer(SliderLayout.Transformer.Default);
        slider_home.setCustomIndicator(custom_indicator_home);
//                            mImageSlider.setCurrentPosition(-1);
        //     mImageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider_home.setPadding(0, 0, 0, 5);
        slider_home.setCustomAnimation(new ChildAnimationExample());


        //for slider 2


        for (String name : file_maps2.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps2.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {


                        }
                    });

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);

            slider_home2.addSlider(textSliderView);

            textSliderView.bundle(new Bundle());

            slider_home2.addSlider(textSliderView);

        }

        slider_home2.setPresetTransformer(SliderLayout.Transformer.Default);
        slider_home2.setCustomIndicator(custom_indicator_home2);
//                            mImageSlider.setCurrentPosition(-1);
        //     mImageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider_home2.setPadding(0, 0, 0, 5);
        slider_home2.setCustomAnimation(new ChildAnimationExample());


        llRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), MobileRechargeActivity.class);
                startActivity(intent);

            }
        });


        llElectricity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), ElectricityBillPaymentActivity.class);
                startActivity(intent);

            }
        });

        llDth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), DTHRechargeActivity.class);
                startActivity(intent);

            }
        });

        llBroadband.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), BroadbandRechargeActivity.class);
                startActivity(intent);
            }
        });

        llLandline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LandlineBillPayment.class);
                startActivity(intent);

            }
        });

        llInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), InsuranceBillPaymentActivity.class);
                startActivity(intent);

            }
        });

        llGas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GasBillPaymentActivity.class);
                startActivity(intent);

            }
        });

        llWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), WaterBillPaymentActivity.class);
                startActivity(intent);

            }
        });

        llMetro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MetroCardRechargeActivity.class);
                startActivity(intent);

            }
        });

        llPayments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), PaymentsActivity.class);
                startActivity(intent);

            }
        });

        llTrainTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), TravelActivity.class);
                intent.putExtra("travel", "train");
                startActivity(intent);

            }
        });

        llFlightTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), TravelActivity.class);
                intent.putExtra("travel", "flight");
                startActivity(intent);

            }
        });

        llBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), TravelActivity.class);
                intent.putExtra("travel", "bus");
                startActivity(intent);

            }
        });


        llMovieTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), MovieTicketsActivity.class);
                startActivity(intent);

            }
        });

        llHotels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), HotelSearchActivity.class);
                startActivity(intent);

            }
        });

        llPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), PayNowActivity.class);
                startActivity(intent);

            }
        });

        llAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), AddMoneyToWallet.class);
                startActivity(intent);

            }
        });


        llRequestMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), RequestMoneyToWallet.class);
                startActivity(intent);

            }
        });

        btnViewAllVouchers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), GiftVoucherAll.class);
                startActivity(intent);
            }
        });

        getTopShoppingOffersRequest();
        getBrandVouchers();
        getCategoriesRequest();

        return mView;
    }

    private void getTopShoppingOffersRequest() {
        String xAccessToken = "mykey";

        topOfferProgressBar.setVisibility(View.VISIBLE);
        topShoppingOffersGridView.setVisibility(View.GONE);

        mainAPIInterface.getTopShoppingOffers(xAccessToken).enqueue(new Callback<GetTopShoppingOffersModel>() {
            @Override
            public void onResponse(Call<GetTopShoppingOffersModel> call, Response<GetTopShoppingOffersModel> response) {

                if (response.isSuccessful()) {

                    topOfferProgressBar.setVisibility(View.GONE);
                    topShoppingOffersGridView.setVisibility(View.VISIBLE);

                    offerArrayList = response.body().getOffers();

                    topShoppingOfferAdapter = new TopShoppingOfferAdapter((AppCompatActivity) getActivity(), offerArrayList);

                    topShoppingOffersGridView.setExpanded(true);

                    topShoppingOffersGridView.setAdapter(topShoppingOfferAdapter);

                }
            }

            @Override
            public void onFailure(Call<GetTopShoppingOffersModel> call, Throwable t) {
                topOfferProgressBar.setVisibility(View.GONE);
                topShoppingOffersGridView.setVisibility(View.VISIBLE);
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    public void getBrandVouchers() {
        String xAccessToken = "mykey";

        brandVoucherProgressBar.setVisibility(View.VISIBLE);
        brandVoucherGridView.setVisibility(View.GONE);

        mainAPIInterface.getAllGiftVouchers(xAccessToken).enqueue(new Callback<GetAllGiftVouchers>() {
            @Override
            public void onResponse(Call<GetAllGiftVouchers> call, Response<GetAllGiftVouchers> response) {


                if (response.isSuccessful()) {

                    brandVoucherProgressBar.setVisibility(View.GONE);
                    brandVoucherGridView.setVisibility(View.VISIBLE);

                    giftVoucherArrayList = response.body().getVouchers();

                    giftVoucherAdapter = new GiftVoucherAdapter((AppCompatActivity) getActivity(), giftVoucherArrayList);

                    brandVoucherGridView.setExpanded(true);

                    brandVoucherGridView.setAdapter(giftVoucherAdapter);

                }
            }

            @Override
            public void onFailure(Call<GetAllGiftVouchers> call, Throwable t) {
                brandVoucherProgressBar.setVisibility(View.GONE);
                brandVoucherGridView.setVisibility(View.VISIBLE);
              //  Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void getCategoriesRequest() {
        String xAccessToken = "mykey";
        categoriesProgressBar1.setVisibility(View.VISIBLE);
        categoryList.setVisibility(View.GONE);


        mainAPIInterface.getCategories(xAccessToken).enqueue(new Callback<CategoryListModel>() {
            @Override
            public void onResponse(Call<CategoryListModel> call, Response<CategoryListModel> response) {

                if (response.isSuccessful()) {
                    categoriesProgressBar1.setVisibility(View.GONE);
                    categoryList.setVisibility(View.VISIBLE);

                    final List<CategoryListModel.Cat> newCatList = response.body().getCat();

                    circularCategoryListAdapter = new CircularCategoryListAdapter(newCatList, getActivity());

                    LinearLayoutManager layoutManager
                            = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);

                    categoryList.setLayoutManager(layoutManager);

                    categoryList.setItemAnimator(new DefaultItemAnimator());
                    categoryList.setAdapter(circularCategoryListAdapter);


                    circularCategoryListAdapter.notifyDataSetChanged();


                    categoryList.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), categoryList, new RecyclerTouchListener.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {

                            if (SmartPayApplication.isNetworkAvailable(getActivity())) {

                                Intent newIntent = new Intent(getActivity(), CategoryDetailActivity.class);
                                newIntent.putExtra("strCategoryName", newCatList.get(position).getServiceName());
                                newIntent.putExtra("strCategoryId", newCatList.get(position).getId());
                                startActivity(newIntent);


                            } else {

                                Toast.makeText(getActivity(), "Check internet connection.!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));

                }
            }

            @Override
            public void onFailure(Call<CategoryListModel> call, Throwable t) {
                categoriesProgressBar1.setVisibility(View.GONE);
                categoryList.setVisibility(View.VISIBLE);

                Log.i("tag", t.getMessage().toString());
            }
        });


    }


}
