package com.smart.paywallet.fragments.mall;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.activity.mall.CartActivity;
import com.smart.paywallet.activity.mall.ThankYouActivity;
import com.smart.paywallet.activity.wallet.PaymentsActivity;
import com.smart.paywallet.adapter.wallet.WalletTransactionAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.db.DatabaseClient;
import com.smart.paywallet.db.Product;
import com.smart.paywallet.models.output.GenerateChecksumOutput;
import com.smart.paywallet.models.output.GetOnlineCartOutput;
import com.smart.paywallet.models.output.GetWalletTransactionsOutput;
import com.smart.paywallet.models.output.ProductOrderNewModel;
import com.smart.paywallet.models.output.ProductOrderNewOutputModel;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_EMAIL;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;
import static com.smart.paywallet.utils.DataVaultManager.KEY_WALLET_ID;


/**
 * Created by Sandeep Londhe on 07-10-2018.
 *
 * @Email :  sandeeplondhe54@gmail.com
 * @Author :  https://twitter.com/mesandeeplondhe
 * @Skype :  sandeeplondhe54
 */

public class PaymentFragment extends Fragment {

    View mView;

    MyTextView placeOrderFinal;

    String newProductNames;

    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 123;

    //Paypal Configuration Object
    private static PayPalConfiguration config;

    LinearLayout paypalLayout, codLayout, paytmLayout, walletLayout;


    ImageView imgPaypalselected, imgWalletselected, imgPaytmselected, imgSmartpayselected;

    int isPaymentSelected;

    String strPaymentType;

    MainAPIInterface mainAPIInterface;

    public ArrayList<String> paybleList;


    TextView finalAmount;

    TextView finalAddress;

    double price = 0;
    double old_price = 0;
    double discount = 0;


    private ProgressDialog dialog;

    String strSellerId;


    String paypal_id;

    TextView walletBalance;

    TextView txtCurrrencySymbol;

    ArrayList<GetOnlineCartOutput.CartProduct> cartProductArrayList;

    ArrayList<ProductOrderNewModel.Product> productArrayList;

    String wallet_id;

    String strCustomerId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.payment_fragment, container, false);

        mainAPIInterface = ApiUtils.getAPIService();

        placeOrderFinal = (MyTextView) mView.findViewById(R.id.placeOrderFinal);

        productArrayList = new ArrayList<ProductOrderNewModel.Product>();

        paypalLayout = (LinearLayout) mView.findViewById(R.id.paypalLayout);
        codLayout = (LinearLayout) mView.findViewById(R.id.codLayout);
        paytmLayout = (LinearLayout) mView.findViewById(R.id.paytmLayout);
        walletLayout = (LinearLayout) mView.findViewById(R.id.walletLayout);

        txtCurrrencySymbol = (TextView) mView.findViewById(R.id.txtCurrrencySymbol);

        CartActivity.strProductName = new ArrayList<String>();

        txtCurrrencySymbol.setText(SmartPayApplication.CURRENCY_CODE + " " + SmartPayApplication.CURRENCY_SYMBOL);

        imgPaypalselected = (ImageView) mView.findViewById(R.id.imgPaypalselected);
        imgWalletselected = (ImageView) mView.findViewById(R.id.imgWalletselected);
        imgPaytmselected = (ImageView) mView.findViewById(R.id.imgPaytmselected);
        imgSmartpayselected = (ImageView) mView.findViewById(R.id.imgSmartpayselected);


        walletBalance = (TextView) mView.findViewById(R.id.walletBalance);

        finalAmount = (TextView) mView.findViewById(R.id.finalAmount);
        finalAddress = (TextView) mView.findViewById(R.id.finalAddress);

        paybleList = CartActivity.strTotalPayble;


        if (SmartPayApplication.PAYPAL_SANDBOX.equalsIgnoreCase("1")) {

            System.out.println(SmartPayApplication.CURRENCY_CODE);


            config = new PayPalConfiguration()
                    // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                    // or live (ENVIRONMENT_PRODUCTION)
                    .rememberUser(false)
                    .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                    .clientId(SmartPayApplication.PAYPAL_CLIENT_ID);

        } else {


            System.out.println(SmartPayApplication.CURRENCY_CODE);

            config = new PayPalConfiguration()
                    // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                    // or live (ENVIRONMENT_PRODUCTION)
                    .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
                    .rememberUser(true)
                    .clientId(SmartPayApplication.PAYPAL_CLIENT_ID);

        }

        getWalletBalanceRequest();


        finalAddress.setText(CartActivity.strCustomerAddress + " " + CartActivity.strCustomerCity + "\n" +
                CartActivity.strCustomerZipcode + " " + CartActivity.strCustomerCountry);


        paypalLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CartActivity.strPaymentType = "1";

                isPaymentSelected = 1;

                paypalLayout.isSelected();
                imgPaypalselected.setVisibility(View.VISIBLE);
                imgWalletselected.setVisibility(View.INVISIBLE);
                imgPaytmselected.setVisibility(View.INVISIBLE);
                imgSmartpayselected.setVisibility(View.INVISIBLE);

            }
        });


        codLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isPaymentSelected = 2;

                codLayout.isSelected();
                imgPaypalselected.setVisibility(View.INVISIBLE);
                imgWalletselected.setVisibility(View.VISIBLE);
                imgPaytmselected.setVisibility(View.INVISIBLE);
                imgSmartpayselected.setVisibility(View.INVISIBLE);


            }
        });

        paytmLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isPaymentSelected = 3;

                paytmLayout.isSelected();
                imgPaypalselected.setVisibility(View.INVISIBLE);
                imgWalletselected.setVisibility(View.INVISIBLE);
                imgPaytmselected.setVisibility(View.VISIBLE);
                imgSmartpayselected.setVisibility(View.INVISIBLE);


            }
        });

        walletLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isPaymentSelected = 4;

                walletLayout.isSelected();
                imgPaypalselected.setVisibility(View.INVISIBLE);
                imgWalletselected.setVisibility(View.INVISIBLE);
                imgPaytmselected.setVisibility(View.INVISIBLE);
                imgSmartpayselected.setVisibility(View.VISIBLE);


            }
        });


        placeOrderFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isPaymentSelected == 1) {

                    paypalPaymentRequest();

                } else if (isPaymentSelected == 2) {

                    placeCashOnDeleveryRequest("2");

                } else if (isPaymentSelected == 3) {

                    placePayTMBookingRequest();


                } else if (isPaymentSelected == 4) {

                    placeSmartPayOrderRequest();

                } else {
                    Toast.makeText(getActivity(), "Please select payment method.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAllCartProducts();

    }


    private void getWalletBalanceRequest() {

        String xAccessToken = "mykey";

        wallet_id = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);


        MultipartBody.Part seller_id_body = MultipartBody.Part.createFormData("wallet_id", wallet_id);

        mainAPIInterface.getWalletTransactions(xAccessToken, seller_id_body).enqueue(new Callback<GetWalletTransactionsOutput>() {
            @Override
            public void onResponse(Call<GetWalletTransactionsOutput> call, Response<GetWalletTransactionsOutput> response) {

                if (response.isSuccessful()) {

                    double value = Double.valueOf(response.body().getBalance());

                    String strAccountBalance = String.format("%.2f", value);
                    walletBalance.setVisibility(View.VISIBLE);
                    walletBalance.setText(SmartPayApplication.CURRENCY_SYMBOL + strAccountBalance);

                }

            }

            @Override
            public void onFailure(Call<GetWalletTransactionsOutput> call, Throwable t) {
                Log.i("tag", t.getMessage().toString());
            }
        });

    }

    public void paypalPaymentRequest() {

        price = price / Double.valueOf(SmartPayApplication.CURRENCY_EXCHANGE_RATE);

        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(price)), "USD", "Pay to " + getActivity().getString(R.string.app_name),
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(getActivity(), PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        Toast.makeText(getActivity(), "Thanks For Making Payment", Toast.LENGTH_LONG).show();

                        JSONObject newJsonArray = new JSONObject(paymentDetails);

                        JSONObject newJsonObject2 = newJsonArray.getJSONObject("response");

                        paypal_id = newJsonObject2.getString("id");

                        System.out.print("PAYPAL ID==" + paypal_id);


                        placeCashOnDeleveryRequest("1");


                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }


    /**
     * Cash On Deliver Order
     */

    private void placeCashOnDeleveryRequest(String strType) {
        String xAccessToken = "mykey";

        dialog = new ProgressDialog(getActivity());

        dialog.setMessage("Placing your order.");
        dialog.show();

        ProductOrderNewModel productOrderNewModel = new ProductOrderNewModel();
        productOrderNewModel.setProducts(productArrayList);
        productOrderNewModel.setCustomerName(CartActivity.strCustomerFirstname);
        productOrderNewModel.setCustomerId(DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID));
        productOrderNewModel.setCustomerEmail(DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_EMAIL));
        productOrderNewModel.setCustomerPhone(CartActivity.strPhoneNo);
        productOrderNewModel.setAddress(CartActivity.strCustomerAddress);
        productOrderNewModel.setCity(CartActivity.strCustomerCity);
        productOrderNewModel.setState(CartActivity.strCustomerState);
        productOrderNewModel.setZipcode(CartActivity.strCustomerZipcode);
        productOrderNewModel.setCountry(CartActivity.strCustomerCountry);
        productOrderNewModel.setPaymentType(strType);
        productOrderNewModel.setSellerId(strSellerId);
        productOrderNewModel.setTotalAmount(String.valueOf(price));
        productOrderNewModel.setShippingCharge("0");
        productOrderNewModel.setTax("0");


        mainAPIInterface.newOrderBookingRequest(xAccessToken, productOrderNewModel).enqueue(new Callback<ProductOrderNewOutputModel>() {
            @Override
            public void onResponse(Call<ProductOrderNewOutputModel> call, Response<ProductOrderNewOutputModel> response) {


                if (response.isSuccessful()) {

                    dialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        new DeleteCartItems().execute();

                        Toast.makeText(getActivity(), "Your Payment Successful.", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(getActivity(), ThankYouActivity.class);
                        i.putStringArrayListExtra("newProductNames", CartActivity.strProductName);
                        i.putExtra("amount", String.valueOf(price));
                        i.putExtra("address", CartActivity.strCustomerAddress);
                        startActivity(i);
                        getActivity().finish();


                    } else {
                        Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_LONG).show();

                    }


                }
            }

            @Override
            public void onFailure(Call<ProductOrderNewOutputModel> call, Throwable t) {
                dialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }


    private void placePayTMBookingRequest() {
        String xAccessToken = "mykey";

        dialog = new ProgressDialog(getActivity());

        dialog.setMessage("Placing your order.");
        dialog.show();

        ProductOrderNewModel productOrderNewModel = new ProductOrderNewModel();
        productOrderNewModel.setProducts(productArrayList);
        productOrderNewModel.setCustomerName(CartActivity.strCustomerFirstname);
        productOrderNewModel.setCustomerId(DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID));
        productOrderNewModel.setCustomerEmail(DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_EMAIL));
        productOrderNewModel.setCustomerPhone(CartActivity.strPhoneNo);
        productOrderNewModel.setAddress(CartActivity.strCustomerAddress);
        productOrderNewModel.setCity(CartActivity.strCustomerCity);
        productOrderNewModel.setState(CartActivity.strCustomerState);
        productOrderNewModel.setZipcode(CartActivity.strCustomerZipcode);
        productOrderNewModel.setCountry(CartActivity.strCustomerCountry);
        productOrderNewModel.setPaymentType("3");
        productOrderNewModel.setSellerId(strSellerId);
        productOrderNewModel.setTotalAmount(String.valueOf(price));
        productOrderNewModel.setShippingCharge("0");
        productOrderNewModel.setTax("0");
        productOrderNewModel.setWallet_id(wallet_id);


        mainAPIInterface.newOrderBookingRequest(xAccessToken, productOrderNewModel).enqueue(new Callback<ProductOrderNewOutputModel>() {
            @Override
            public void onResponse(Call<ProductOrderNewOutputModel> call, Response<ProductOrderNewOutputModel> response) {


                if (response.isSuccessful()) {

                    dialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        new DeleteCartItems().execute();

                        generateChecksumRequest(String.valueOf(response.body().getBookingId()));


                    } else {
                        Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_LONG).show();

                    }


                }
            }

            @Override
            public void onFailure(Call<ProductOrderNewOutputModel> call, Throwable t) {
                dialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    /**
     * Pay By SmartPay Wallet
     */

    private void placeSmartPayOrderRequest() {



        String xAccessToken = "mykey";

        dialog = new ProgressDialog(getActivity());

        dialog.setMessage("Placing your order.");
        dialog.show();

        ProductOrderNewModel productOrderNewModel = new ProductOrderNewModel();
        productOrderNewModel.setProducts(productArrayList);
        productOrderNewModel.setCustomerName(CartActivity.strCustomerFirstname);
        productOrderNewModel.setCustomerId(DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID));
        productOrderNewModel.setCustomerEmail(DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_EMAIL));
        productOrderNewModel.setCustomerPhone(CartActivity.strPhoneNo);
        productOrderNewModel.setAddress(CartActivity.strCustomerAddress);
        productOrderNewModel.setCity(CartActivity.strCustomerCity);
        productOrderNewModel.setState(CartActivity.strCustomerState);
        productOrderNewModel.setZipcode(CartActivity.strCustomerZipcode);
        productOrderNewModel.setCountry(CartActivity.strCustomerCountry);
        productOrderNewModel.setPaymentType("4");
        productOrderNewModel.setSellerId(strSellerId);
        productOrderNewModel.setTotalAmount(String.valueOf(price));
        productOrderNewModel.setShippingCharge("0");
        productOrderNewModel.setTax("0");
        productOrderNewModel.setWallet_id(wallet_id);


        mainAPIInterface.newOrderBookingByWalletRequest(xAccessToken, productOrderNewModel).enqueue(new Callback<ProductOrderNewOutputModel>() {
            @Override
            public void onResponse(Call<ProductOrderNewOutputModel> call, Response<ProductOrderNewOutputModel> response) {


                if (response.isSuccessful()) {

                    dialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        new DeleteCartItems().execute();

                        Toast.makeText(getActivity(), "Your Payment Successful.", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(getActivity(), ThankYouActivity.class);
                        i.putStringArrayListExtra("newProductNames", CartActivity.strProductName);
                        i.putExtra("amount", String.valueOf(price));
                        i.putExtra("address", CartActivity.strCustomerAddress);
                        startActivity(i);
                        getActivity().finish();


                    } else {
                        Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_LONG).show();

                    }


                }
            }

            @Override
            public void onFailure(Call<ProductOrderNewOutputModel> call, Throwable t) {
                dialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    class DeleteCartItems extends AsyncTask<Void, Void, List<Product>> {

        @Override
        protected void onPreExecute() {

//            deliveryResponseList.clear();
            Log.e("onPreExecutive", "called");
        }

        @Override
        protected List<Product> doInBackground(Void... voids) {
            DatabaseClient
                    .getInstance(getActivity())
                    .getAppDatabase()
                    .daoProduct()
                    .delete();


            return null;


        }

        @Override
        protected void onPostExecute(List<Product> productsList) {
            super.onPostExecute(productsList);

            //     Toast.makeText(getActivity(), "Product Removed", Toast.LENGTH_LONG).show();

        }

    }

    private void getAllCartProducts() {
        String xAccessToken = "mykey";

        dialog = new ProgressDialog(getActivity());

        dialog.setMessage("Calculating final amount.");
        dialog.show();

        strCustomerId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);

        MultipartBody.Part customer_id_body = MultipartBody.Part.createFormData("customer_id", strCustomerId);

        mainAPIInterface.getOnlineCartList(xAccessToken, customer_id_body).enqueue(new Callback<GetOnlineCartOutput>() {
            @Override
            public void onResponse(Call<GetOnlineCartOutput> call, Response<GetOnlineCartOutput> response) {


                if (response.isSuccessful()) {

                    dialog.dismiss();

                    cartProductArrayList = response.body().getCartProducts();


                    for (int i = 0; i < cartProductArrayList.size(); i++) {

                        price += Double.valueOf(cartProductArrayList.get(i).getPrice());
                        old_price += Double.valueOf(cartProductArrayList.get(i).getOld_price());

                        strSellerId = cartProductArrayList.get(i).getSellerId();
                        ProductOrderNewModel.Product product = new ProductOrderNewModel.Product();


                        product.setProductId(cartProductArrayList.get(i).getProductId());
                        product.setPrice(cartProductArrayList.get(i).getPrice());
                        product.setDiscount(cartProductArrayList.get(i).getDiscount());
                        product.setProductName(cartProductArrayList.get(i).getProductName());
                        product.setQuantity(cartProductArrayList.get(i).getQuantity());
                        product.setSellerId(cartProductArrayList.get(i).getSellerId());

                        productArrayList.add(product);


                        CartActivity.strProductName.add(cartProductArrayList.get(i).getProductName());

                    }

                    discount = old_price - price;

                    finalAmount.setText(String.valueOf(price));


                }
            }

            @Override
            public void onFailure(Call<GetOnlineCartOutput> call, Throwable t) {
                dialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }


    //For PayTM

    private void generateChecksumRequest(final String order_id) {

        String xAccessToken = "mykey";

        String strCallBackUrl = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + order_id;

        MultipartBody.Part MID_BODY = MultipartBody.Part.createFormData("MID", SmartPayApplication.PAYTM_MERCHANT_ID);
        MultipartBody.Part ORDER_ID_BODY = MultipartBody.Part.createFormData("ORDER_ID", order_id);
        MultipartBody.Part CUST_ID_BODY = MultipartBody.Part.createFormData("CUST_ID", strCustomerId);
        MultipartBody.Part CHANNEL_ID_BODY = MultipartBody.Part.createFormData("CHANNEL_ID", "WAP");
        MultipartBody.Part TXN_AMOUNT_BODY = MultipartBody.Part.createFormData("TXN_AMOUNT", String.valueOf(price));
        MultipartBody.Part WEBSITE_BODY = MultipartBody.Part.createFormData("WEBSITE", "APPSTAGING");
        MultipartBody.Part CALLBACK_URL_BODY = MultipartBody.Part.createFormData("CALLBACK_URL", strCallBackUrl);
        MultipartBody.Part INDUSTRY_TYPE_ID_BODY = MultipartBody.Part.createFormData("INDUSTRY_TYPE_ID", "Retail");

        mainAPIInterface.generatePaytmChecksumRequest(xAccessToken, MID_BODY, ORDER_ID_BODY, CUST_ID_BODY, CHANNEL_ID_BODY,
                TXN_AMOUNT_BODY, WEBSITE_BODY, CALLBACK_URL_BODY, INDUSTRY_TYPE_ID_BODY).enqueue(new Callback<GenerateChecksumOutput>() {
            @Override
            public void onResponse(Call<GenerateChecksumOutput> call, Response<GenerateChecksumOutput> response) {

                if (response.isSuccessful()) {
                    dialog.dismiss();

                    intiatePayTmPayment(response.body().getCHECKSUMHASH(), order_id);

                } else {

                    Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<GenerateChecksumOutput> call, Throwable t) {
                dialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }


    private void intiatePayTmPayment(String checkSum, final String order_id) {

        String strCallBackUrl = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + order_id;

        //For staging
        PaytmPGService Service = PaytmPGService.getStagingService();

        //for production
//        PaytmPGService Service = PaytmPGService.getProductionService();


        Map<String, String> paramMap = new HashMap<String, String>();

        paramMap.put("MID", SmartPayApplication.PAYTM_MERCHANT_ID); //Test MID

// Key in your staging and production MID available in your dashboard
        paramMap.put("ORDER_ID", order_id);
        paramMap.put("CUST_ID", strCustomerId);
//        paramMap.put("MOBILE_NO", strCustomerPhone);
        //      paramMap.put("EMAIL", strCustomerEmail);
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("TXN_AMOUNT", String.valueOf(price));
        paramMap.put("WEBSITE", "APPSTAGING");
// This is the staging value. Production value is available in your dashboard
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
// This is the staging value. Production value is available in your dashboard
        paramMap.put("CALLBACK_URL", strCallBackUrl);
        paramMap.put("CHECKSUMHASH", checkSum);

        PaytmOrder Order = new PaytmOrder((HashMap<String, String>) paramMap);

        Service.initialize(Order, null);

        Service.startPaymentTransaction(getActivity(), true, true, new PaytmPaymentTransactionCallback() {
            /*Call Backs*/
            public void someUIErrorOccurred(String inErrorMessage) {
            }

            public void onTransactionResponse(Bundle inResponse) {
                /*Display the message as below */
                JSONObject json = new JSONObject();
                Set<String> keys = inResponse.keySet();
                for (String key : keys) {
                    try {
                        json.put(key, inResponse.get(key));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


//                updatePaymentForPaytm(json, order_id);

                Toast.makeText(getActivity(), "Your Payment Successful.", Toast.LENGTH_LONG).show();
                Intent i = new Intent(getActivity(), ThankYouActivity.class);
                i.putStringArrayListExtra("newProductNames", CartActivity.strProductName);
                i.putExtra("amount", String.valueOf(price));
                i.putExtra("address", CartActivity.strCustomerAddress);
                startActivity(i);
                getActivity().finish();


                Log.d("paytm", inResponse.toString());
            }

            public void networkNotAvailable() {
            }

            public void clientAuthenticationFailed(String inErrorMessage) {
            }

            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
            }

            public void onBackPressedCancelTransaction() {
            }

            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
            }
        });

    }


}