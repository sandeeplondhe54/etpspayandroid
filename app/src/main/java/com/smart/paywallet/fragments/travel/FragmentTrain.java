package com.smart.paywallet.fragments.travel;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.smart.paywallet.R;
import com.smart.paywallet.activity.wallet.TrainSearchResultActivity;
import com.smart.paywallet.utils.AppConstants;
import com.smart.paywallet.views.MyTextView;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class FragmentTrain extends Fragment {

    View mView;

    MyTextView btnSearchTrains;
    TextView tv_departure_date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.train_fragment, container, false);

        btnSearchTrains = (MyTextView) mView.findViewById(R.id.btnSearchTrains);
        tv_departure_date =(TextView)mView.findViewById(R.id.tv_departure_date);

        tv_departure_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar newCalendar = Calendar.getInstance();
                DatePickerDialog fromDatePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy");
                        tv_departure_date.setText(dayOfMonth+"/"+monthOfYear+"/"+year);
                        //tv_departure_date.setText(dateFormat.format(dateFormat)+"");
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                fromDatePickerDialog.show();

            }
        });

        btnSearchTrains.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), TrainSearchResultActivity.class);
                startActivity(intent);

            }
        });
        return mView;
    }

/*    public  DatePickerDialog showDatePickerDialog(final TextView textView) {
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                textView.setText(newDate.toString());
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();

        return fromDatePickerDialog;
    }*/
}
