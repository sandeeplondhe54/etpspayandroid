package com.smart.paywallet.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.smart.paywallet.R;
import com.smart.paywallet.activity.wallet.HomeActivity;
import com.smart.paywallet.activity.wallet.PaymentsActivity;
import com.smart.paywallet.activity.wallet.RequestMoneyToWallet;
import com.smart.paywallet.activity.wallet.SupportCenterActivity;
import com.smart.paywallet.db.DatabaseClient;


/**
 * Created by Sandeep Londhe on 15-Oct-16.
 */
public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    String notification_type = "";
    String strMessage = "";
    String strDate = "";
    String strTitle = "";
    String strSender_name = "";
    String strSender_id = "";
    String strSender_image = "";
    String strItem_id = "";
    String strItem_name = "";
    String strItem_date = "";

    String userMessageText = "";
    String userMessageText2 = "";


    @SuppressLint("WrongThread")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);

        System.out.println("Notification recived");
        System.out.println("Notification data==" + remoteMessage.getData().toString());


        notification_type = remoteMessage.getData().get("notification_type");
        strMessage = remoteMessage.getData().get("message");
        strDate = remoteMessage.getData().get("date");
        strTitle = remoteMessage.getData().get("title");
        strSender_name = remoteMessage.getData().get("sender_name");
        strSender_id = remoteMessage.getData().get("sender_id");
        strSender_image = remoteMessage.getData().get("sender_image");
        strItem_id = remoteMessage.getData().get("item_id");
        strItem_name = remoteMessage.getData().get("item_name");
        strItem_date = remoteMessage.getData().get("item_date");

        userMessageText = "";
        userMessageText2 = "";

        if (notification_type.equalsIgnoreCase("order")) {

            userMessageText = "Your new order for " + strItem_name + " is confirmed.";

            userMessageText2 = "New Order Status";

            new SaveNotificationToDb().execute();

            showNotification(strMessage, userMessageText2, userMessageText, strItem_id);

        } else if (notification_type.equalsIgnoreCase("support")) {

            userMessageText = "Your support ticket is answered";

            userMessageText2 = "Your support ticket";

            showSupportNotification();

        } else if (notification_type.equalsIgnoreCase("money_added")) {

            userMessageText = "Money Added To Your Account";

            userMessageText2 = "Money Added";

            new SaveNotificationToDb().execute();

            showNotificationPayment(strMessage, userMessageText2, userMessageText, strItem_id);


        } else if (notification_type.equalsIgnoreCase("money_received")) {

            userMessageText = "You Received a Payment";

            userMessageText2 = "Money Received";

            new SaveNotificationToDb().execute();

            showNotificationPayment(strMessage, userMessageText2, userMessageText, strItem_id);

        } else if (notification_type.equalsIgnoreCase("request")) {

            userMessageText = "You Received a Payment Request";

            userMessageText2 = "Payment Request";

            new SaveNotificationToDb().execute();

            showNotificationPaymentRequest(strMessage, userMessageText2, userMessageText, strItem_id);

        } else if (notification_type.equalsIgnoreCase("request_accepted")) {

            userMessageText = "You received a new payment.";

            userMessageText2 = "Payment Received";

            new SaveNotificationToDb().execute();

            showNotificationPayment(strMessage, userMessageText2, userMessageText, strItem_id);

        } else if (notification_type.equalsIgnoreCase("request_rejected")) {

            userMessageText = "Your payment request is declined.";

            userMessageText2 = "Request Declined";

            new SaveNotificationToDb().execute();

            showNotificationPaymentRequest(strMessage, userMessageText2, userMessageText, strItem_id);

        }

    }

    private void showNotification(String message, String smallTitle, String notificationType, String strItem_id) {


        NotificationManager mNotificationManager;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "notify_001");
        Intent ii = new Intent(getApplicationContext(), HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(message);
        bigText.setBigContentTitle(message);
        bigText.setSummaryText(smallTitle);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(smallTitle);
        mBuilder.setContentText(notificationType);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);

        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "YOUR_CHANNEL_ID";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        mNotificationManager.notify(0, mBuilder.build());

    }

    private void showNotificationPayment(String message, String smallTitle, String notificationType, String strItem_id) {


        NotificationManager mNotificationManager;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "notify_001");
        Intent ii = new Intent(getApplicationContext(), PaymentsActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(message);
        bigText.setBigContentTitle(message);
        bigText.setSummaryText(smallTitle);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(smallTitle);
        mBuilder.setContentText(notificationType);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);

        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "YOUR_CHANNEL_ID";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        mNotificationManager.notify(0, mBuilder.build());

    }

    private void showNotificationPaymentRequest(String message, String smallTitle, String notificationType, String strItem_id) {


        NotificationManager mNotificationManager;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "notify_001");
        Intent ii = new Intent(getApplicationContext(), RequestMoneyToWallet.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(message);
        bigText.setBigContentTitle(message);
        bigText.setSummaryText(smallTitle);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(smallTitle);
        mBuilder.setContentText(notificationType);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);

        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "YOUR_CHANNEL_ID";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        mNotificationManager.notify(0, mBuilder.build());

    }

    private void showSupportNotification() {

        NotificationManager mNotificationManager;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "notify_001");
        Intent ii = new Intent(getApplicationContext(), SupportCenterActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText("Your Support Ticket");
        bigText.setBigContentTitle("Your Support Ticket");
        bigText.setSummaryText("Your Support Ticket");

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle("Your Support Ticket");
        mBuilder.setContentText("Your Support Ticket is updated by admin");
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);

        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "YOUR_CHANNEL_ID";
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        mNotificationManager.notify(0, mBuilder.build());


    }

    class SaveNotificationToDb extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            //adding new notification

            com.smart.paywallet.db.Notification newNotification = new com.smart.paywallet.db.Notification();

            newNotification.setItemId(strItem_id);
            newNotification.setSend_date(strDate);
            newNotification.setSender_id(strSender_id);
            newNotification.setTitle(strTitle);
            newNotification.setSender_image(strSender_image);
            newNotification.setType(notification_type);

            //adding to database
            DatabaseClient.getInstance(getApplicationContext()).getNotificationDatabase()
                    .daoNotification()
                    .insert(newNotification);

            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }


}
