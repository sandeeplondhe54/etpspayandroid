package com.smart.paywallet.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Sandeep Londhe on 15-Oct-16.
 */


public class FirebaseInstanceIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {

        String token = FirebaseInstanceId.getInstance().getToken();

        sendToken(token);
    }


    public void sendToken(String token) {

        //   new RegisterToken().execute("http://howtododemo.esy.es/api.php?token="+token);

    }

}
