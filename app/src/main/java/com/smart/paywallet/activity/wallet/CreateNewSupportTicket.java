package com.smart.paywallet.activity.wallet;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.IsSellerExist;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_NAME;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;


public class CreateNewSupportTicket extends AppCompatActivity {

    MyTextView upload_attachment;
    MyEditText edtUserIssue;
    MyEditText edtDescription;
    MyTextView btnTicketSubmit;

    ImageView img_attachment;

    String selectedPath1, strUserSubject, strDescription;

    ProgressDialog newProgressDialog;

    MainAPIInterface mainAPIInterface;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_support_ticket_layout);
        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();


        upload_attachment = (MyTextView) findViewById(R.id.upload_attachment);
        edtUserIssue = (MyEditText) findViewById(R.id.edtUserIssue);
        edtDescription = (MyEditText) findViewById(R.id.edtDescription);
        btnTicketSubmit = (MyTextView) findViewById(R.id.btnTicketSubmit);

        img_attachment = (ImageView) findViewById(R.id.img_attachment);

        upload_attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PickImageDialog.build(new PickSetup())
                        .setOnPickResult(new IPickResult() {
                            @Override
                            public void onPickResult(PickResult r) {
                                //TODO: do what you have to...

                                img_attachment.setImageBitmap(r.getBitmap());
                                img_attachment.setVisibility(View.VISIBLE);

                                selectedPath1 = getRealPathFromURI(r.getUri());


                            }
                        })
                        .setOnPickCancel(new IPickCancel() {
                            @Override
                            public void onCancelClick() {
                                //TODO: do what you have to if user clicked cancel
                            }
                        }).show(getSupportFragmentManager());


            }
        });

        btnTicketSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strUserSubject = edtUserIssue.getText().toString();
                strDescription = edtDescription.getText().toString();

                if (strUserSubject.length() < 4) {

                    edtUserIssue.setFocusable(true);
                    edtUserIssue.setError("Enter the issue");

                } else {

                    createTicketRequest();
                }

            }
        });

    }


    private void createTicketRequest() {

        String strCustomerId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);
        String strCustomerName = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_NAME);

        String xAccessToken = "mykey";

        if (selectedPath1 != null) {

            newProgressDialog = new ProgressDialog(CreateNewSupportTicket.this);

            newProgressDialog.setMessage("Creating your Support Ticket.");

            newProgressDialog.show();

            File file = new File(selectedPath1);

            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part fileBody = MultipartBody.Part.createFormData("message_attachment", file.getName(), reqFile);

            MultipartBody.Part subjectBody = MultipartBody.Part.createFormData("subject", strUserSubject);
            MultipartBody.Part customerId = MultipartBody.Part.createFormData("customer_id", strCustomerId);
            MultipartBody.Part customerName = MultipartBody.Part.createFormData("customer_name", strCustomerName);
            MultipartBody.Part messageText = MultipartBody.Part.createFormData("message_text", strDescription);

            mainAPIInterface.createSupportTicketWithAttachment(xAccessToken, subjectBody,
                    customerId, customerName, messageText, fileBody).enqueue(new Callback<IsSellerExist>() {
                @Override
                public void onResponse(Call<IsSellerExist> call, Response<IsSellerExist> response) {
                    if (response.isSuccessful()) {
                        newProgressDialog.dismiss();

                        if (response.body().getSuccess() == 1) {

                            Toast.makeText(CreateNewSupportTicket.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                            finish();

                        } else {
                            Toast.makeText(CreateNewSupportTicket.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<IsSellerExist> call, Throwable t) {
                    newProgressDialog.dismiss();
                    Log.i("tag", t.getMessage().toString());
                }
            });


        } else {


            newProgressDialog = new ProgressDialog(CreateNewSupportTicket.this);

            newProgressDialog.setMessage("Creating your Support Ticket.");

            newProgressDialog.show();

            MultipartBody.Part subjectBody = MultipartBody.Part.createFormData("subject", strUserSubject);
            MultipartBody.Part customerId = MultipartBody.Part.createFormData("customer_id", strCustomerId);
            MultipartBody.Part customerName = MultipartBody.Part.createFormData("customer_name", strCustomerName);
            MultipartBody.Part messageText = MultipartBody.Part.createFormData("message_text", strDescription);

            mainAPIInterface.createSupportTicketWithoutAttachment(xAccessToken, subjectBody, messageText,
                    customerId, customerName).enqueue(new Callback<IsSellerExist>() {
                @Override
                public void onResponse(Call<IsSellerExist> call, Response<IsSellerExist> response) {
                    if (response.isSuccessful()) {
                        newProgressDialog.dismiss();

                        if (response.body().getSuccess() == 1) {

                            Toast.makeText(CreateNewSupportTicket.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            finish();

                        } else {
                            Toast.makeText(CreateNewSupportTicket.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<IsSellerExist> call, Throwable t) {
                    newProgressDialog.dismiss();
                    Log.i("tag", t.getMessage().toString());
                }
            });

        }

    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);

        ImageView appLogo = toolbar.findViewById(R.id.appLogo);
        appLogo.setVisibility(View.GONE);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("24*7 Help");


        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        toolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
