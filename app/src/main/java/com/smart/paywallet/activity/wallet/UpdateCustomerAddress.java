package com.smart.paywallet.activity.wallet;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.activity.mall.EditCustomerAccountActivity;
import com.smart.paywallet.activity.mall.ProfileViewActivity;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.EditUserAccountInputModel;
import com.smart.paywallet.models.output.GetAccountDetailsOutputModel;
import com.smart.paywallet.models.output.PostSellerReviewOutputModel;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;

public class UpdateCustomerAddress extends AppCompatActivity {

    MyEditText customerFullName, addL1, addL2, city, state, zipcode, country;

    MainAPIInterface mainAPIInterface;

    ProgressDialog newProgressDialog;
    ArrayList<GetAccountDetailsOutputModel.Profile> profileArrayList;
    MyTextView saveAddress;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.update_customer_address);
        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();

        customerFullName = (MyEditText) findViewById(R.id.customerFullName);
        addL1 = (MyEditText) findViewById(R.id.addL1);
        addL2 = (MyEditText) findViewById(R.id.addL2);
        city = (MyEditText) findViewById(R.id.city);
        state = (MyEditText) findViewById(R.id.state);
        zipcode = (MyEditText) findViewById(R.id.zipcode);
        country = (MyEditText) findViewById(R.id.country);

        saveAddress = (MyTextView) findViewById(R.id.saveAddress);

        getAccountDetailsRequest();

        saveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (customerFullName.getText().toString().length() < 4) {
                    customerFullName.setError("Enter customer name");
                    customerFullName.setFocusable(true);

                } else if (addL1.getText().toString().length() < 0) {
                    addL1.setError("Enter address here");
                    addL1.setFocusable(true);
                } else if (city.getText().toString().length() < 0) {
                    city.setError("Enter city name");
                    city.setFocusable(true);
                } else if (zipcode.getText().toString().length() < 0) {
                    zipcode.setError("Enter zipcode");
                    zipcode.setFocusable(true);
                } else {
                    updateAddressDetailRequest();
                }
            }
        });

    }


    private void getAccountDetailsRequest() {

        String xAccessToken = "mykey";

        String strCustomerId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);


        newProgressDialog = new ProgressDialog(UpdateCustomerAddress.this);

        newProgressDialog.setMessage("Getting account details.");

        newProgressDialog.show();

        MultipartBody.Part customer_id_body = MultipartBody.Part.createFormData("customer_id", strCustomerId);

        mainAPIInterface.getUserAccountDetails(xAccessToken, customer_id_body).enqueue(new Callback<GetAccountDetailsOutputModel>() {
            @Override
            public void onResponse(Call<GetAccountDetailsOutputModel> call, Response<GetAccountDetailsOutputModel> response) {


                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        profileArrayList = response.body().getProfile();

                        customerFullName.setText(profileArrayList.get(0).getUsername());
                        addL1.setText(profileArrayList.get(0).getAddressLine1());
                        addL2.setText(profileArrayList.get(0).getAddressLine2());
                        city.setText(profileArrayList.get(0).getCity());
                        state.setText(profileArrayList.get(0).getState());
                        zipcode.setText(profileArrayList.get(0).getZipcode());
                        country.setText(profileArrayList.get(0).getCountry());

                    } else {

                        Toast.makeText(UpdateCustomerAddress.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<GetAccountDetailsOutputModel> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });

    }

    private void updateAddressDetailRequest() {

        String xAccessToken = "mykey";

        String strCustomerId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);


        newProgressDialog = new ProgressDialog(UpdateCustomerAddress.this);

        newProgressDialog.setMessage("Updating account details.");

        newProgressDialog.show();


        EditUserAccountInputModel editUserAccountInputModel = new EditUserAccountInputModel();

        editUserAccountInputModel.setUserId(strCustomerId);
        editUserAccountInputModel.setFirstname(customerFullName.getText().toString());
        editUserAccountInputModel.setEmailId("");
        editUserAccountInputModel.setAddressLine1(addL1.getText().toString());
        editUserAccountInputModel.setAddressLine2(addL2.getText().toString());
        editUserAccountInputModel.setCity(city.getText().toString());
        editUserAccountInputModel.setState(state.getText().toString());
        editUserAccountInputModel.setZipcode(zipcode.getText().toString());
        editUserAccountInputModel.setCountry(country.getText().toString());
        editUserAccountInputModel.setPhoneNo("");
        editUserAccountInputModel.setBirthdate("");
        editUserAccountInputModel.setGender("");


        mainAPIInterface.postEditUserAccount(xAccessToken, editUserAccountInputModel).enqueue(new Callback<PostSellerReviewOutputModel>() {
            @Override
            public void onResponse(Call<PostSellerReviewOutputModel> call, Response<PostSellerReviewOutputModel> response) {


                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        Toast.makeText(UpdateCustomerAddress.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        finish();

                    } else {

                        Toast.makeText(UpdateCustomerAddress.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<PostSellerReviewOutputModel> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Update Address");

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
