package com.smart.paywallet.activity.wallet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.activity.mall.CartActivity;
import com.smart.paywallet.activity.mall.ThankYouActivity;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.GenerateChecksumOutput;
import com.smart.paywallet.models.output.IsSellerExist;
import com.smart.paywallet.models.output.ProductOrderNewOutputModel;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;
import static com.smart.paywallet.utils.DataVaultManager.KEY_WALLET_ID;

public class AddMoneyToWalletPaymentActivity extends AppCompatActivity {

    String strCustomerId, amountSum, strWalletId;
    MainAPIInterface mainAPIInterface;

    LinearLayout llAddMoneyToWallet;
    MyEditText edtAmount;

    ProgressDialog dialog;

    PayPalConfiguration config;

    double price = 0;
    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 123;

    String paypal_id;

    String amount;

    LinearLayout paypalLayout, paytmLayout;
    ImageView imgPaypalselected, imgPaytmselected;

    int isPaymentSelected = 0;


    MyTextView placeOrderFinal;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_payment_options);

        mainAPIInterface = ApiUtils.getAPIService();

        Intent value = getIntent();

        amount = value.getExtras().getString("price");
        price = Double.valueOf(amount);


        setupActionBar();

        TextView finalAmount = (TextView) findViewById(R.id.finalAmount);
        TextView txtCurrrencySymbol = (TextView) findViewById(R.id.txtCurrrencySymbol);
        paypalLayout = (LinearLayout) findViewById(R.id.paypalLayout);
        paytmLayout = (LinearLayout) findViewById(R.id.paytmLayout);
        imgPaypalselected = (ImageView) findViewById(R.id.imgPaypalselected);
        imgPaytmselected = (ImageView) findViewById(R.id.imgPaytmselected);
        placeOrderFinal = (MyTextView) findViewById(R.id.placeOrderFinal);

        finalAmount.setText(amount);

        strCustomerId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);

        if (SmartPayApplication.PAYPAL_SANDBOX.equalsIgnoreCase("1")) {

            System.out.println(SmartPayApplication.CURRENCY_CODE);


            config = new PayPalConfiguration()
                    // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                    // or live (ENVIRONMENT_PRODUCTION)
                    .rememberUser(true)
                    .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                    .clientId(SmartPayApplication.PAYPAL_CLIENT_ID);

        } else {


            System.out.println(SmartPayApplication.CURRENCY_CODE);

            config = new PayPalConfiguration()
                    // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                    // or live (ENVIRONMENT_PRODUCTION)
                    .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
                    .rememberUser(true)
                    .clientId(SmartPayApplication.PAYPAL_CLIENT_ID);

        }

        paypalLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isPaymentSelected = 1;

                paypalLayout.isSelected();
                imgPaypalselected.setVisibility(View.VISIBLE);
                imgPaytmselected.setVisibility(View.INVISIBLE);

            }
        });

        paytmLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isPaymentSelected = 2;

                paytmLayout.isSelected();
                imgPaypalselected.setVisibility(View.INVISIBLE);
                imgPaytmselected.setVisibility(View.VISIBLE);


            }
        });

        placeOrderFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isPaymentSelected == 1) {

                    initiatePayment();

                } else if (isPaymentSelected == 2) {

                    initiatePayTMPayment();


                } else {
                    Toast.makeText(AddMoneyToWalletPaymentActivity.this, "Please select payment method.", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private void initiatePayment() {

        price = price / Double.valueOf(SmartPayApplication.CURRENCY_EXCHANGE_RATE);

        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(price)), "USD", "Pay to " + AddMoneyToWalletPaymentActivity.this.getString(R.string.app_name),
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(AddMoneyToWalletPaymentActivity.this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        Toast.makeText(AddMoneyToWalletPaymentActivity.this, "Thanks For Making Payment", Toast.LENGTH_LONG).show();

                        JSONObject newJsonArray = new JSONObject(paymentDetails);

                        JSONObject newJsonObject2 = newJsonArray.getJSONObject("response");

                        paypal_id = newJsonObject2.getString("id");

                        System.out.print("PAYPAL ID==" + paypal_id);


                        addMoneyToWalletRequest();


                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    private void initiatePayTMPayment() {

        dialog = new ProgressDialog(AddMoneyToWalletPaymentActivity.this);

        dialog.setMessage("Opening Paytm Wallet.");
        dialog.show();


        String xAccessToken = "mykey";
        String strWalletId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);
        String strCustomerId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);

        MultipartBody.Part wallet_id_body = MultipartBody.Part.createFormData("wallet_id", strWalletId);
        MultipartBody.Part amount_body = MultipartBody.Part.createFormData("amount", String.valueOf(price));
        MultipartBody.Part user_id_body = MultipartBody.Part.createFormData("user_id", strCustomerId);


        mainAPIInterface.paytmAddMoneyOrder(xAccessToken, wallet_id_body, amount_body, user_id_body).enqueue(new Callback<ProductOrderNewOutputModel>() {
            @Override
            public void onResponse(Call<ProductOrderNewOutputModel> call, Response<ProductOrderNewOutputModel> response) {
                if (response.isSuccessful()) {

                    dialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        generateChecksumRequest(String.valueOf(response.body().getBookingId()));

                    } else {

                        Toast.makeText(AddMoneyToWalletPaymentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<ProductOrderNewOutputModel> call, Throwable t) {
                dialog.dismiss();
                Log.i("Error", t.getMessage().toString());
            }
        });


    }

    //For PayTM

    private void generateChecksumRequest(final String order_id) {

        String xAccessToken = "mykey";

        String strCallBackUrl = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + order_id;

        MultipartBody.Part MID_BODY = MultipartBody.Part.createFormData("MID", SmartPayApplication.PAYTM_MERCHANT_ID);
        MultipartBody.Part ORDER_ID_BODY = MultipartBody.Part.createFormData("ORDER_ID", order_id);
        MultipartBody.Part CUST_ID_BODY = MultipartBody.Part.createFormData("CUST_ID", strCustomerId);
        MultipartBody.Part CHANNEL_ID_BODY = MultipartBody.Part.createFormData("CHANNEL_ID", "WAP");
        MultipartBody.Part TXN_AMOUNT_BODY = MultipartBody.Part.createFormData("TXN_AMOUNT", String.valueOf(price));
        MultipartBody.Part WEBSITE_BODY = MultipartBody.Part.createFormData("WEBSITE", "APPSTAGING");
        MultipartBody.Part CALLBACK_URL_BODY = MultipartBody.Part.createFormData("CALLBACK_URL", strCallBackUrl);
        MultipartBody.Part INDUSTRY_TYPE_ID_BODY = MultipartBody.Part.createFormData("INDUSTRY_TYPE_ID", "Retail");

        mainAPIInterface.generatePaytmChecksumRequest(xAccessToken, MID_BODY, ORDER_ID_BODY, CUST_ID_BODY, CHANNEL_ID_BODY,
                TXN_AMOUNT_BODY, WEBSITE_BODY, CALLBACK_URL_BODY, INDUSTRY_TYPE_ID_BODY).enqueue(new Callback<GenerateChecksumOutput>() {
            @Override
            public void onResponse(Call<GenerateChecksumOutput> call, Response<GenerateChecksumOutput> response) {

                if (response.isSuccessful()) {
                    dialog.dismiss();

                    intiatePayTmPayment(response.body().getCHECKSUMHASH(), order_id);

                } else {

                    Toast.makeText(AddMoneyToWalletPaymentActivity.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<GenerateChecksumOutput> call, Throwable t) {
                dialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }


    private void intiatePayTmPayment(String checkSum, final String order_id) {

        String strCallBackUrl = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + order_id;

        //For staging
        PaytmPGService Service = PaytmPGService.getStagingService();

        //for production
//        PaytmPGService Service = PaytmPGService.getProductionService();


        Map<String, String> paramMap = new HashMap<String, String>();

        paramMap.put("MID", SmartPayApplication.PAYTM_MERCHANT_ID); //Test MID

// Key in your staging and production MID available in your dashboard
        paramMap.put("ORDER_ID", order_id);
        paramMap.put("CUST_ID", strCustomerId);
//        paramMap.put("MOBILE_NO", strCustomerPhone);
        //      paramMap.put("EMAIL", strCustomerEmail);
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("TXN_AMOUNT", String.valueOf(price));
        paramMap.put("WEBSITE", "APPSTAGING");
// This is the staging value. Production value is available in your dashboard
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
// This is the staging value. Production value is available in your dashboard
        paramMap.put("CALLBACK_URL", strCallBackUrl);
        paramMap.put("CHECKSUMHASH", checkSum);

        PaytmOrder Order = new PaytmOrder((HashMap<String, String>) paramMap);

        Service.initialize(Order, null);

        Service.startPaymentTransaction(AddMoneyToWalletPaymentActivity.this, true, true, new PaytmPaymentTransactionCallback() {
            /*Call Backs*/
            public void someUIErrorOccurred(String inErrorMessage) {
            }

            public void onTransactionResponse(Bundle inResponse) {
                /*Display the message as below */
                JSONObject json = new JSONObject();
                Set<String> keys = inResponse.keySet();
                for (String key : keys) {
                    try {
                        json.put(key, inResponse.get(key));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


//                updatePaymentForPaytm(json, order_id);

                addMoneyToWalletRequest();

                Log.d("paytm", inResponse.toString());
            }

            public void networkNotAvailable() {
            }

            public void clientAuthenticationFailed(String inErrorMessage) {
            }

            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
            }

            public void onBackPressedCancelTransaction() {
            }

            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
            }
        });

    }


    private void addMoneyToWalletRequest() {
        dialog = new ProgressDialog(AddMoneyToWalletPaymentActivity.this);

        dialog.setMessage("Adding money to your wallet.");
        dialog.show();


        String xAccessToken = "mykey";
        String strWalletId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);

        MultipartBody.Part wallet_id_body = MultipartBody.Part.createFormData("wallet_id", strWalletId);
        MultipartBody.Part amount_body = MultipartBody.Part.createFormData("amount", String.valueOf(price));


        mainAPIInterface.addMoneyToWallet(xAccessToken, wallet_id_body, amount_body).enqueue(new Callback<IsSellerExist>() {
            @Override
            public void onResponse(Call<IsSellerExist> call, Response<IsSellerExist> response) {
                if (response.isSuccessful()) {

                    dialog.dismiss();

                    if (response.body().getSuccess() == 1) {

                        finish();
                        Toast.makeText(AddMoneyToWalletPaymentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                    } else {

                        Toast.makeText(AddMoneyToWalletPaymentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<IsSellerExist> call, Throwable t) {
                dialog.dismiss();
                Log.i("Error", t.getMessage().toString());
            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Choose Payment Option");
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        toolbar.setTitleTextColor(Color.WHITE);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
