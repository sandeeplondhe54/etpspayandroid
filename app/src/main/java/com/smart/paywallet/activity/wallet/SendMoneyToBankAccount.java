package com.smart.paywallet.activity.wallet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.wallet.BeneficiaryListAdapter;
import com.smart.paywallet.adapter.wallet.WalletTransactionAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.GetWalletTransactionsOutput;
import com.smart.paywallet.models.output.MoneyTransferCustomerDetails;
import com.smart.paywallet.models.output.MoneyTransferToBankAccount;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_MOBILE;
import static com.smart.paywallet.utils.DataVaultManager.KEY_NAME;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;
import static com.smart.paywallet.utils.DataVaultManager.KEY_WALLET_ID;

public class SendMoneyToBankAccount extends AppCompatActivity {

    MainAPIInterface mainAPIInterface;

    MyTextView txt_beneficiary_name, txt_beneficiary_account_number, txt_ifsc_code,
            txt_app_balance;

    MyEditText txt_amount_to_send;

    MyTextView btnProceed;

    String strBeneficiaryName, strBeneficiaryAccountNo, strIfscCode, strBeneficiaryId;

    ProgressDialog newProgressDialog;

    double mainWalletBalance, amountToSend;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_money_to_bank_account);
        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();

        Intent value = getIntent();

        strBeneficiaryId = value.getExtras().getString("strBeneficiaryId");
        strBeneficiaryName = value.getExtras().getString("strBeneficiaryName");
        strBeneficiaryAccountNo = value.getExtras().getString("strBeneficiaryAccountNo");
        strIfscCode = value.getExtras().getString("strIfscCode");

        txt_beneficiary_name = (MyTextView) findViewById(R.id.txt_beneficiary_name);
        txt_beneficiary_account_number = (MyTextView) findViewById(R.id.txt_beneficiary_account_number);
        txt_ifsc_code = (MyTextView) findViewById(R.id.txt_ifsc_code);
        txt_amount_to_send = (MyEditText) findViewById(R.id.txt_amount_to_send);
        txt_app_balance = (MyTextView) findViewById(R.id.txt_app_balance);
        btnProceed = (MyTextView) findViewById(R.id.btnProceed);

        txt_beneficiary_name.setText("Send To:- " + strBeneficiaryName);
        txt_beneficiary_account_number.setText("A/C No. " + strBeneficiaryAccountNo);
        txt_ifsc_code.setText("IFSC:- " + strIfscCode);


        getAllTransactionsRequest();

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txt_amount_to_send.getText().toString().length() <= 0) {
                    txt_amount_to_send.setError("Enter amount");
                    txt_amount_to_send.setFocusable(true);
                    Toast.makeText(SendMoneyToBankAccount.this,
                            "Enter the amount to send.", Toast.LENGTH_SHORT).show();

                } else if (txt_amount_to_send.getText().toString().length() > 0) {
                    amountToSend = Double.valueOf(txt_amount_to_send.getText().toString());

                    if (amountToSend > mainWalletBalance) {
                        Toast.makeText(SendMoneyToBankAccount.this,
                                "Your Wallet does not have sufficient funds.", Toast.LENGTH_SHORT).show();
                    } else {
                        sendMoneyToBankAccountRequest();
                    }

                }
            }
        });

    }

    private void getAllTransactionsRequest() {

        String xAccessToken = "mykey";

        String wallet_id = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);

        newProgressDialog = new ProgressDialog(SendMoneyToBankAccount.this);

        newProgressDialog.setMessage("Checking your wallet balance.");

        newProgressDialog.show();

        MultipartBody.Part seller_id_body = MultipartBody.Part.createFormData("wallet_id", wallet_id);

        mainAPIInterface.getWalletTransactions(xAccessToken, seller_id_body).enqueue(new Callback<GetWalletTransactionsOutput>() {
            @Override
            public void onResponse(Call<GetWalletTransactionsOutput> call, Response<GetWalletTransactionsOutput> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        mainWalletBalance = Double.valueOf(response.body().getBalance());

                        String strAccountBalance = String.format("%.2f", mainWalletBalance);

                        txt_app_balance.setText("Balance:- " + SmartPayApplication.CURRENCY_SYMBOL + strAccountBalance);
                    }

                }
            }

            @Override
            public void onFailure(Call<GetWalletTransactionsOutput> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });

    }

    private void sendMoneyToBankAccountRequest() {

        String strCustomerId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);
        String strUserName = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_NAME);
        String strMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);
        String strWalletId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);

        newProgressDialog = new ProgressDialog(SendMoneyToBankAccount.this);

        newProgressDialog.setMessage("Sending Money to Bank Account.");

        newProgressDialog.show();


        String xAccessToken = "mykey";

        MultipartBody.Part customer_mobile_body = MultipartBody.Part.createFormData("customerMobile", strMobile);
        MultipartBody.Part beneficiaryId_body = MultipartBody.Part.createFormData("beneficiaryId", strBeneficiaryId);
        MultipartBody.Part amount_body = MultipartBody.Part.createFormData("amount", String.valueOf((int) amountToSend));
        MultipartBody.Part wallet_id_body = MultipartBody.Part.createFormData("wallet_id", strWalletId);
        MultipartBody.Part customer_id_body = MultipartBody.Part.createFormData("customer_id", strCustomerId);

        mainAPIInterface.moneyTransferToBankAccount(xAccessToken, customer_mobile_body, beneficiaryId_body,
                amount_body, wallet_id_body, customer_id_body).enqueue(new Callback<MoneyTransferToBankAccount>() {
            @Override
            public void onResponse(Call<MoneyTransferToBankAccount> call, Response<MoneyTransferToBankAccount> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("0")) {

                        Toast.makeText(SendMoneyToBankAccount.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    } else if (response.body().getSuccess().equalsIgnoreCase("3")) {

                        Toast.makeText(SendMoneyToBankAccount.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                        final Dialog dialog = new Dialog(SendMoneyToBankAccount.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.money_sent_dialog);

                        MyTextView btnOkay = (MyTextView) dialog.findViewById(R.id.btnOkay);
                        MyTextView receiverName = (MyTextView) dialog.findViewById(R.id.receiverName);
                        MyTextView amount_sent = (MyTextView) dialog.findViewById(R.id.amount_sent);

                        receiverName.setText(response.body().getBeneficiaryName());
                        amount_sent.setText(SmartPayApplication.CURRENCY_SYMBOL + Double.toString(amountToSend));

                        btnOkay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                        dialog.show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MoneyTransferToBankAccount> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });

    }
    private void setupActionBar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Send Money");
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
