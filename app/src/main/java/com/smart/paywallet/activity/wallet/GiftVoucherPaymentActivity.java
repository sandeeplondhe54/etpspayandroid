package com.smart.paywallet.activity.wallet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.RechargeOutput;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_MOBILE;
import static com.smart.paywallet.utils.DataVaultManager.KEY_NAME;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;
import static com.smart.paywallet.utils.DataVaultManager.KEY_WALLET_ID;

public class GiftVoucherPaymentActivity extends AppCompatActivity {

    MainAPIInterface mainAPIInterface;
    ProgressDialog newProgressDialog;

    MyEditText edtreciver_name, edtreciver_email, edtreciver_mobile, edtreciver_message;

    String opid, voucher_name, voucher_image, amount, receiverName, receiverEmail, receiverMobile, giftMessage, strSenderMobile;

    MyTextView payForVoucherButton;

    String strWalletId, strSenderName, customer_id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.voucher_checkout);
        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();

        Intent value = getIntent();

        opid = value.getExtras().getString("opid");
        amount = value.getExtras().getString("amount");
        voucher_name = value.getExtras().getString("voucher_name");
        voucher_image = value.getExtras().getString("voucher_image");

        edtreciver_name = (MyEditText) findViewById(R.id.edtreciver_name);
        edtreciver_email = (MyEditText) findViewById(R.id.edtreciver_email);
        edtreciver_mobile = (MyEditText) findViewById(R.id.edtreciver_mobile);
        edtreciver_message = (MyEditText) findViewById(R.id.edtreciver_message);

        payForVoucherButton = (MyTextView) findViewById(R.id.payForVoucherButton);

        payForVoucherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                receiverName = edtreciver_name.getText().toString();
                receiverEmail = edtreciver_email.getText().toString();
                receiverMobile = edtreciver_mobile.getText().toString();
                giftMessage = edtreciver_message.getText().toString();

                if (receiverName.length() < 3) {
                    edtreciver_name.setError("Enter name here");
                    edtreciver_name.setFocusable(true);
                } else if (receiverEmail.length() < 5) {
                    edtreciver_email.setError("Enter email here");
                    edtreciver_email.setFocusable(true);
                } else if (receiverMobile.length() < 9) {
                    edtreciver_mobile.setError("Enter mobile here");
                    edtreciver_mobile.setFocusable(true);
                } else {

                    buyGiftVoucherRequest();
                }

            }
        });
    }


    private void buyGiftVoucherRequest() {
        String xAccessToken = "mykey";

        customer_id = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);
        strWalletId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);
        strSenderName = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_NAME);
        strSenderMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);


        newProgressDialog = new ProgressDialog(GiftVoucherPaymentActivity.this);

        newProgressDialog.setMessage("Placing your request.");

        newProgressDialog.show();

        strSenderName = strSenderName.toLowerCase().replaceAll(" ", "");
        receiverName = receiverName.toLowerCase().replaceAll(" ", "");
        giftMessage = giftMessage.toLowerCase().replaceAll(" ", "");

        MultipartBody.Part amount_body = MultipartBody.Part.createFormData("amount", amount);
        MultipartBody.Part operatorId_body = MultipartBody.Part.createFormData("operator_code", opid);
        MultipartBody.Part senderName_body = MultipartBody.Part.createFormData("senderName", strSenderName);
        MultipartBody.Part receiverName_body = MultipartBody.Part.createFormData("receiverName", receiverName);
        MultipartBody.Part receiverEmail_body = MultipartBody.Part.createFormData("receiverEmail", receiverEmail);
        MultipartBody.Part senderMobile_body = MultipartBody.Part.createFormData("senderMobile", strSenderMobile);
        MultipartBody.Part receiverMobile_body = MultipartBody.Part.createFormData("receiverMobile", receiverMobile);
        MultipartBody.Part giftMessage_body = MultipartBody.Part.createFormData("giftMessage", giftMessage);
        MultipartBody.Part user_id_body = MultipartBody.Part.createFormData("customer_id", customer_id);
        MultipartBody.Part wallet_id_body = MultipartBody.Part.createFormData("wallet_id", strWalletId);

        mainAPIInterface.buyGiftVoucherRequest(xAccessToken,
                amount_body, operatorId_body, senderName_body, receiverName_body, receiverEmail_body,
                senderMobile_body, receiverMobile_body, giftMessage_body, user_id_body, wallet_id_body
        ).enqueue(new Callback<RechargeOutput>() {
            @Override
            public void onResponse(Call<RechargeOutput> call, Response<RechargeOutput> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        Toast.makeText(GiftVoucherPaymentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(GiftVoucherPaymentActivity.this, ThankYouGiftVoucherDone.class);
                        intent.putExtra("strOperatorCode", opid);
                        intent.putExtra("strOperatorName", voucher_name);
                        intent.putExtra("strOperatorImage", voucher_image);
                        intent.putExtra("amount", amount);
                        intent.putExtra("receiverName", receiverName);
                        intent.putExtra("receiverEmail", receiverEmail);
                        intent.putExtra("receiverMobile", receiverMobile);
                        intent.putExtra("strSenderMobile", strSenderMobile);
                        intent.putExtra("order_id", response.body().getOrderId().toString());

                        startActivity(intent);
                        finish();

                    } else {

                        Toast.makeText(GiftVoucherPaymentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(GiftVoucherPaymentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RechargeOutput> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }


    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Voucher Payment");

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
