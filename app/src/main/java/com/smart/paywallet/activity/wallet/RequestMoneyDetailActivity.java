package com.smart.paywallet.activity.wallet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.recyclerview.RequestListAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.GetWalletTransactionsOutput;
import com.smart.paywallet.models.output.IsSellerExist;
import com.smart.paywallet.models.output.RequestListOutput;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyTextView;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_MOBILE;
import static com.smart.paywallet.utils.DataVaultManager.KEY_WALLET_ID;

public class RequestMoneyDetailActivity extends AppCompatActivity {

    MainAPIInterface mainAPIInterface;

    String request_id, senderMobile, receiverMobile, amount, senderName, receiverName, added_date;

    int requestStatus;

    LinearLayout llAcceptReject;

    MyTextView requestType, requestDate, txtReceiverName, txtRequestId, txtSenderName, txtRequestStatus, txtRequestedAmount;

    ProgressBar requestStatusProgressBar;

    MyTextView btnAcceptPaymentRequest, btnRejectPaymentRequest;

    double mainWalletBalance;

    ProgressDialog newProgressDialog;
    String strMobile;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_money_detail);

        mainAPIInterface = ApiUtils.getAPIService();

        Intent value = getIntent();

        request_id = value.getExtras().getString("request_id");
        senderMobile = value.getExtras().getString("senderMobile");
        receiverMobile = value.getExtras().getString("receverMobile");
        amount = value.getExtras().getString("amount");
        senderName = value.getExtras().getString("senderName");
        receiverName = value.getExtras().getString("receiverName");
        added_date = value.getExtras().getString("added_date");

        setupActionBar();

        llAcceptReject = (LinearLayout) findViewById(R.id.llAcceptReject);
        requestType = (MyTextView) findViewById(R.id.requestType);
        requestDate = (MyTextView) findViewById(R.id.requestDate);
        txtReceiverName = (MyTextView) findViewById(R.id.txtReceiverName);
        txtSenderName = (MyTextView) findViewById(R.id.txtSenderName);
        txtRequestId = (MyTextView) findViewById(R.id.txtRequestId);
        txtRequestStatus = (MyTextView) findViewById(R.id.txtRequestStatus);
        txtRequestedAmount = (MyTextView) findViewById(R.id.txtRequestedAmount);

        requestStatusProgressBar = (ProgressBar) findViewById(R.id.requestStatusProgressBar);

        btnAcceptPaymentRequest = (MyTextView) findViewById(R.id.btnAcceptPaymentRequest);
        btnRejectPaymentRequest = (MyTextView) findViewById(R.id.btnRejectPaymentRequest);

        strMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        if (strMobile.equalsIgnoreCase(senderMobile)) {
            requestType.setText("Request Sent");
        } else {
            requestType.setText("Request Received");
        }

        txtRequestedAmount.setText(SmartPayApplication.CURRENCY_SYMBOL + amount);
        requestDate.setText("Date:- " + added_date);

        txtSenderName.setText(senderName);
        txtReceiverName.setText(receiverName);
        txtRequestId.setText("Request ID:- " + request_id);


        btnAcceptPaymentRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mainWalletBalance >= Double.valueOf(amount)) {
                    acceptPaymentRequest();

                } else {
                    Toast.makeText(RequestMoneyDetailActivity.this, "Please make sure your wallet have sufficient balance to accept this request.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        btnRejectPaymentRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rejectPaymentRequest();
            }
        });

        getRequestStatusDetail();


    }

    @Override
    public void onResume() {
        super.onResume();
        getWalletBalance();
    }

    private void getWalletBalance() {

        String xAccessToken = "mykey";

        String wallet_id = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);

        MultipartBody.Part seller_id_body = MultipartBody.Part.createFormData("wallet_id", wallet_id);

        mainAPIInterface.getWalletTransactions(xAccessToken, seller_id_body).enqueue(new Callback<GetWalletTransactionsOutput>() {
            @Override
            public void onResponse(Call<GetWalletTransactionsOutput> call, Response<GetWalletTransactionsOutput> response) {

                if (response.isSuccessful()) {

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        mainWalletBalance = Double.valueOf(response.body().getBalance());

                    }

                }
            }

            @Override
            public void onFailure(Call<GetWalletTransactionsOutput> call, Throwable t) {
                Log.i("tag", t.getMessage().toString());
            }
        });

    }

    private void getRequestStatusDetail() {

        txtRequestStatus.setVisibility(View.GONE);
        requestStatusProgressBar.setVisibility(View.VISIBLE);

        String xAccessToken = "mykey";

        MultipartBody.Part request_id_body = MultipartBody.Part.createFormData("request_id", request_id);

        mainAPIInterface.checkPaymentRequestStatus(xAccessToken, request_id_body).enqueue(new Callback<IsSellerExist>() {
            @Override
            public void onResponse(Call<IsSellerExist> call, Response<IsSellerExist> response) {

                if (response.isSuccessful()) {

                    txtRequestStatus.setVisibility(View.VISIBLE);
                    requestStatusProgressBar.setVisibility(View.GONE);

                    if (response.body().getSuccess() == 1) {

                        if (response.body().getStatus().equalsIgnoreCase("0")) {

                            txtRequestStatus.setText("Request is Placed");
                            requestStatus = 0;

                        } else if (response.body().getStatus().equalsIgnoreCase("1")) {
                            txtRequestStatus.setText("Request is accepted");
                            requestStatus = 1;

                        } else if (response.body().getStatus().equalsIgnoreCase("2")) {
                            txtRequestStatus.setText("Request is rejected");
                            requestStatus = 2;
                        }

                        if (requestStatus == 0) {

                            if (strMobile.equalsIgnoreCase(senderMobile)) {
                                llAcceptReject.setVisibility(View.GONE);

                            } else {
                                llAcceptReject.setVisibility(View.VISIBLE);

                            }

                        } else {
                            llAcceptReject.setVisibility(View.GONE);
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call<IsSellerExist> call, Throwable t) {
                txtRequestStatus.setVisibility(View.VISIBLE);
                requestStatusProgressBar.setVisibility(View.GONE);
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void acceptPaymentRequest() {

        newProgressDialog = new ProgressDialog(RequestMoneyDetailActivity.this);

        newProgressDialog.setMessage("Updating your request.");

        newProgressDialog.show();

        String xAccessToken = "mykey";

        MultipartBody.Part request_id_body = MultipartBody.Part.createFormData("request_id", request_id);

        mainAPIInterface.acceptPaymentRequest(xAccessToken, request_id_body).enqueue(new Callback<IsSellerExist>() {
            @Override
            public void onResponse(Call<IsSellerExist> call, Response<IsSellerExist> response) {

                if (response.isSuccessful()) {
                    newProgressDialog.dismiss();

                    if (response.body().getSuccess() == 1) {

                        Toast.makeText(RequestMoneyDetailActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        getRequestStatusDetail();
                    }


                }
            }

            @Override
            public void onFailure(Call<IsSellerExist> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void rejectPaymentRequest() {

        newProgressDialog = new ProgressDialog(RequestMoneyDetailActivity.this);

        newProgressDialog.setMessage("Updating your request.");

        newProgressDialog.show();

        String xAccessToken = "mykey";

        MultipartBody.Part request_id_body = MultipartBody.Part.createFormData("request_id", request_id);

        mainAPIInterface.rejectPaymentRequest(xAccessToken, request_id_body).enqueue(new Callback<IsSellerExist>() {
            @Override
            public void onResponse(Call<IsSellerExist> call, Response<IsSellerExist> response) {

                if (response.isSuccessful()) {
                    newProgressDialog.dismiss();

                    if (response.body().getSuccess() == 1) {

                        Toast.makeText(RequestMoneyDetailActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        getRequestStatusDetail();
                    }


                }
            }

            @Override
            public void onFailure(Call<IsSellerExist> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Request Details");
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        toolbar.setTitleTextColor(Color.WHITE);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
