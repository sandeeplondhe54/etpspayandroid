package com.smart.paywallet.activity.wallet;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.wallet.SupportTicketListAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.SupportTicketListOutput;
import com.smart.paywallet.utils.DataVaultManager;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;


public class SupportCenterActivity extends AppCompatActivity {

    RecyclerView supportListView;
    LinearLayout ll_support_layout;

    SupportTicketListAdapter supportTicketListAdapter;
    ArrayList<SupportTicketListOutput.Ticket> ticketArrayList;

    MainAPIInterface mainAPIInterface;
    ProgressDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support_center);
        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();

        supportListView = (RecyclerView) findViewById(R.id.supportListView);
        ll_support_layout = (LinearLayout) findViewById(R.id.ll_support_layout);

        getAllSupportTickets();

    }

    private void getAllSupportTickets() {
        dialog = new ProgressDialog(SupportCenterActivity.this);

        dialog.setMessage("Checking your tickets.");
        dialog.show();

        String strCustomerId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);

        String xAccessToken = "mykey";
        MultipartBody.Part customerId = MultipartBody.Part.createFormData("customer_id", strCustomerId);

        mainAPIInterface.getAllSupportTickets(xAccessToken, customerId).enqueue(new Callback<SupportTicketListOutput>() {
            @Override
            public void onResponse(Call<SupportTicketListOutput> call, Response<SupportTicketListOutput> response) {
                if (response.isSuccessful()) {

                    dialog.dismiss();

                    ticketArrayList = response.body().getTickets();


                    if (ticketArrayList.isEmpty()) {

                        supportListView.setVisibility(View.GONE);
                        ll_support_layout.setVisibility(View.VISIBLE);

                    } else {

                        supportListView.setVisibility(View.VISIBLE);
                        ll_support_layout.setVisibility(View.GONE);

                        supportTicketListAdapter = new SupportTicketListAdapter(ticketArrayList, SupportCenterActivity.this);

                        LinearLayoutManager layoutManager
                                = new LinearLayoutManager(SupportCenterActivity.this, RecyclerView.VERTICAL, false);

                        supportListView.setLayoutManager(layoutManager);

                        supportListView.setItemAnimator(new DefaultItemAnimator());
                        supportListView.setAdapter(supportTicketListAdapter);

                        supportTicketListAdapter.notifyDataSetChanged();

                    }


                }
            }

            @Override
            public void onFailure(Call<SupportTicketListOutput> call, Throwable t) {
                dialog.dismiss();
                Log.i("Error", t.getMessage().toString());
            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);

        ImageView appLogo = toolbar.findViewById(R.id.appLogo);
        appLogo.setVisibility(View.GONE);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Support Center");


        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        toolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
