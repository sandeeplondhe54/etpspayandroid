package com.smart.paywallet.activity.wallet;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.smart.paywallet.BuildConfig;
import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.activity.mall.ChangePasswordActivity;
import com.smart.paywallet.activity.mall.ProfileViewActivity;
import com.smart.paywallet.models.output.GetAccountDetailsOutputModel;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyTextView;

import java.util.EnumMap;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_MOBILE;
import static com.smart.paywallet.utils.DataVaultManager.KEY_NAME;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;

public class ProfileActivity extends AppCompatActivity {

    MyTextView user_name;
    MyTextView user_mobile;
    ImageView user_qr_image;

    String strUserName, strUserMobile;

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    private MyTextView btnChangePassword, btnUpdateAddress, btnInvite, btnLogout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.profile_activity);
        setupActionBar();

        strUserName = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_NAME);
        strUserMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);


        user_name = (MyTextView) findViewById(R.id.user_name);
        user_qr_image = (ImageView) findViewById(R.id.user_qr_image);
        user_mobile = (MyTextView) findViewById(R.id.user_mobile);

        btnChangePassword = (MyTextView) findViewById(R.id.btnChangePassword);
        btnUpdateAddress = (MyTextView) findViewById(R.id.btnUpdateAddress);
        btnInvite = (MyTextView) findViewById(R.id.btnInvite);
        btnLogout = (MyTextView) findViewById(R.id.btnLogout);


        user_name.setText(strUserName);
        user_mobile.setText(strUserMobile);


        // barcode image
        Bitmap bitmap = null;

        try {

            bitmap = encodeAsBitmap(strUserMobile, BarcodeFormat.QR_CODE, 250, 250);
            user_qr_image.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }


        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent update = new Intent(ProfileActivity.this, ChangePasswordActivity.class);
                startActivity(update);

            }
        });

        btnUpdateAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent update = new Intent(ProfileActivity.this, UpdateCustomerAddress.class);
                startActivity(update);
            }
        });

        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this, R.style.MyAlertDialogStyle);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Are you sure to want to Logout ?");
                builder.setIcon(R.mipmap.ic_launcher);
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                logoutUserRequest();
                            }
                        });
                builder.setNegativeButton("No",

                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                                dialog.cancel();
                            }
                        });

                builder.show();

            }
        });

    }


    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }


    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Profile");

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void logoutUserRequest() {

        DataVaultManager.getInstance(ProfileActivity.this).saveName("");
        DataVaultManager.getInstance(ProfileActivity.this).saveUserEmail("");
        DataVaultManager.getInstance(ProfileActivity.this).saveUserMobile("");
        DataVaultManager.getInstance(ProfileActivity.this).saveUserPassword("");
        DataVaultManager.getInstance(ProfileActivity.this).saveUserId("");
        DataVaultManager.getInstance(ProfileActivity.this).saveUserLogo("");
        DataVaultManager.getInstance(ProfileActivity.this).saveWalletId("");

        Intent i = new Intent(ProfileActivity.this, SignInActivity.class);
        startActivity(i);
        finish();
    }
}
