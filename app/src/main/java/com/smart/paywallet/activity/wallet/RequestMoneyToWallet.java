package com.smart.paywallet.activity.wallet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.recyclerview.RequestListAdapter;
import com.smart.paywallet.adapter.wallet.BankTransferListAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.AllBankTransfers;
import com.smart.paywallet.models.output.CheckWalletOutput;
import com.smart.paywallet.models.output.IsSellerExist;
import com.smart.paywallet.models.output.RequestListOutput;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_MOBILE;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;

public class RequestMoneyToWallet extends AppCompatActivity {

    MainAPIInterface mainAPIInterface;
    MyTextView notFoundMessage;
    MyEditText edtMobileNumber, edtAmount, edtCustomerName;
    LinearLayout llSendRequestToWallet, ll_empty_sales_layout;

    RecyclerView allRequestList;
    ProgressDialog newProgressDialog;

    String xAccessToken = "mykey";

    String strMobileNumber, strRequestedAmount,strUserMobile;

    RequestListAdapter requestListAdapter;
    ArrayList<RequestListOutput.Request> requestArrayList;
    ProgressBar requestListProgressBar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_money_to_wallet);

        mainAPIInterface = ApiUtils.getAPIService();

        setupActionBar();

        notFoundMessage = (MyTextView) findViewById(R.id.notFoundMessage);
        edtMobileNumber = (MyEditText) findViewById(R.id.edtMobileNumber);
        edtCustomerName = (MyEditText) findViewById(R.id.edtCustomerName);
        edtAmount = (MyEditText) findViewById(R.id.edtAmount);

        llSendRequestToWallet = (LinearLayout) findViewById(R.id.llSendRequestToWallet);
        ll_empty_sales_layout = (LinearLayout) findViewById(R.id.ll_empty_sales_layout);

        allRequestList = (RecyclerView) findViewById(R.id.allRequestList);
        requestListProgressBar = (ProgressBar) findViewById(R.id.requestListProgressBar);

        strUserMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);


        edtMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                strMobileNumber = edtMobileNumber.getText().toString();
                if (strMobileNumber.length() == 10) {
                    getWalletDetails();
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        llSendRequestToWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edtMobileNumber.getText().toString().length() == 10) {

                    strRequestedAmount = edtAmount.getText().toString();

                    if (strRequestedAmount.length() < 0) {
                        edtAmount.setFocusable(true);
                        edtAmount.setError("Enter amount");
                    } else {
                        if (!edtMobileNumber.getText().toString().equalsIgnoreCase(strUserMobile)){
                            sendMoneyRequestToUser();
                        }else {
                            Toast.makeText(getApplicationContext()," You Cannot request money to your won wallet.",Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    edtMobileNumber.setFocusable(true);
                    edtMobileNumber.setError("Enter valid mobile number");
                }
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getAllSentRequests();
    }

    private void getWalletDetails() {
        newProgressDialog = new ProgressDialog(RequestMoneyToWallet.this);

        newProgressDialog.setMessage("Checking wallet details.");

        newProgressDialog.show();

        MultipartBody.Part mobile_body = MultipartBody.Part.createFormData("mobile", strMobileNumber);


        mainAPIInterface.checkIfWalletExist(xAccessToken, mobile_body).enqueue(new Callback<CheckWalletOutput>() {
            @Override
            public void onResponse(Call<CheckWalletOutput> call, Response<CheckWalletOutput> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        edtCustomerName.setVisibility(View.VISIBLE);
                        edtCustomerName.setText(response.body().getData().getUsername());
                        edtAmount.setVisibility(View.VISIBLE);
                    } else {
                        notFoundMessage.setVisibility(View.VISIBLE);
                        notFoundMessage.setText(response.body().getMessage());
                    }

                }
            }
            @Override
            public void onFailure(Call<CheckWalletOutput> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });
    }


    private void sendMoneyRequestToUser() {
        String strSenderMobileNumber = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        newProgressDialog = new ProgressDialog(RequestMoneyToWallet.this);

        newProgressDialog.setMessage("Sending your request.");

        newProgressDialog.show();

        MultipartBody.Part sender_mobile_body = MultipartBody.Part.createFormData("sender_mobile", strSenderMobileNumber);
        MultipartBody.Part receiver_mobile_body = MultipartBody.Part.createFormData("receiver_mobile", strMobileNumber);
        MultipartBody.Part requested_amount_body = MultipartBody.Part.createFormData("requested_amount", strRequestedAmount);

        mainAPIInterface.addPaymentRequest(xAccessToken, sender_mobile_body, receiver_mobile_body,
                requested_amount_body).enqueue(new Callback<IsSellerExist>() {
            @Override
            public void onResponse(Call<IsSellerExist> call, Response<IsSellerExist> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess() == 1) {

                        edtMobileNumber.setText("");
                        edtAmount.setText("");
                        edtCustomerName.setText("");
                        edtAmount.setVisibility(View.GONE);
                        edtCustomerName.setVisibility(View.GONE);
                        getAllSentRequests();

                        Toast.makeText(RequestMoneyToWallet.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    } else {

                        Toast.makeText(RequestMoneyToWallet.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<IsSellerExist> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });
    }

    private void getAllSentRequests() {

        String strCustomerMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        requestListProgressBar.setVisibility(View.VISIBLE);
        allRequestList.setVisibility(View.GONE);

        String xAccessToken = "mykey";

        MultipartBody.Part mobile_body = MultipartBody.Part.createFormData("mobile", strCustomerMobile);


        mainAPIInterface.getAllRequestList(xAccessToken, mobile_body).enqueue(new Callback<RequestListOutput>() {
            @Override
            public void onResponse(Call<RequestListOutput> call, Response<RequestListOutput> response) {

                if (response.isSuccessful()) {

                    requestListProgressBar.setVisibility(View.GONE);

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        allRequestList.setVisibility(View.VISIBLE);
                        ll_empty_sales_layout.setVisibility(View.GONE);

                        requestArrayList = response.body().getRequests();

                        requestListAdapter = new RequestListAdapter(requestArrayList, RequestMoneyToWallet.this);

                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        layoutManager.setOrientation(RecyclerView.VERTICAL);
//                            layoutManager.setStackFromEnd(true);

                        allRequestList.setLayoutManager(layoutManager);
                        allRequestList.setItemAnimator(new DefaultItemAnimator());
                        allRequestList.getItemAnimator().setChangeDuration(0);

                        //fix slow recyclerview start
                        allRequestList.setHasFixedSize(true);
                        allRequestList.setItemViewCacheSize(10);
                        allRequestList.setDrawingCacheEnabled(true);
                        allRequestList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                        ///fix slow recyclerview end

                        allRequestList.setAdapter(requestListAdapter);

                        requestListAdapter.notifyDataSetChanged();


                    } else {
                        ll_empty_sales_layout.setVisibility(View.VISIBLE);
                        allRequestList.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<RequestListOutput> call, Throwable t) {
                requestListProgressBar.setVisibility(View.GONE);
                allRequestList.setVisibility(View.GONE);
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Request Money");
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        toolbar.setTitleTextColor(Color.WHITE);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
