package com.smart.paywallet.activity.wallet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.grids.OperatorsGridViewAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.GetAllOperators;
import com.smart.paywallet.models.output.GetWalletTransactionsOutput;
import com.smart.paywallet.models.output.RechargeOutput;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.ExpandableHeightGridView;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;
import static com.smart.paywallet.utils.DataVaultManager.KEY_WALLET_ID;

public class GasBillPaymentActivity extends AppCompatActivity {

    ProgressDialog newProgressDialog;
    MainAPIInterface mainAPIInterface;

    ArrayList<GetAllOperators.Operator> operatorArrayList;

    ExpandableHeightGridView operatorsGrid;

    MyEditText edtconsumer_number, edtAmount;

    MyTextView edt_operator;

    OperatorsGridViewAdapter operatorsGridViewAdapter;

    String strOperatorCode, strOperatorName, strOperatorImage, strConsumerNumber, strAmount;

    MyTextView payButton;

    double mainWalletBalance;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.electricity_bill_pay_activity);
        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();

        edt_operator = (MyTextView) findViewById(R.id.edt_operator);
        edtconsumer_number = (MyEditText) findViewById(R.id.edtconsumer_number);
        edtAmount = (MyEditText) findViewById(R.id.edtAmount);
        payButton = (MyTextView) findViewById(R.id.payButton);

        edt_operator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOperators();
            }
        });

        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strConsumerNumber = edtconsumer_number.getText().toString();
                strAmount = edtAmount.getText().toString();

                if (strConsumerNumber.length() < 4) {
                    edtconsumer_number.setError("Enter consumer number");
                    edtconsumer_number.setFocusable(true);
                } else if (strAmount.length() < 0) {

                    edtAmount.setError("Enter amount");
                    edtAmount.setFocusable(true);

                } else {

                    if (Double.valueOf(strAmount) <= mainWalletBalance) {
                        placeBillPaymentRequest();

                    } else {
                        Toast.makeText(GasBillPaymentActivity.this,
                                "Your Wallet does not have sufficient funds.", Toast.LENGTH_SHORT).show();

                    }

                }


            }
        });

    }

    private void showOperators() {
        final Dialog mBottomSheetDialog = new Dialog(GasBillPaymentActivity.this);

        mBottomSheetDialog.setContentView(R.layout.custom_operator_dialog); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        ImageView closeDialog = mBottomSheetDialog.findViewById(R.id.closeDialog);

        operatorsGrid = mBottomSheetDialog.findViewById(R.id.operatorsGrid);

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBottomSheetDialog.dismiss();
            }
        });

        getOperators();

        operatorsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {

                strOperatorName = operatorArrayList.get(position).getOperatorName();
                strOperatorCode = operatorArrayList.get(position).getOperatorCode();
                strOperatorImage = operatorArrayList.get(position).getOperator_image();


                edt_operator.setText(strOperatorName);

                System.out.println("Operator Code==" + strOperatorCode + "\nOperator Name==" + strOperatorName);

                mBottomSheetDialog.dismiss();

            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Gas");

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getOperators() {
        String xAccessToken = "mykey";

        //4 For Electricity

        MultipartBody.Part operator_type_body = MultipartBody.Part.createFormData("operator_type", "5");

        mainAPIInterface.getAllOperators(xAccessToken, operator_type_body).enqueue(new Callback<GetAllOperators>() {
            @Override
            public void onResponse(Call<GetAllOperators> call, Response<GetAllOperators> response) {


                if (response.isSuccessful()) {


                    operatorArrayList = response.body().getOperators();

                    operatorsGridViewAdapter = new OperatorsGridViewAdapter(GasBillPaymentActivity.this,
                            operatorArrayList);

                    operatorsGrid.setExpanded(true);
                    operatorsGrid.setAdapter(operatorsGridViewAdapter);


                }
            }

            @Override
            public void onFailure(Call<GetAllOperators> call, Throwable t) {
                // dialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void placeBillPaymentRequest() {

        String xAccessToken = "mykey";

        String user_id = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);
        String strWalletId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);

        newProgressDialog = new ProgressDialog(GasBillPaymentActivity.this);

        newProgressDialog.setMessage("Placing your request.");

        newProgressDialog.show();


        System.out.println("OPERATOR_CODE=" + strOperatorCode);

        MultipartBody.Part mobile_body = MultipartBody.Part.createFormData("mobile", strConsumerNumber);
        MultipartBody.Part amount_body = MultipartBody.Part.createFormData("amount", strAmount);
        MultipartBody.Part operatorId_body = MultipartBody.Part.createFormData("operatorId", strOperatorCode);
        MultipartBody.Part user_id_body = MultipartBody.Part.createFormData("user_id", user_id);
        MultipartBody.Part wallet_id_body = MultipartBody.Part.createFormData("wallet_id", strWalletId);
        MultipartBody.Part recharge_type_body = MultipartBody.Part.createFormData("recharge_type", "5");

        mainAPIInterface.placeMobileRechargeRequest(xAccessToken, mobile_body,
                amount_body, operatorId_body, user_id_body, wallet_id_body,
                recharge_type_body).enqueue(new Callback<RechargeOutput>() {
            @Override
            public void onResponse(Call<RechargeOutput> call, Response<RechargeOutput> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        Toast.makeText(GasBillPaymentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(GasBillPaymentActivity.this, ThankYouRechargeDone.class);
                        intent.putExtra("strOperatorCode", strOperatorCode);
                        intent.putExtra("strOperatorName", strOperatorName);
                        intent.putExtra("strOperatorImage", strOperatorImage);
                        intent.putExtra("amount", strAmount);
                        intent.putExtra("phone", strConsumerNumber);
                        intent.putExtra("order_id", response.body().getOrderId().toString());

                        startActivity(intent);
                        finish();

                    } else {

                        Toast.makeText(GasBillPaymentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(GasBillPaymentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RechargeOutput> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        getWalletBalance();
    }

    private void getWalletBalance() {

        String xAccessToken = "mykey";

        String wallet_id = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);

        MultipartBody.Part seller_id_body = MultipartBody.Part.createFormData("wallet_id", wallet_id);

        mainAPIInterface.getWalletTransactions(xAccessToken, seller_id_body).enqueue(new Callback<GetWalletTransactionsOutput>() {
            @Override
            public void onResponse(Call<GetWalletTransactionsOutput> call, Response<GetWalletTransactionsOutput> response) {

                if (response.isSuccessful()) {

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        mainWalletBalance = Double.valueOf(response.body().getBalance());

                    }

                }
            }

            @Override
            public void onFailure(Call<GetWalletTransactionsOutput> call, Throwable t) {
                Log.i("tag", t.getMessage().toString());
            }
        });

    }
}
