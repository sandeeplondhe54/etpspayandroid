package com.smart.paywallet.activity.wallet;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.snackbar.Snackbar;
import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.RechargeOutput;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ThankYouRechargeDone extends AppCompatActivity {

    CoordinatorLayout coordinatorLayout;

    MyTextView rechargeOperatorName, rechargeNumber, rechargeOrderId, rechargeTime;

    ImageView rechargeOperatorImage;

    MyTextView rechargeOperatorName2, rechargeAmount, rechargeStatusFromServer;

    ProgressBar rechargeStatusProgress;

    MyTextView btnRechargeAnother, btnRechargeGoBack;

    MainAPIInterface mainAPIInterface;

    String strOperatorCode, strOperatorName, strOperatorImage, amount, phone, order_id;

    ImageView successImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.recharge_done_activity);
        setupActionBar();

        Intent value = getIntent();

        strOperatorCode = value.getExtras().getString("strOperatorCode");
        strOperatorName = value.getExtras().getString("strOperatorName");
        strOperatorImage = value.getExtras().getString("strOperatorImage");
        amount = value.getExtras().getString("amount");
        phone = value.getExtras().getString("phone");
        order_id = value.getExtras().getString("order_id");

        mainAPIInterface = ApiUtils.getAPIService();

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Payment Done Successfully", Snackbar.LENGTH_LONG);

        snackbar.show();

        rechargeOperatorName = (MyTextView) findViewById(R.id.rechargeOperatorName);
        rechargeOperatorName2 = (MyTextView) findViewById(R.id.rechargeOperatorName2);
        rechargeNumber = (MyTextView) findViewById(R.id.rechargeNumber);
        rechargeOrderId = (MyTextView) findViewById(R.id.rechargeOrderId);
        rechargeTime = (MyTextView) findViewById(R.id.rechargeTime);
        rechargeOperatorImage = (ImageView) findViewById(R.id.rechargeOperatorImage);
        rechargeAmount = (MyTextView) findViewById(R.id.rechargeAmount);
        rechargeStatusFromServer = (MyTextView) findViewById(R.id.rechargeStatusFromServer);
        successImage = (ImageView) findViewById(R.id.successImage);

        rechargeStatusProgress = (ProgressBar) findViewById(R.id.rechargeStatusProgress);

        btnRechargeAnother = (MyTextView) findViewById(R.id.btnRechargeAnother);
        btnRechargeGoBack = (MyTextView) findViewById(R.id.btnRechargeGoBack);

        rechargeOperatorName.setText(strOperatorName);
        rechargeOperatorName2.setText(strOperatorName);

        rechargeNumber.setText(phone);
        rechargeOrderId.setText("Order Id: " + order_id);

        Picasso.with(ThankYouRechargeDone.this)
                .load(Constants.OPERATORS_IMAGE_PATH + strOperatorImage)
                .placeholder(R.drawable.placeholder)
                .into(rechargeOperatorImage);

        rechargeAmount.setText(SmartPayApplication.CURRENCY_SYMBOL + amount);


        btnRechargeAnother.setVisibility(View.GONE);

        btnRechargeAnother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent newIntent = new Intent(ThankYouRechargeDone.this, MobileRechargeActivity.class);
                startActivity(newIntent);
                finish();

            }
        });

        btnRechargeGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        checkRechargeStatusRequest();

    }

    private void checkRechargeStatusRequest() {
        String xAccessToken = "mykey";

        rechargeStatusProgress.setVisibility(View.VISIBLE);
        successImage.setVisibility(View.GONE);

        MultipartBody.Part orderid_body = MultipartBody.Part.createFormData("orderid", order_id);


        mainAPIInterface.checkRechargeStatus(xAccessToken, orderid_body).enqueue(new Callback<RechargeOutput>() {
            @Override
            public void onResponse(Call<RechargeOutput> call, Response<RechargeOutput> response) {

                if (response.isSuccessful()) {

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        rechargeStatusProgress.setVisibility(View.GONE);
                        rechargeStatusFromServer.setText("Recharge Successfull");
                        successImage.setVisibility(View.VISIBLE);

                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {

                        rechargeStatusProgress.setVisibility(View.GONE);
                        rechargeStatusFromServer.setText("Recharge Pending");
                        successImage.setVisibility(View.VISIBLE);

                    } else {
                        rechargeStatusProgress.setVisibility(View.GONE);
                        rechargeStatusFromServer.setText("Recharge Failed");
                        successImage.setVisibility(View.GONE);

                    }


                }
            }

            @Override
            public void onFailure(Call<RechargeOutput> call, Throwable t) {
                rechargeStatusProgress.setVisibility(View.GONE);
                successImage.setVisibility(View.GONE);

                Log.i("tag", t.getMessage().toString());
            }
        });


    }


    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Thank you");

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
