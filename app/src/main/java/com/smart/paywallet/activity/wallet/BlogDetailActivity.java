package com.smart.paywallet.activity.wallet;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.smart.paywallet.R;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.views.MyTextView;

public class BlogDetailActivity extends AppCompatActivity {
    ProgressDialog newProgressDialog;
    MainAPIInterface mainAPIInterface;
    MyTextView inbox_title;
    TextView message_time,txt_article_likes,txt_article_share;
    ImageView  inbox_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);

        setupActionBar();

        inbox_title = (MyTextView)findViewById(R.id.inbox_title);
        message_time =(TextView)findViewById(R.id.message_time);
        txt_article_likes =(TextView)findViewById(R.id.txt_article_likes);
        txt_article_share =(TextView)findViewById(R.id.txt_article_share);
        inbox_image =(ImageView)findViewById(R.id.inbox_image);
    }


    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Blog Details");
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

                default:
                   return super.onOptionsItemSelected(item);
        }
    }
}
