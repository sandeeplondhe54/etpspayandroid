package com.smart.paywallet.activity.wallet;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.qhutch.bottomsheetlayout.BottomSheetLayout;
import com.smart.paywallet.R;
import com.smart.paywallet.activity.mall.AllBrandsActivity;
import com.smart.paywallet.adapter.mall.AllBrandGridAdapter;
import com.smart.paywallet.adapter.mall.GiftVoucherAdapter;
import com.smart.paywallet.adapter.mall.GiftVoucherCategoriesAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.GetAllGiftVouchers;
import com.smart.paywallet.models.output.GiftVoucherCategories;
import com.smart.paywallet.views.ExpandableHeightGridView;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftVoucherAll extends AppCompatActivity {

    MainAPIInterface mainAPIInterface;
    ProgressBar categoriesProgressBar;
    LinearLayout categoriesLayout;

    CircleImageView cat1_image, cat2_image, cat3_image, catmore_image;
    MyTextView cat1_title, cat2_title, cat3_title;

    ArrayList<GiftVoucherCategories.Category> categoryArrayList;

    GiftVoucherCategoriesAdapter giftVoucherCategoriesAdapter;
    ExpandableHeightGridView voucherCategoriesGrid;

    private BottomSheetBehavior mBehavior;
    private View bottom_sheet_layout;
    private BottomSheetDialog mBottomSheetDialog;
    private BottomSheetBehavior mDialogBehavior;

    ProgressBar brandVoucherProgressBar;
    ExpandableHeightGridView brandVoucherGridView;
    ArrayList<GetAllGiftVouchers.GiftVoucher> giftVoucherArrayList;
    GiftVoucherAdapter giftVoucherAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voucher_all_activity);

        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();

        cat1_image = (CircleImageView) findViewById(R.id.cat1_image);
        cat2_image = (CircleImageView) findViewById(R.id.cat2_image);
        cat3_image = (CircleImageView) findViewById(R.id.cat3_image);

        catmore_image = (CircleImageView) findViewById(R.id.catmore_image);

        cat1_title = (MyTextView) findViewById(R.id.cat1_title);
        cat2_title = (MyTextView) findViewById(R.id.cat2_title);
        cat3_title = (MyTextView) findViewById(R.id.cat3_title);

        categoriesProgressBar = (ProgressBar) findViewById(R.id.categoriesProgressBar);
        categoriesLayout = (LinearLayout) findViewById(R.id.categoriesLayout);
        bottom_sheet_layout = (LinearLayout) findViewById(R.id.bottom_sheet_layout);
        voucherCategoriesGrid = (ExpandableHeightGridView) findViewById(R.id.voucherCategoriesGrid);
        brandVoucherGridView = (ExpandableHeightGridView) findViewById(R.id.brandVoucherGridView);
        brandVoucherProgressBar = (ProgressBar) findViewById(R.id.brandVoucherProgressBar);

        getVoucherCategories();

        mBehavior = BottomSheetBehavior.from(bottom_sheet_layout);
        mBehavior.setHideable(true);


        cat1_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(GiftVoucherAll.this, GiftVoucherCategoryAll.class);

                i.putExtra("category_name", cat1_title.getText().toString());
                i.putExtra("category_id", "1");

                startActivity(i);
            }
        });

        cat2_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(GiftVoucherAll.this, GiftVoucherCategoryAll.class);

                i.putExtra("category_name", cat2_title.getText().toString());
                i.putExtra("category_id", "2");

                startActivity(i);
            }
        });

        cat3_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(GiftVoucherAll.this, GiftVoucherCategoryAll.class);

                i.putExtra("category_name", cat3_title.getText().toString());
                i.putExtra("category_id", "3");

                startActivity(i);
            }
        });


        catmore_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//
//                if (mBottomSheetDialog != null) {
//                    mBottomSheetDialog.dismiss();
//                }

                if (mBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                    System.out.println("SHEET OPEN");

                } else {
                    System.out.println("SHEET HIDDEN");

                    mBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

                    mBehavior.setPeekHeight(0);
                }
            }
        });

        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        getBrandVouchers();
    }


    public void getVoucherCategories() {
        String xAccessToken = "mykey";

        categoriesProgressBar.setVisibility(View.VISIBLE);
        categoriesLayout.setVisibility(View.GONE);

        mainAPIInterface.getAllGiftVoucherCategories(xAccessToken).enqueue(new Callback<GiftVoucherCategories>() {
            @Override
            public void onResponse(Call<GiftVoucherCategories> call, Response<GiftVoucherCategories> response) {


                if (response.isSuccessful()) {

                    categoriesProgressBar.setVisibility(View.GONE);
                    categoriesLayout.setVisibility(View.VISIBLE);

                    Picasso.with(GiftVoucherAll.this)
                            .load(response.body().getCategories().get(0).getCategoryIcon())
                            .placeholder(R.drawable.placeholder)
                            .into(cat1_image);

                    Picasso.with(GiftVoucherAll.this)
                            .load(response.body().getCategories().get(1).getCategoryIcon())
                            .placeholder(R.drawable.placeholder)
                            .into(cat2_image);

                    Picasso.with(GiftVoucherAll.this)
                            .load(response.body().getCategories().get(3).getCategoryIcon())
                            .placeholder(R.drawable.placeholder)
                            .into(cat3_image);


                    cat1_title.setText(response.body().getCategories().get(0).getCategoryName());
                    cat2_title.setText(response.body().getCategories().get(1).getCategoryName());
                    cat3_title.setText(response.body().getCategories().get(3).getCategoryName());

                    categoryArrayList = response.body().getCategories();


                    giftVoucherCategoriesAdapter = new GiftVoucherCategoriesAdapter(GiftVoucherAll.this, categoryArrayList);

                    voucherCategoriesGrid.setExpanded(true);

                    voucherCategoriesGrid.setAdapter(giftVoucherCategoriesAdapter);

                }
            }

            @Override
            public void onFailure(Call<GiftVoucherCategories> call, Throwable t) {
                categoriesProgressBar.setVisibility(View.GONE);
                categoriesLayout.setVisibility(View.VISIBLE);
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    public void getBrandVouchers() {
        String xAccessToken = "mykey";

        brandVoucherProgressBar.setVisibility(View.VISIBLE);
        brandVoucherGridView.setVisibility(View.GONE);

        mainAPIInterface.getAllVouchers(xAccessToken).enqueue(new Callback<GetAllGiftVouchers>() {
            @Override
            public void onResponse(Call<GetAllGiftVouchers> call, Response<GetAllGiftVouchers> response) {


                if (response.isSuccessful()) {

                    brandVoucherProgressBar.setVisibility(View.GONE);
                    brandVoucherGridView.setVisibility(View.VISIBLE);

                    giftVoucherArrayList = response.body().getVouchers();

                    giftVoucherAdapter = new GiftVoucherAdapter(GiftVoucherAll.this, giftVoucherArrayList);

                    brandVoucherGridView.setExpanded(true);

                    brandVoucherGridView.setAdapter(giftVoucherAdapter);

                }
            }

            @Override
            public void onFailure(Call<GetAllGiftVouchers> call, Throwable t) {
                brandVoucherProgressBar.setVisibility(View.GONE);
                brandVoucherGridView.setVisibility(View.VISIBLE);
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);

        ImageView appLogo = toolbar.findViewById(R.id.appLogo);
        appLogo.setVisibility(View.GONE);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("All Gift Vouchers");


        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
