package com.smart.paywallet.activity.wallet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.wallet.SupportMessageListAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.IsSellerExist;
import com.smart.paywallet.models.output.SupportTicketMessagesList;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_NAME;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;


public class SupportTicketDetails extends AppCompatActivity {

    MainAPIInterface mainAPIInterface;

    String ticket_id;

    RecyclerView listMessages;

    ArrayList<SupportTicketMessagesList.Ticket> ticketArrayList;
    SupportMessageListAdapter supportMessageListAdapter;

    ProgressBar message_progressbar;

    ImageButton send_button;

    MyEditText MessageWrapper;


    String strMessageText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support_ticket_message_list);

        Intent value = getIntent();

        ticket_id = value.getExtras().getString("ticket_id");


        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();

        listMessages = (RecyclerView) findViewById(R.id.listMessages);
        message_progressbar = (ProgressBar) findViewById(R.id.message_progressbar);

        send_button = (ImageButton) findViewById(R.id.send_button);
        MessageWrapper = (MyEditText) findViewById(R.id.MessageWrapper);


        getAllMessages();

        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strMessageText = MessageWrapper.getText().toString();

                if (strMessageText.length() < 1) {
                    MessageWrapper.setFocusable(true);
                    MessageWrapper.setError("Please enter a message");

                } else {

                    MessageWrapper.setText("");
                    sendAndUpdateTicket();
                }

            }
        });

    }


    public void getAllMessages() {

        String strUserId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);

        String xAccessToken = "mykey";

        message_progressbar.setVisibility(View.VISIBLE);
        listMessages.setVisibility(View.GONE);

        MultipartBody.Part ticket_id_body = MultipartBody.Part.createFormData("ticket_id", ticket_id);

        mainAPIInterface.getAllSupportTickeDetail(xAccessToken, ticket_id_body).enqueue(new Callback<SupportTicketMessagesList>() {
            @Override
            public void onResponse(Call<SupportTicketMessagesList> call, Response<SupportTicketMessagesList> response) {


                if (response.isSuccessful()) {

                    message_progressbar.setVisibility(View.GONE);
                    listMessages.setVisibility(View.VISIBLE);

                    ticketArrayList = response.body().getTicket();


                    supportMessageListAdapter = new SupportMessageListAdapter(ticketArrayList, SupportTicketDetails.this);


                    LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    layoutManager.setStackFromEnd(true);

                    listMessages.setLayoutManager(layoutManager);
                    listMessages.setItemAnimator(new DefaultItemAnimator());
                    listMessages.getItemAnimator().setChangeDuration(0);

                    //fix slow recyclerview start
                    listMessages.setHasFixedSize(true);
                    listMessages.setItemViewCacheSize(10);
                    listMessages.setDrawingCacheEnabled(true);
                    listMessages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                    ///fix slow recyclerview end

                    listMessages.setAdapter(supportMessageListAdapter);

                    supportMessageListAdapter.notifyDataSetChanged();


                }
            }

            @Override
            public void onFailure(Call<SupportTicketMessagesList> call, Throwable t) {
                message_progressbar.setVisibility(View.GONE);
                listMessages.setVisibility(View.VISIBLE);
                Log.i("tag", t.getMessage().toString());
            }
        });
    }

    private void sendAndUpdateTicket() {

        String strUserId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);
        String strUserName = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_NAME);

        String xAccessToken = "mykey";

        message_progressbar.setVisibility(View.VISIBLE);
        listMessages.setVisibility(View.GONE);

        MultipartBody.Part customer_id_body = MultipartBody.Part.createFormData("customer_id", strUserId);
        MultipartBody.Part customer_name_body = MultipartBody.Part.createFormData("customer_name", strUserName);
        MultipartBody.Part message_text_body = MultipartBody.Part.createFormData("message_text", strMessageText);
        MultipartBody.Part ticket_id_body = MultipartBody.Part.createFormData("ticket_id", ticket_id);

        mainAPIInterface.sendSupportTicketSendMessage(xAccessToken, customer_id_body,
                customer_name_body, message_text_body, ticket_id_body).enqueue(new Callback<IsSellerExist>() {
            @Override
            public void onResponse(Call<IsSellerExist> call, Response<IsSellerExist> response) {


                if (response.isSuccessful()) {

                    if (response.body().getSuccess() == 1) {

                        Toast.makeText(SupportTicketDetails.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        MessageWrapper.setText("");
                        getAllMessages();

                    } else {
                        Toast.makeText(SupportTicketDetails.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<IsSellerExist> call, Throwable t) {
                message_progressbar.setVisibility(View.GONE);
                listMessages.setVisibility(View.VISIBLE);
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);

        ImageView appLogo = toolbar.findViewById(R.id.appLogo);
        appLogo.setVisibility(View.GONE);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("TICKET ID:- " + ticket_id);


        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        toolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
