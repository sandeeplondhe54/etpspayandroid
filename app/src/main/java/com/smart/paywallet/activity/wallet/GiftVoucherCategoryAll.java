package com.smart.paywallet.activity.wallet;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.smart.paywallet.R;
import com.smart.paywallet.adapter.mall.GiftVoucherAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.GetAllGiftVouchers;
import com.smart.paywallet.views.ExpandableHeightGridView;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftVoucherCategoryAll extends AppCompatActivity {

    String category_name, category_id;

    MainAPIInterface mainAPIInterface;

    ProgressBar brandVoucherProgressBar;
    ExpandableHeightGridView brandVoucherGridView;
    ArrayList<GetAllGiftVouchers.GiftVoucher> giftVoucherArrayList;
    GiftVoucherAdapter giftVoucherAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voucher_category_all_activity);

        Intent value = getIntent();

        category_id = value.getExtras().getString("category_id");
        category_name = value.getExtras().getString("category_name");


        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();

        brandVoucherProgressBar = (ProgressBar) findViewById(R.id.brandVoucherProgressBar);
        brandVoucherGridView = (ExpandableHeightGridView) findViewById(R.id.brandVoucherGridView);


        getBrandVouchers();

    }

    public void getBrandVouchers() {
        String xAccessToken = "mykey";

        brandVoucherProgressBar.setVisibility(View.VISIBLE);
        brandVoucherGridView.setVisibility(View.GONE);

        MultipartBody.Part category_id_body = MultipartBody.Part.createFormData("category_id", category_id);


        mainAPIInterface.getGiftVoucherByCategory(xAccessToken, category_id_body).enqueue(new Callback<GetAllGiftVouchers>() {
            @Override
            public void onResponse(Call<GetAllGiftVouchers> call, Response<GetAllGiftVouchers> response) {


                if (response.isSuccessful()) {

                    brandVoucherProgressBar.setVisibility(View.GONE);
                    brandVoucherGridView.setVisibility(View.VISIBLE);

                    giftVoucherArrayList = response.body().getVouchers();

                    giftVoucherAdapter = new GiftVoucherAdapter(GiftVoucherCategoryAll.this, giftVoucherArrayList);

                    brandVoucherGridView.setExpanded(true);

                    brandVoucherGridView.setAdapter(giftVoucherAdapter);

                }
            }

            @Override
            public void onFailure(Call<GetAllGiftVouchers> call, Throwable t) {
                brandVoucherProgressBar.setVisibility(View.GONE);
                brandVoucherGridView.setVisibility(View.VISIBLE);
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);

        ImageView appLogo = toolbar.findViewById(R.id.appLogo);
        appLogo.setVisibility(View.GONE);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText(category_name);


        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
