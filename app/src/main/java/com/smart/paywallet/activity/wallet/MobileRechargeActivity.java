package com.smart.paywallet.activity.wallet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.wallet.BankTransferListAdapter;
import com.smart.paywallet.adapter.wallet.BeneficiaryListAdapter;
import com.smart.paywallet.adapter.wallet.RechargeSpinnerAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.fragments.orders.FragmentDealsOrders;
import com.smart.paywallet.fragments.orders.FragmentGiftCards;
import com.smart.paywallet.fragments.orders.FragmentRechargeOrders;
import com.smart.paywallet.fragments.orders.FragmentShoppingOrders;
import com.smart.paywallet.fragments.orders.FragmentTicketsOrder;
import com.smart.paywallet.fragments.orders.FragmentTravelOrders;
import com.smart.paywallet.models.input.MobileRechargeModel;
import com.smart.paywallet.models.output.AllBankTransfers;
import com.smart.paywallet.models.output.AllPlans;
import com.smart.paywallet.models.output.CommonOutput;
import com.smart.paywallet.models.output.GetAllOperators;
import com.smart.paywallet.models.output.GetWalletTransactionsOutput;
import com.smart.paywallet.models.output.MoneyTransferCustomerDetails;
import com.smart.paywallet.models.output.RechargeOutput;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;
import static com.smart.paywallet.utils.DataVaultManager.KEY_WALLET_ID;

public class MobileRechargeActivity extends AppCompatActivity {

    MyTextView payButton;

    Switch rechargeSwitch;

    MyEditText edtphone_number;
    Spinner edtOperator;
    static MyEditText edtAmount;

    MyTextView btnSeePlans;

    String strPhone;
    static String strOperatorCode;
    String strOperatorName;
    String strOperatorImage;
    String strAmount;
    String strPlan;
    String recharge_type = "1";
    String strWalletId;

    ProgressDialog newProgressDialog;
    static MainAPIInterface mainAPIInterface;
    RechargeSpinnerAdapter rechargeSpinnerAdapter;
    ArrayList<GetAllOperators.Operator> operatorArrayList;

    public static DialogFragment newFragment;

    public static ArrayList<AllPlans.Plans> plansArrayList;
    public static PlansListAdapter plansListAdapter;

    double mainWalletBalance;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.recharge_activity);
        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();

        payButton = (MyTextView) findViewById(R.id.payButton);
        rechargeSwitch = (Switch) findViewById(R.id.rechargeSwitch);

        edtphone_number = (MyEditText) findViewById(R.id.edtphone_number);
        edtOperator = (Spinner) findViewById(R.id.edtOperator);
        edtAmount = (MyEditText) findViewById(R.id.edtAmount);

        btnSeePlans = (MyTextView) findViewById(R.id.btnSeePlans);

        rechargeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                Log.v("Switch State=", "" + isChecked);

                if (isChecked) {
                    recharge_type = "2";
                    getPostpaidOperators();

                } else {
                    recharge_type = "1";
                    getOperators();
                }

            }
        });

        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strPhone = edtphone_number.getText().toString();
                //  strOperatorCode = edtOperator.getText().toString();

                strAmount = edtAmount.getText().toString();

                if (strPhone.length() == 0) {
                    edtphone_number.setError("Enter mobile number");
                    edtphone_number.setFocusable(true);
                } else if (strOperatorCode.length() == 0) {

                    Toast.makeText(MobileRechargeActivity.this, "Please select the operator",
                            Toast.LENGTH_SHORT).show();

                } else if (strAmount.length() == 0) {
                    edtAmount.setFocusable(true);
                    edtAmount.setError("Enter the amount");
                } else {

                    if (Double.valueOf(strAmount) <= mainWalletBalance) {
                        placeRechargeRequest();

                    } else {
                        Toast.makeText(MobileRechargeActivity.this,
                                "Your Wallet does not have sufficient funds.", Toast.LENGTH_SHORT).show();

                    }
                }

            }
        });

        getOperators();

        btnSeePlans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                newFragment = PlansDialogFragment.newInstance();
                newFragment.show(ft, "dialog");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getWalletBalance();
    }

    private void placeRechargeRequest() {


        String xAccessToken = "mykey";

        String user_id = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);
        strWalletId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);

        newProgressDialog = new ProgressDialog(MobileRechargeActivity.this);

        newProgressDialog.setMessage("Placing your request.");

        newProgressDialog.show();


        System.out.println("OPERATOR_CODE=" + strOperatorCode);

        MultipartBody.Part mobile_body = MultipartBody.Part.createFormData("mobile", strPhone);
        MultipartBody.Part amount_body = MultipartBody.Part.createFormData("amount", strAmount);
        MultipartBody.Part operatorId_body = MultipartBody.Part.createFormData("operatorId", strOperatorCode);
        MultipartBody.Part user_id_body = MultipartBody.Part.createFormData("user_id", user_id);
        MultipartBody.Part wallet_id_body = MultipartBody.Part.createFormData("wallet_id", strWalletId);
        MultipartBody.Part recharge_type_body = MultipartBody.Part.createFormData("recharge_type", recharge_type);


        mainAPIInterface.placeMobileRechargeRequest(xAccessToken, mobile_body,
                amount_body, operatorId_body, user_id_body, wallet_id_body,
                recharge_type_body).enqueue(new Callback<RechargeOutput>() {
            @Override
            public void onResponse(Call<RechargeOutput> call, Response<RechargeOutput> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        Toast.makeText(MobileRechargeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(MobileRechargeActivity.this, ThankYouRechargeDone.class);
                        intent.putExtra("strOperatorCode", strOperatorCode);
                        intent.putExtra("strOperatorName", strOperatorName);
                        intent.putExtra("strOperatorImage", strOperatorImage);
                        intent.putExtra("amount", strAmount);
                        intent.putExtra("phone", strPhone);
                        intent.putExtra("order_id", response.body().getOrderId().toString());

                        startActivity(intent);
                        finish();

                    } else {

                        Toast.makeText(MobileRechargeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(MobileRechargeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RechargeOutput> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Phone Recharge");

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


                default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getOperators() {
        String xAccessToken = "mykey";

        MultipartBody.Part operator_type_body = MultipartBody.Part.createFormData("operator_type", "1");


        mainAPIInterface.getAllOperators(xAccessToken, operator_type_body).enqueue(new Callback<GetAllOperators>() {
            @Override
            public void onResponse(Call<GetAllOperators> call, Response<GetAllOperators> response) {


                if (response.isSuccessful()) {

                    operatorArrayList = response.body().getOperators();

                    rechargeSpinnerAdapter = new RechargeSpinnerAdapter(MobileRechargeActivity.this, R.layout.recharge_spinner_layout, operatorArrayList);

                    edtOperator.setAdapter(rechargeSpinnerAdapter);

                    edtOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                            // your code here

                            strOperatorCode = operatorArrayList.get(position).getOperatorCode();
                            strOperatorName = operatorArrayList.get(position).getOperatorName();
                            strOperatorImage = operatorArrayList.get(position).getOperator_image();

                            System.out.println("OPERATOR_CODE=" + strOperatorCode);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });

                }
            }

            @Override
            public void onFailure(Call<GetAllOperators> call, Throwable t) {
                // dialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void getPostpaidOperators() {
        String xAccessToken = "mykey";

        MultipartBody.Part operator_type_body = MultipartBody.Part.createFormData("operator_type", "2");

        mainAPIInterface.getAllOperators(xAccessToken, operator_type_body).enqueue(new Callback<GetAllOperators>() {
            @Override
            public void onResponse(Call<GetAllOperators> call, Response<GetAllOperators> response) {


                if (response.isSuccessful()) {

                    operatorArrayList = response.body().getOperators();

                    rechargeSpinnerAdapter = new RechargeSpinnerAdapter(MobileRechargeActivity.this, R.layout.recharge_spinner_layout, operatorArrayList);

                    edtOperator.setAdapter(rechargeSpinnerAdapter);

                    edtOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                            // your code here

                            strOperatorCode = operatorArrayList.get(position).getOperatorCode();
                            strOperatorName = operatorArrayList.get(position).getOperatorName();
                            strOperatorImage = operatorArrayList.get(position).getOperator_image();

                            System.out.println("OPERATOR_CODE=" + strOperatorCode);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });


                }
            }

            @Override
            public void onFailure(Call<GetAllOperators> call, Throwable t) {
                // dialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    public static class PlansDialogFragment extends DialogFragment {
        ImageView closeDialog;
        TabLayout plans_tabs;
        ViewPager plans_viewpager;

        static PlansDialogFragment newInstance() {
            PlansDialogFragment f = new PlansDialogFragment();
            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.plans_fragment_dialog, container, false);

            closeDialog = (ImageView) v.findViewById(R.id.closeDialog);
            plans_tabs = (TabLayout) v.findViewById(R.id.plans_tabs);
            plans_viewpager = (ViewPager) v.findViewById(R.id.plans_viewpager);

            closeDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    newFragment.dismiss();
                }
            });


            setupViewPager(plans_viewpager);
            plans_tabs.setupWithViewPager(plans_viewpager);
            plans_tabs.setTabGravity(TabLayout.GRAVITY_FILL);

            return v;
        }

        private void setupViewPager(ViewPager viewPager) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
            adapter.addFragment(new FragmentSpecialRecharge(), "Special");
            adapter.addFragment(new FragmentDataRecharge(), "Data 3G/4G");
            adapter.addFragment(new FragmentFullTalktimeRecharge(), "Full Talktime");
            adapter.addFragment(new FragmentTupRecharge(), "Regular Talktime");
            adapter.addFragment(new FragmentRoamingRecharge(), "Roaming");
            viewPager.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    public static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public static class FragmentSpecialRecharge extends Fragment {

        View mView;
        RecyclerView plansList;
        ProgressBar plansShowProgressBar;
        RelativeLayout empty_layout;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            mView = inflater.inflate(R.layout.plan_fragment_special, container, false);

            plansList = (RecyclerView) mView.findViewById(R.id.plansList);
            plansShowProgressBar = (ProgressBar) mView.findViewById(R.id.plansShowProgressBar);
            empty_layout = (RelativeLayout) mView.findViewById(R.id.empty_layout);

            getAllPlansRequest();
            return mView;
        }


        private void getAllPlansRequest() {

            plansShowProgressBar.setVisibility(View.VISIBLE);
            plansList.setVisibility(View.GONE);

            String xAccessToken = "mykey";

            MultipartBody.Part rechType_body = MultipartBody.Part.createFormData("rechType", "SPL");
            MultipartBody.Part circleCode_body = MultipartBody.Part.createFormData("circleCode", "1");
            MultipartBody.Part opid_body = MultipartBody.Part.createFormData("opid", strOperatorCode);

            mainAPIInterface.getAllRechargePlans(xAccessToken, rechType_body, circleCode_body,
                    opid_body).enqueue(new Callback<AllPlans>() {
                @Override
                public void onResponse(Call<AllPlans> call, Response<AllPlans> response) {

                    if (response.isSuccessful()) {

                        plansShowProgressBar.setVisibility(View.GONE);

                        if (response.body().getSuccess().equalsIgnoreCase("1")) {
                            plansList.setVisibility(View.VISIBLE);
                            empty_layout.setVisibility(View.GONE);

                            plansArrayList = response.body().getData();

                            plansListAdapter = new PlansListAdapter(plansArrayList, getActivity());

                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
//                            layoutManager.setStackFromEnd(true);

                            plansList.setLayoutManager(layoutManager);
                            plansList.setItemAnimator(new DefaultItemAnimator());
                            plansList.getItemAnimator().setChangeDuration(0);

                            //fix slow recyclerview start
                            plansList.setHasFixedSize(true);
                            plansList.setItemViewCacheSize(10);
                            plansList.setDrawingCacheEnabled(true);
                            plansList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                            ///fix slow recyclerview end

                            plansList.setAdapter(plansListAdapter);

                            plansListAdapter.notifyDataSetChanged();


                        } else {
                            empty_layout.setVisibility(View.VISIBLE);
                            plansList.setVisibility(View.GONE);
                        }

                    }
                }

                @Override
                public void onFailure(Call<AllPlans> call, Throwable t) {
                    plansShowProgressBar.setVisibility(View.GONE);
                    plansList.setVisibility(View.VISIBLE);
                    Log.i("tag", t.getMessage().toString());
                }
            });


        }
    }

    public static class FragmentDataRecharge extends Fragment {

        View mView;
        RecyclerView plansList;
        ProgressBar plansShowProgressBar;
        RelativeLayout empty_layout;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            mView = inflater.inflate(R.layout.plan_fragment_special, container, false);

            plansList = (RecyclerView) mView.findViewById(R.id.plansList);
            plansShowProgressBar = (ProgressBar) mView.findViewById(R.id.plansShowProgressBar);
            empty_layout = (RelativeLayout) mView.findViewById(R.id.empty_layout);

            getAllPlansRequest();
            return mView;
        }


        private void getAllPlansRequest() {

            plansShowProgressBar.setVisibility(View.VISIBLE);
            plansList.setVisibility(View.GONE);

            String xAccessToken = "mykey";

            MultipartBody.Part rechType_body = MultipartBody.Part.createFormData("rechType", "DATA");
            MultipartBody.Part circleCode_body = MultipartBody.Part.createFormData("circleCode", "1");
            MultipartBody.Part opid_body = MultipartBody.Part.createFormData("opid", strOperatorCode);

            mainAPIInterface.getAllRechargePlans(xAccessToken, rechType_body, circleCode_body,
                    opid_body).enqueue(new Callback<AllPlans>() {
                @Override
                public void onResponse(Call<AllPlans> call, Response<AllPlans> response) {

                    if (response.isSuccessful()) {

                        plansShowProgressBar.setVisibility(View.GONE);

                        if (response.body().getSuccess().equalsIgnoreCase("1")) {
                            plansList.setVisibility(View.VISIBLE);
                            empty_layout.setVisibility(View.GONE);

                            plansArrayList = response.body().getData();

                            plansListAdapter = new PlansListAdapter(plansArrayList, getActivity());

                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
//                            layoutManager.setStackFromEnd(true);

                            plansList.setLayoutManager(layoutManager);
                            plansList.setItemAnimator(new DefaultItemAnimator());
                            plansList.getItemAnimator().setChangeDuration(0);

                            //fix slow recyclerview start
                            plansList.setHasFixedSize(true);
                            plansList.setItemViewCacheSize(10);
                            plansList.setDrawingCacheEnabled(true);
                            plansList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                            ///fix slow recyclerview end

                            plansList.setAdapter(plansListAdapter);

                            plansListAdapter.notifyDataSetChanged();


                        } else {
                            empty_layout.setVisibility(View.VISIBLE);
                            plansList.setVisibility(View.GONE);
                        }

                    }
                }

                @Override
                public void onFailure(Call<AllPlans> call, Throwable t) {
                    plansShowProgressBar.setVisibility(View.GONE);
                    plansList.setVisibility(View.VISIBLE);
                    Log.i("tag", t.getMessage().toString());
                }
            });


        }
    }

    public static class FragmentFullTalktimeRecharge extends Fragment {

        View mView;
        RecyclerView plansList;
        ProgressBar plansShowProgressBar;
        RelativeLayout empty_layout;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            mView = inflater.inflate(R.layout.plan_fragment_special, container, false);

            plansList = (RecyclerView) mView.findViewById(R.id.plansList);
            plansShowProgressBar = (ProgressBar) mView.findViewById(R.id.plansShowProgressBar);
            empty_layout = (RelativeLayout) mView.findViewById(R.id.empty_layout);

            getAllPlansRequest();
            return mView;
        }


        private void getAllPlansRequest() {

            plansShowProgressBar.setVisibility(View.VISIBLE);
            plansList.setVisibility(View.GONE);

            String xAccessToken = "mykey";

            MultipartBody.Part rechType_body = MultipartBody.Part.createFormData("rechType", "FTT");
            MultipartBody.Part circleCode_body = MultipartBody.Part.createFormData("circleCode", "1");
            MultipartBody.Part opid_body = MultipartBody.Part.createFormData("opid", strOperatorCode);

            mainAPIInterface.getAllRechargePlans(xAccessToken, rechType_body, circleCode_body,
                    opid_body).enqueue(new Callback<AllPlans>() {
                @Override
                public void onResponse(Call<AllPlans> call, Response<AllPlans> response) {

                    if (response.isSuccessful()) {

                        plansShowProgressBar.setVisibility(View.GONE);

                        if (response.body().getSuccess().equalsIgnoreCase("1")) {
                            plansList.setVisibility(View.VISIBLE);
                            empty_layout.setVisibility(View.GONE);

                            plansArrayList = response.body().getData();

                            plansListAdapter = new PlansListAdapter(plansArrayList, getActivity());

                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
//                            layoutManager.setStackFromEnd(true);

                            plansList.setLayoutManager(layoutManager);
                            plansList.setItemAnimator(new DefaultItemAnimator());
                            plansList.getItemAnimator().setChangeDuration(0);

                            //fix slow recyclerview start
                            plansList.setHasFixedSize(true);
                            plansList.setItemViewCacheSize(10);
                            plansList.setDrawingCacheEnabled(true);
                            plansList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                            ///fix slow recyclerview end

                            plansList.setAdapter(plansListAdapter);

                            plansListAdapter.notifyDataSetChanged();


                        } else {
                            empty_layout.setVisibility(View.VISIBLE);
                            plansList.setVisibility(View.GONE);
                        }

                    }
                }

                @Override
                public void onFailure(Call<AllPlans> call, Throwable t) {
                    plansShowProgressBar.setVisibility(View.GONE);
                    plansList.setVisibility(View.VISIBLE);
                    Log.i("tag", t.getMessage().toString());
                }
            });


        }
    }

    public static class FragmentTupRecharge extends Fragment {

        View mView;
        RecyclerView plansList;
        ProgressBar plansShowProgressBar;
        RelativeLayout empty_layout;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            mView = inflater.inflate(R.layout.plan_fragment_special, container, false);

            plansList = (RecyclerView) mView.findViewById(R.id.plansList);
            plansShowProgressBar = (ProgressBar) mView.findViewById(R.id.plansShowProgressBar);
            empty_layout = (RelativeLayout) mView.findViewById(R.id.empty_layout);

            getAllPlansRequest();
            return mView;
        }


        private void getAllPlansRequest() {

            plansShowProgressBar.setVisibility(View.VISIBLE);
            plansList.setVisibility(View.GONE);

            String xAccessToken = "mykey";

            MultipartBody.Part rechType_body = MultipartBody.Part.createFormData("rechType", "TUP");
            MultipartBody.Part circleCode_body = MultipartBody.Part.createFormData("circleCode", "1");
            MultipartBody.Part opid_body = MultipartBody.Part.createFormData("opid", strOperatorCode);

            mainAPIInterface.getAllRechargePlans(xAccessToken, rechType_body, circleCode_body,
                    opid_body).enqueue(new Callback<AllPlans>() {
                @Override
                public void onResponse(Call<AllPlans> call, Response<AllPlans> response) {

                    if (response.isSuccessful()) {

                        plansShowProgressBar.setVisibility(View.GONE);

                        if (response.body().getSuccess().equalsIgnoreCase("1")) {
                            plansList.setVisibility(View.VISIBLE);
                            empty_layout.setVisibility(View.GONE);

                            plansArrayList = response.body().getData();

                            plansListAdapter = new PlansListAdapter(plansArrayList, getActivity());

                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
//                            layoutManager.setStackFromEnd(true);

                            plansList.setLayoutManager(layoutManager);
                            plansList.setItemAnimator(new DefaultItemAnimator());
                            plansList.getItemAnimator().setChangeDuration(0);

                            //fix slow recyclerview start
                            plansList.setHasFixedSize(true);
                            plansList.setItemViewCacheSize(10);
                            plansList.setDrawingCacheEnabled(true);
                            plansList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                            ///fix slow recyclerview end

                            plansList.setAdapter(plansListAdapter);

                            plansListAdapter.notifyDataSetChanged();


                        } else {
                            empty_layout.setVisibility(View.VISIBLE);
                            plansList.setVisibility(View.GONE);
                        }

                    }
                }

                @Override
                public void onFailure(Call<AllPlans> call, Throwable t) {
                    plansShowProgressBar.setVisibility(View.GONE);
                    plansList.setVisibility(View.VISIBLE);
                    Log.i("tag", t.getMessage().toString());
                }
            });


        }
    }

    public static class FragmentRoamingRecharge extends Fragment {

        View mView;
        RecyclerView plansList;
        ProgressBar plansShowProgressBar;
        RelativeLayout empty_layout;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            mView = inflater.inflate(R.layout.plan_fragment_special, container, false);

            plansList = (RecyclerView) mView.findViewById(R.id.plansList);
            plansShowProgressBar = (ProgressBar) mView.findViewById(R.id.plansShowProgressBar);
            empty_layout = (RelativeLayout) mView.findViewById(R.id.empty_layout);

            getAllPlansRequest();
            return mView;
        }


        private void getAllPlansRequest() {

            plansShowProgressBar.setVisibility(View.VISIBLE);
            plansList.setVisibility(View.GONE);

            String xAccessToken = "mykey";

            MultipartBody.Part rechType_body = MultipartBody.Part.createFormData("rechType", "RMG");
            MultipartBody.Part circleCode_body = MultipartBody.Part.createFormData("circleCode", "1");
            MultipartBody.Part opid_body = MultipartBody.Part.createFormData("opid", strOperatorCode);

            mainAPIInterface.getAllRechargePlans(xAccessToken, rechType_body, circleCode_body,
                    opid_body).enqueue(new Callback<AllPlans>() {
                @Override
                public void onResponse(Call<AllPlans> call, Response<AllPlans> response) {

                    if (response.isSuccessful()) {

                        plansShowProgressBar.setVisibility(View.GONE);

                        if (response.body().getSuccess().equalsIgnoreCase("1")) {
                            plansList.setVisibility(View.VISIBLE);
                            empty_layout.setVisibility(View.GONE);

                            plansArrayList = response.body().getData();

                            plansListAdapter = new PlansListAdapter(plansArrayList, getActivity());

                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            layoutManager.setOrientation(RecyclerView.VERTICAL);
//                            layoutManager.setStackFromEnd(true);

                            plansList.setLayoutManager(layoutManager);
                            plansList.setItemAnimator(new DefaultItemAnimator());
                            plansList.getItemAnimator().setChangeDuration(0);

                            //fix slow recyclerview start
                            plansList.setHasFixedSize(true);
                            plansList.setItemViewCacheSize(10);
                            plansList.setDrawingCacheEnabled(true);
                            plansList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                            ///fix slow recyclerview end

                            plansList.setAdapter(plansListAdapter);

                            plansListAdapter.notifyDataSetChanged();


                        } else {
                            empty_layout.setVisibility(View.VISIBLE);
                            plansList.setVisibility(View.GONE);
                        }

                    }
                }

                @Override
                public void onFailure(Call<AllPlans> call, Throwable t) {
                    plansShowProgressBar.setVisibility(View.GONE);
                    plansList.setVisibility(View.VISIBLE);
                    Log.i("tag", t.getMessage().toString());
                }
            });


        }
    }

    public static class PlansListAdapter extends RecyclerView.Adapter<PlansListAdapter.MyViewHolder> {

        ArrayList<AllPlans.Plans> plansArrayList;
        Activity activity;

        public PlansListAdapter(ArrayList<AllPlans.Plans> plansArrayList, Activity activity) {

            this.plansArrayList = plansArrayList;
            this.activity = activity;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public MyTextView txtValidity;
            public MyTextView txtAmount;
            public MyTextView txtDetails;
            public MyTextView txtTalktime;

            public MyViewHolder(View view) {
                super(view);

                txtValidity = (MyTextView) view.findViewById(R.id.txtValidity);
                txtAmount = (MyTextView) view.findViewById(R.id.txtAmount);
                txtDetails = (MyTextView) view.findViewById(R.id.txtDetails);
                txtTalktime = (MyTextView) view.findViewById(R.id.txtTalktime);

            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.plans_list_item, parent, false);

            return new MyViewHolder(itemView);
        }


        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final AllPlans.Plans plans = plansArrayList.get(position);

            holder.txtAmount.setText("Amount: " + SmartPayApplication.CURRENCY_SYMBOL + plans.getAmount());
            holder.txtDetails.setText(plans.getDetail());
            holder.txtValidity.setText("Validity: " + plans.getValidity());
            holder.txtTalktime.setText("Talktime: " + plans.getTalktime());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    newFragment.dismiss();
                    edtAmount.setText(plans.getAmount());
                }
            });
        }


        @Override
        public int getItemCount() {
            return plansArrayList.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

    }

    private void getWalletBalance() {

        String xAccessToken = "mykey";

        String wallet_id = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);

        MultipartBody.Part seller_id_body = MultipartBody.Part.createFormData("wallet_id", wallet_id);

        mainAPIInterface.getWalletTransactions(xAccessToken, seller_id_body).enqueue(new Callback<GetWalletTransactionsOutput>() {
            @Override
            public void onResponse(Call<GetWalletTransactionsOutput> call, Response<GetWalletTransactionsOutput> response) {

                if (response.isSuccessful()) {

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        mainWalletBalance = Double.valueOf(response.body().getBalance());

                    }

                }
            }

            @Override
            public void onFailure(Call<GetWalletTransactionsOutput> call, Throwable t) {
                Log.i("tag", t.getMessage().toString());
            }
        });

    }

}
