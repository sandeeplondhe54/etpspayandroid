package com.smart.paywallet.activity.wallet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.wallet.BankTransferListAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.AllBankTransfers;
import com.smart.paywallet.models.output.IsSellerExist;
import com.smart.paywallet.models.output.MoneyTranferCustomerReg;
import com.smart.paywallet.models.output.MoneyTransferCustomerDetails;
import com.smart.paywallet.models.output.NormalResponseBody;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_MOBILE;
import static com.smart.paywallet.utils.DataVaultManager.KEY_NAME;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;

public class WithdrawMoneyToBankAccount extends AppCompatActivity {

    MainAPIInterface mainAPIInterface;

    ProgressDialog newProgressDialog;

    LinearLayout llExistingPayee, llNewPayee;

    String strCustomerPincode;

    LinearLayout ll_empty_sales_layout;

    private MyEditText edtOtpNumber;
    private MyTextView btnResendOtp, txtTimer;
    FloatingActionButton confirm_otp;
    int seconds = 60;
    int minutes = 0;
    //Declare the timer
    Timer newTimer;

    String strOtp, sentOtp;

    Dialog mBottomSheetDialog;

    RecyclerView bankTransferListView;
    ProgressBar bankTransferProgressBar;

    ArrayList<AllBankTransfers.Transfer> transferArrayList;

    BankTransferListAdapter bankTransferListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_to_bank_account_transfer);

        mainAPIInterface = ApiUtils.getAPIService();

        setupActionBar();

        llExistingPayee = (LinearLayout) findViewById(R.id.llExistingPayee);
        llNewPayee = (LinearLayout) findViewById(R.id.llNewPayee);

        ll_empty_sales_layout = (LinearLayout) findViewById(R.id.ll_empty_sales_layout);

        bankTransferListView = (RecyclerView) findViewById(R.id.bankTransferListView);
        bankTransferProgressBar = (ProgressBar) findViewById(R.id.bankTransferProgressBar);


        llExistingPayee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent newIntent = new Intent(WithdrawMoneyToBankAccount.this, ExistingPayeeAccounts.class);
                startActivity(newIntent);

            }
        });

        llNewPayee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent newIntent = new Intent(WithdrawMoneyToBankAccount.this, AddNewPayee.class);
                startActivity(newIntent);

            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        checkingCustomerDetailsRequest();

    }


    private void checkingCustomerDetailsRequest() {
        String strUserId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);
        String strUserName = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_NAME);
        String strMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        newProgressDialog = new ProgressDialog(WithdrawMoneyToBankAccount.this);

        newProgressDialog.setMessage("Checking customer details.");

        newProgressDialog.show();


        String xAccessToken = "mykey";

        MultipartBody.Part customer_mobile_body = MultipartBody.Part.createFormData("customerMobile", strMobile);

        mainAPIInterface.getCustomerDetailsRequest(xAccessToken, customer_mobile_body).enqueue(new Callback<MoneyTransferCustomerDetails>() {
            @Override
            public void onResponse(Call<MoneyTransferCustomerDetails> call, Response<MoneyTransferCustomerDetails> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("2")) {

                        System.out.println(response.body());

                        if (response.body().getDetails().getOtpVerified() == 1) {


                            getBankTransferHistory();

                        } else {

                            registeringFirstTimeCustomer();
                        }

                    } else {

                        System.out.println(response.body());

                    }

                }
            }

            @Override
            public void onFailure(Call<MoneyTransferCustomerDetails> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }


    private void registeringFirstTimeCustomer() {

        String strUserId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);
        String strUserName = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_NAME);
        String strMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        newProgressDialog = new ProgressDialog(WithdrawMoneyToBankAccount.this);

        newProgressDialog.setMessage("Registering your account for KYC update.");

        newProgressDialog.show();

        String xAccessToken = "mykey";

        strUserName = strUserName.toLowerCase().replaceAll(" ", "");

        MultipartBody.Part customer_mobile_body = MultipartBody.Part.createFormData("customerMobile", strMobile);
        MultipartBody.Part customer_name_body = MultipartBody.Part.createFormData("customerName", strUserName);
        MultipartBody.Part customer_pincode_body = MultipartBody.Part.createFormData("customerPincode", "400612");


        mainAPIInterface.moneyTransferCustomerRegistration(xAccessToken, customer_mobile_body, customer_name_body, customer_pincode_body).enqueue(new Callback<MoneyTranferCustomerReg>() {
            @Override
            public void onResponse(Call<MoneyTranferCustomerReg> call, Response<MoneyTranferCustomerReg> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("3") || (response.body().getSuccess().equalsIgnoreCase("1"))) {

                        System.out.println(response.body());
                        Toast.makeText(WithdrawMoneyToBankAccount.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        openVerificationAtBottom();

                    } else {

                        System.out.println(response.body());
                        Toast.makeText(WithdrawMoneyToBankAccount.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<MoneyTranferCustomerReg> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });

    }

    private void getBankTransferHistory() {

        String strUserId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);

        bankTransferProgressBar.setVisibility(View.VISIBLE);
        bankTransferListView.setVisibility(View.GONE);

        String xAccessToken = "mykey";

        MultipartBody.Part customer_id_body = MultipartBody.Part.createFormData("customer_id", strUserId);


        mainAPIInterface.getAllBankTransfers(xAccessToken, customer_id_body).enqueue(new Callback<AllBankTransfers>() {
            @Override
            public void onResponse(Call<AllBankTransfers> call, Response<AllBankTransfers> response) {

                if (response.isSuccessful()) {

                    bankTransferProgressBar.setVisibility(View.GONE);

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        bankTransferListView.setVisibility(View.VISIBLE);
                        ll_empty_sales_layout.setVisibility(View.GONE);

                        transferArrayList = response.body().getTransfers();

                        bankTransferListAdapter = new BankTransferListAdapter(transferArrayList, WithdrawMoneyToBankAccount.this);

                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        layoutManager.setOrientation(RecyclerView.VERTICAL);
//                            layoutManager.setStackFromEnd(true);

                        bankTransferListView.setLayoutManager(layoutManager);
                        bankTransferListView.setItemAnimator(new DefaultItemAnimator());
                        bankTransferListView.getItemAnimator().setChangeDuration(0);

                        //fix slow recyclerview start
                        bankTransferListView.setHasFixedSize(true);
                        bankTransferListView.setItemViewCacheSize(10);
                        bankTransferListView.setDrawingCacheEnabled(true);
                        bankTransferListView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                        ///fix slow recyclerview end

                        bankTransferListView.setAdapter(bankTransferListAdapter);

                        bankTransferListAdapter.notifyDataSetChanged();


                    } else {
                        ll_empty_sales_layout.setVisibility(View.VISIBLE);
                        bankTransferListView.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(Call<AllBankTransfers> call, Throwable t) {
                bankTransferProgressBar.setVisibility(View.GONE);
                bankTransferListView.setVisibility(View.VISIBLE);
                Log.i("tag", t.getMessage().toString());
            }
        });

    }

    private void openVerificationAtBottom() {

        mBottomSheetDialog = new Dialog(WithdrawMoneyToBankAccount.this);

        mBottomSheetDialog.setContentView(R.layout.confirm_otp_dialog); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.show();

        edtOtpNumber = (MyEditText) mBottomSheetDialog.findViewById(R.id.edtOtpNumber);
        btnResendOtp = (MyTextView) mBottomSheetDialog.findViewById(R.id.btnResendOtp);
        confirm_otp = (FloatingActionButton) mBottomSheetDialog.findViewById(R.id.confirm_otp);

        confirm_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strOtp = edtOtpNumber.getText().toString();

                if (strOtp.length() < 4) {
                    edtOtpNumber.setFocusable(true);
                    edtOtpNumber.setError("Please enter OTP");
                } else {

                    confirmOtpRequest();
                }

            }
        });

        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    private void confirmOtpRequest() {

        String xAccessToken = "mykey";

        newProgressDialog = new ProgressDialog(WithdrawMoneyToBankAccount.this);

        newProgressDialog.setMessage("Confirming your OTP.");
        newProgressDialog.show();

        String strMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        MultipartBody.Part customerMobile_body = MultipartBody.Part.createFormData("customerMobile", strMobile);

        MultipartBody.Part otp_body = MultipartBody.Part.createFormData("otp", strOtp);


        mainAPIInterface.moneyTransferVerifyCustomer(xAccessToken, customerMobile_body, otp_body).enqueue(new Callback<MoneyTranferCustomerReg>() {
            @Override
            public void onResponse(Call<MoneyTranferCustomerReg> call, Response<MoneyTranferCustomerReg> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    mBottomSheetDialog.dismiss();

                    Toast.makeText(WithdrawMoneyToBankAccount.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call<MoneyTranferCustomerReg> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Bank Transfer");
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
