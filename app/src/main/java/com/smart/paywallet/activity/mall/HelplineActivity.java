package com.smart.paywallet.activity.mall;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smart.paywallet.BuildConfig;
import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.activity.wallet.CreateNewSupportTicket;
import com.smart.paywallet.activity.wallet.SupportCenterActivity;


/**
 * Created by Nagendra Yadav on 07-02-2019.
 *
 * @Email :  nagendrayadav0786@gmail.com
 * @Author :  https://twitter.com/nagendrayadav
 * @Skype :  nagendrayadav01
 */

public class HelplineActivity extends AppCompatActivity {

    TextView txtVisitWebsite;
    TextView support_phone;
    TextView support_email;

    LinearLayout llCreateTicket, llViewTickets;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.helpline_layout);
        setupActionBar();

        support_phone = (TextView) findViewById(R.id.support_phone);
        support_email = (TextView) findViewById(R.id.support_email);

        llCreateTicket = (LinearLayout) findViewById(R.id.llCreateTicket);
        llViewTickets = (LinearLayout) findViewById(R.id.llViewTickets);


        txtVisitWebsite = (TextView) findViewById(R.id.txtVisitWebsite);
        txtVisitWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  String url = BuildConfig.BASE_URL;
                String url = "https://etpsonline.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        support_phone.setText(SmartPayApplication.APP_SUPPORT_PHONE);
        support_email.setText(SmartPayApplication.APP_SUPPORT_EMAIL);


        support_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_DIAL); // Action for what intent called for
                intent.setData(Uri.parse("tel: " + SmartPayApplication.APP_SUPPORT_PHONE)); // Data with intent respective action on intent
                startActivity(intent);
            }
        });

        support_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{SmartPayApplication.APP_SUPPORT_EMAIL});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Customer Inquiry");
                intent.setPackage("com.google.android.gm");
                if (intent.resolveActivity(getPackageManager())!=null)
                    startActivity(intent);
                else
                    Toast.makeText(HelplineActivity.this,"Gmail App is not installed",Toast.LENGTH_SHORT).show();
            }
        });
        llCreateTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent newIntent = new Intent(HelplineActivity.this, CreateNewSupportTicket.class);
                startActivity(newIntent);

            }
        });

        llViewTickets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent newIntent = new Intent(HelplineActivity.this, SupportCenterActivity.class);
                startActivity(newIntent);

            }
        });

    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);

        ImageView appLogo = toolbar.findViewById(R.id.appLogo);
        appLogo.setVisibility(View.GONE);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("24*7 Help");

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

                default:
                return super.onOptionsItemSelected(item);
        }
    }


}
