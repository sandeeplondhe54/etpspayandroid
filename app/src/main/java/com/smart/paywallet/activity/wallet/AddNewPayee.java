package com.smart.paywallet.activity.wallet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.AddBeneficiaryOutput;
import com.smart.paywallet.models.output.MoneyTranferCustomerReg;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyEditText;
import com.smart.paywallet.views.MyTextView;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_MOBILE;

public class AddNewPayee extends AppCompatActivity {


    MainAPIInterface mainAPIInterface;

    MyEditText edtbn_name, edtbn_bank_account_number, edtbn_bank_ifsc_code, edtbn_mobile;

    MyTextView btnAddPayee;

    String strBeneficiaryName, strBeneficiaryAccountNumber, strBankIfscCode, strBeneficiaryMobile;
    String customerMobile;

    ProgressDialog newProgressDialog;

    String strOtp, sentOtp;

    Dialog mBottomSheetDialog;
    private MyEditText edtOtpNumber;
    private MyTextView btnResendOtp, txtTimer;
    FloatingActionButton confirm_otp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_payee);

        mainAPIInterface = ApiUtils.getAPIService();

        setupActionBar();

        edtbn_name = (MyEditText) findViewById(R.id.edtbn_name);
        edtbn_bank_account_number = (MyEditText) findViewById(R.id.edtbn_bank_account_number);
        edtbn_bank_ifsc_code = (MyEditText) findViewById(R.id.edtbn_bank_ifsc_code);
        edtbn_mobile = (MyEditText) findViewById(R.id.edtbn_mobile);

        btnAddPayee = (MyTextView) findViewById(R.id.btnAddPayee);

        customerMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        btnAddPayee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strBeneficiaryName = edtbn_name.getText().toString();
                strBeneficiaryAccountNumber = edtbn_bank_account_number.getText().toString();
                strBankIfscCode = edtbn_bank_ifsc_code.getText().toString();
                strBeneficiaryMobile = edtbn_mobile.getText().toString();

                if (strBeneficiaryMobile.length() < 4) {
                    edtbn_name.setError("Enter name here");
                    edtbn_name.setFocusable(true);
                } else if (strBeneficiaryAccountNumber.length() < 5) {
                    edtbn_bank_account_number.setError("Enter proper bank account number");
                    edtbn_bank_account_number.setFocusable(true);
                } else if (strBankIfscCode.length() < 0) {
                    edtbn_bank_ifsc_code.setError("Enter IFSC Code");
                    edtbn_bank_ifsc_code.setFocusable(true);
                } else if (strBeneficiaryMobile.length() < 10) {
                    edtbn_mobile.setError("Enter mobile number");
                    edtbn_mobile.setFocusable(true);
                } else {
                    registerBeneficiaryRequest();
                }
            }
        });
    }

    private void registerBeneficiaryRequest() {

        String xAccessToken = "mykey";

        newProgressDialog = new ProgressDialog(AddNewPayee.this);

        newProgressDialog.setMessage("Adding Beneificiary...");
        newProgressDialog.show();

        strBeneficiaryName = strBeneficiaryName.toLowerCase().replaceAll(" ", "%20");

        MultipartBody.Part customerMobile_body = MultipartBody.Part.createFormData("customerMobile", customerMobile);
        MultipartBody.Part beneficiaryName_body = MultipartBody.Part.createFormData("beneficiaryName", strBeneficiaryName);
        MultipartBody.Part beneficiaryMobileNumber_body = MultipartBody.Part.createFormData("beneficiaryMobileNumber", strBeneficiaryMobile);
        MultipartBody.Part beneficiaryAccountNumber_body = MultipartBody.Part.createFormData("beneficiaryAccountNumber", strBeneficiaryAccountNumber);
        MultipartBody.Part ifscCode_body = MultipartBody.Part.createFormData("ifscCode", strBankIfscCode);


        mainAPIInterface.moneyTransferAddBeneficiary(xAccessToken, customerMobile_body,
                beneficiaryName_body, beneficiaryMobileNumber_body,
                beneficiaryAccountNumber_body, ifscCode_body).enqueue(new Callback<AddBeneficiaryOutput>() {
            @Override
            public void onResponse(Call<AddBeneficiaryOutput> call, Response<AddBeneficiaryOutput> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("3")) {

                        Toast.makeText(AddNewPayee.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    } else {

                        openVerificationAtBottom(response.body().getBeneficiaryId());

                        Toast.makeText(AddNewPayee.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<AddBeneficiaryOutput> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }

    private void openVerificationAtBottom(final int beneficiaryId) {

        mBottomSheetDialog = new Dialog(AddNewPayee.this);

        mBottomSheetDialog.setContentView(R.layout.confirm_otp_dialog); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.show();

        edtOtpNumber = (MyEditText) mBottomSheetDialog.findViewById(R.id.edtOtpNumber);
        btnResendOtp = (MyTextView) mBottomSheetDialog.findViewById(R.id.btnResendOtp);
        confirm_otp = (FloatingActionButton) mBottomSheetDialog.findViewById(R.id.confirm_otp);

        confirm_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strOtp = edtOtpNumber.getText().toString();

                if (strOtp.length() < 4) {
                    edtOtpNumber.setFocusable(true);
                    edtOtpNumber.setError("Please enter OTP");
                } else {

                    confirmOtpRequest(beneficiaryId);
                }

            }
        });

        btnResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void confirmOtpRequest(int beneficiaryId) {

        String xAccessToken = "mykey";

        newProgressDialog = new ProgressDialog(AddNewPayee.this);

        newProgressDialog.setMessage("Confirming your OTP.");
        newProgressDialog.show();

        String strMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        MultipartBody.Part customerMobile_body = MultipartBody.Part.createFormData("customerMobile", strMobile);

        MultipartBody.Part otp_body = MultipartBody.Part.createFormData("otp", strOtp);
        MultipartBody.Part beneficiaryId_body = MultipartBody.Part.createFormData("beneficiaryId", String.valueOf(beneficiaryId));


        mainAPIInterface.moneyTransferVerifyBeneficiaryOtp(xAccessToken, customerMobile_body, otp_body,
                beneficiaryId_body).enqueue(new Callback<AddBeneficiaryOutput>() {
            @Override
            public void onResponse(Call<AddBeneficiaryOutput> call, Response<AddBeneficiaryOutput> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    mBottomSheetDialog.dismiss();

                    Intent newIntent = new Intent(AddNewPayee.this, ExistingPayeeAccounts.class);
                    startActivity(newIntent);
                    finish();

                    Toast.makeText(AddNewPayee.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AddBeneficiaryOutput> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });

    }


    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Add New Payee");
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
