package com.smart.paywallet.activity.wallet;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.adapter.wallet.BeneficiaryListAdapter;
import com.smart.paywallet.adapter.wallet.SupportMessageListAdapter;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.MoneyTransferCustomerDetails;
import com.smart.paywallet.utils.DataVaultManager;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_MOBILE;
import static com.smart.paywallet.utils.DataVaultManager.KEY_NAME;
import static com.smart.paywallet.utils.DataVaultManager.KEY_USER_ID;

public class ExistingPayeeAccounts extends AppCompatActivity {

    RecyclerView beneficiaryListView;

    MainAPIInterface mainAPIInterface;

    ArrayList<MoneyTransferCustomerDetails.BeneficiaryList> beneficiaryListArrayList;

    BeneficiaryListAdapter beneficiaryListAdapter;

    ProgressDialog newProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.existing_payee_accounts);

        mainAPIInterface = ApiUtils.getAPIService();

        setupActionBar();
        beneficiaryListView = (RecyclerView) findViewById(R.id.beneficiaryListView);

        checkingCustomerDetailsRequest();
    }


    private void checkingCustomerDetailsRequest() {
        String strUserId = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_USER_ID);
        String strUserName = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_NAME);
        String strMobile = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_MOBILE);

        newProgressDialog = new ProgressDialog(ExistingPayeeAccounts.this);

        newProgressDialog.setMessage("Checking customer details.");

        newProgressDialog.show();


        String xAccessToken = "mykey";

        MultipartBody.Part customer_mobile_body = MultipartBody.Part.createFormData("customerMobile", strMobile);

        mainAPIInterface.getCustomerDetailsRequest(xAccessToken, customer_mobile_body).enqueue(new Callback<MoneyTransferCustomerDetails>() {
            @Override
            public void onResponse(Call<MoneyTransferCustomerDetails> call, Response<MoneyTransferCustomerDetails> response) {

                if (response.isSuccessful()) {

                    newProgressDialog.dismiss();

                    if (response.body().getSuccess().equalsIgnoreCase("2")) {

                        System.out.println(response.body());

                        if (response.body().getDetails().getOtpVerified() == 1) {


                            beneficiaryListArrayList = response.body().getBeneficiaryList();

                            if (beneficiaryListArrayList == null) {

                                Toast.makeText(ExistingPayeeAccounts.this, "No Beneficiary Accounts Found", Toast.LENGTH_SHORT).show();

                            } else {

                                beneficiaryListAdapter = new BeneficiaryListAdapter(beneficiaryListArrayList, ExistingPayeeAccounts.this);

                                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                layoutManager.setOrientation(RecyclerView.VERTICAL);
//                            layoutManager.setStackFromEnd(true);

                                beneficiaryListView.setLayoutManager(layoutManager);
                                beneficiaryListView.setItemAnimator(new DefaultItemAnimator());
                                beneficiaryListView.getItemAnimator().setChangeDuration(0);

                                //fix slow recyclerview start
                                beneficiaryListView.setHasFixedSize(true);
                                beneficiaryListView.setItemViewCacheSize(10);
                                beneficiaryListView.setDrawingCacheEnabled(true);
                                beneficiaryListView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                ///fix slow recyclerview end

                                beneficiaryListView.setAdapter(beneficiaryListAdapter);

                                beneficiaryListAdapter.notifyDataSetChanged();

                            }


                        } else {

//                            registeringFirstTimeCustomer();

                            Toast.makeText(ExistingPayeeAccounts.this, "No Beneficiary Accounts Found", Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        System.out.println(response.body());

                    }

                }
            }

            @Override
            public void onFailure(Call<MoneyTransferCustomerDetails> call, Throwable t) {
                newProgressDialog.dismiss();
                Log.i("tag", t.getMessage().toString());
            }
        });


    }


    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Existing Payees");
        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
