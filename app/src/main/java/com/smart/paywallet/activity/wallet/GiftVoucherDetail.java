package com.smart.paywallet.activity.wallet;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.smart.paywallet.R;
import com.smart.paywallet.SmartPayApplication;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.api.MainAPIInterface;
import com.smart.paywallet.models.output.GetWalletTransactionsOutput;
import com.smart.paywallet.utils.DataVaultManager;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.smart.paywallet.utils.DataVaultManager.KEY_WALLET_ID;

public class GiftVoucherDetail extends AppCompatActivity {
    MainAPIInterface mainAPIInterface;

    String strVoucherName, voucher_image, voucher_code, voucher_id;
    String strWalletId, strUserId;

    MyTextView txtVoucherName;
    ImageView voucherImageView;


    MyTextView rs100, rs200, rs500, rs1000, rs5000, rs10000;

    int isAmountSelected = 0;

    String strAmount;

    MyTextView btnBuyVoucherNow;

    double mainWalletBalance;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voucher_detail_activity);

        Intent value = getIntent();

        if (value.getExtras().getString("voucher_name") != null) {
            strVoucherName = value.getExtras().getString("voucher_name");
        }

        voucher_image = value.getExtras().getString("voucher_image");
        voucher_code = value.getExtras().getString("voucher_code");
        voucher_id = value.getExtras().getString("voucher_id");

        setupActionBar();

        mainAPIInterface = ApiUtils.getAPIService();


        txtVoucherName = (MyTextView) findViewById(R.id.txtVoucherName);
        voucherImageView = (ImageView) findViewById(R.id.voucherImageView);

        rs100 = (MyTextView) findViewById(R.id.rs100);
        rs200 = (MyTextView) findViewById(R.id.rs200);
        rs500 = (MyTextView) findViewById(R.id.rs500);
        rs1000 = (MyTextView) findViewById(R.id.rs1000);
        rs5000 = (MyTextView) findViewById(R.id.rs5000);
        rs10000 = (MyTextView) findViewById(R.id.rs10000);

        btnBuyVoucherNow = (MyTextView) findViewById(R.id.btnBuyVoucherNow);

        txtVoucherName.setText(strVoucherName);

        Picasso.with(GiftVoucherDetail.this)
                .load(Constants.GIFT_VOUCHER_IMAGE_PATH + voucher_image)
                .placeholder(R.drawable.placeholder)
                .into(voucherImageView);


        rs100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rs100.setBackgroundResource(R.drawable.blue_rounded_dark_bg);

                rs200.setBackgroundResource(R.drawable.blue_corner_bg);
                rs500.setBackgroundResource(R.drawable.blue_corner_bg);
                rs1000.setBackgroundResource(R.drawable.blue_corner_bg);
                rs5000.setBackgroundResource(R.drawable.blue_corner_bg);
                rs10000.setBackgroundResource(R.drawable.blue_corner_bg);


                isAmountSelected = 1;
                strAmount = "100";
            }
        });


        rs200.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rs200.setBackgroundResource(R.drawable.blue_rounded_dark_bg);

                rs100.setBackgroundResource(R.drawable.blue_corner_bg);
                rs500.setBackgroundResource(R.drawable.blue_corner_bg);
                rs1000.setBackgroundResource(R.drawable.blue_corner_bg);
                rs5000.setBackgroundResource(R.drawable.blue_corner_bg);
                rs10000.setBackgroundResource(R.drawable.blue_corner_bg);

                isAmountSelected = 1;
                strAmount = "200";
            }
        });


        rs500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rs500.setBackgroundResource(R.drawable.blue_rounded_dark_bg);

                rs100.setBackgroundResource(R.drawable.blue_corner_bg);
                rs200.setBackgroundResource(R.drawable.blue_corner_bg);
                rs1000.setBackgroundResource(R.drawable.blue_corner_bg);
                rs5000.setBackgroundResource(R.drawable.blue_corner_bg);
                rs10000.setBackgroundResource(R.drawable.blue_corner_bg);

                isAmountSelected = 1;

                strAmount = "500";
            }
        });

        rs1000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rs1000.setBackgroundResource(R.drawable.blue_rounded_dark_bg);


                rs100.setBackgroundResource(R.drawable.blue_corner_bg);
                rs200.setBackgroundResource(R.drawable.blue_corner_bg);
                rs500.setBackgroundResource(R.drawable.blue_corner_bg);
                rs5000.setBackgroundResource(R.drawable.blue_corner_bg);
                rs10000.setBackgroundResource(R.drawable.blue_corner_bg);

                isAmountSelected = 1;

                strAmount = "1000";
            }
        });


        rs5000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rs5000.setBackgroundResource(R.drawable.blue_rounded_dark_bg);

                rs100.setBackgroundResource(R.drawable.blue_corner_bg);
                rs200.setBackgroundResource(R.drawable.blue_corner_bg);
                rs500.setBackgroundResource(R.drawable.blue_corner_bg);
                rs1000.setBackgroundResource(R.drawable.blue_corner_bg);
                rs10000.setBackgroundResource(R.drawable.blue_corner_bg);
                isAmountSelected = 1;

                strAmount = "5000";

            }
        });

        rs10000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rs10000.setBackgroundResource(R.drawable.blue_rounded_dark_bg);

                rs100.setBackgroundResource(R.drawable.blue_corner_bg);
                rs200.setBackgroundResource(R.drawable.blue_corner_bg);
                rs500.setBackgroundResource(R.drawable.blue_corner_bg);
                rs1000.setBackgroundResource(R.drawable.blue_corner_bg);
                rs5000.setBackgroundResource(R.drawable.blue_corner_bg);
                isAmountSelected = 1;

                strAmount = "10000";

            }
        });

        btnBuyVoucherNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isAmountSelected == 1) {


                    if (Double.valueOf(strAmount) <= mainWalletBalance) {

                        Intent checkout = new Intent(GiftVoucherDetail.this, GiftVoucherPaymentActivity.class);
                        checkout.putExtra("opid", voucher_code);
                        checkout.putExtra("amount", strAmount);
                        checkout.putExtra("voucher_name", strVoucherName);
                        checkout.putExtra("voucher_image", voucher_image);

                        startActivity(checkout);
                        finish();

                    } else {
                        Toast.makeText(GiftVoucherDetail.this,
                                "Your Wallet does not have sufficient funds.", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(GiftVoucherDetail.this, "Please select the amount",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);

        ImageView appLogo = toolbar.findViewById(R.id.appLogo);
        appLogo.setVisibility(View.GONE);

        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText(strVoucherName);


        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getWalletBalance();
    }


    private void getWalletBalance() {

        String xAccessToken = "mykey";

        String wallet_id = DataVaultManager.getInstance(SmartPayApplication.getInstance()).getVaultValue(KEY_WALLET_ID);

        MultipartBody.Part seller_id_body = MultipartBody.Part.createFormData("wallet_id", wallet_id);

        mainAPIInterface.getWalletTransactions(xAccessToken, seller_id_body).enqueue(new Callback<GetWalletTransactionsOutput>() {
            @Override
            public void onResponse(Call<GetWalletTransactionsOutput> call, Response<GetWalletTransactionsOutput> response) {

                if (response.isSuccessful()) {

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        mainWalletBalance = Double.valueOf(response.body().getBalance());

                    }

                }
            }

            @Override
            public void onFailure(Call<GetWalletTransactionsOutput> call, Throwable t) {
                Log.i("tag", t.getMessage().toString());
            }
        });

    }
}
