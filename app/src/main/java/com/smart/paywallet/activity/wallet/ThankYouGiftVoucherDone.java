package com.smart.paywallet.activity.wallet;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.snackbar.Snackbar;
import com.smart.paywallet.R;
import com.smart.paywallet.api.ApiUtils;
import com.smart.paywallet.api.Constants;
import com.smart.paywallet.views.MyTextView;
import com.squareup.picasso.Picasso;

public class ThankYouGiftVoucherDone extends AppCompatActivity {
    CoordinatorLayout coordinatorLayout;

    ImageView rechargeOperatorImage;

    String strOperatorCode, strOperatorName, strOperatorImage, amount, receiverName, receiverEmail,
            receiverMobile, order_id, strSenderMobile;

    MyTextView voucherName, yourNumber, rechargeOperatorName2, rechargeAmount, txtReceiverName,
            txtReceiverEmail, txtReceiverMobile;

    MyTextView btnRechargeGoBack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gift_voucher_done_activity);
        setupActionBar();


        Intent value = getIntent();
        strOperatorCode = value.getExtras().getString("strOperatorCode");
        strOperatorName = value.getExtras().getString("strOperatorName");
        strOperatorImage = value.getExtras().getString("strOperatorImage");
        receiverName = value.getExtras().getString("receiverName");
        receiverEmail = value.getExtras().getString("receiverEmail");
        receiverMobile = value.getExtras().getString("receiverMobile");
        order_id = value.getExtras().getString("order_id");
        strSenderMobile = value.getExtras().getString("strSenderMobile");


        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        voucherName = (MyTextView) findViewById(R.id.voucherName);
        yourNumber = (MyTextView) findViewById(R.id.yourNumber);
        rechargeOperatorName2 = (MyTextView) findViewById(R.id.rechargeOperatorName2);
        rechargeAmount = (MyTextView) findViewById(R.id.rechargeAmount);
        txtReceiverName = (MyTextView) findViewById(R.id.txtReceiverName);
        txtReceiverEmail = (MyTextView) findViewById(R.id.txtReceiverEmail);
        txtReceiverMobile = (MyTextView) findViewById(R.id.txtReceiverMobile);
        btnRechargeGoBack = (MyTextView) findViewById(R.id.btnRechargeGoBack);

        rechargeOperatorImage = (ImageView) findViewById(R.id.rechargeOperatorImage);

        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Payment Done Successfully", Snackbar.LENGTH_LONG);

        snackbar.show();

        voucherName.setText(strOperatorName);
        yourNumber.setText(strSenderMobile);
        rechargeOperatorName2.setText(strOperatorName);
        rechargeAmount.setText(amount);
        txtReceiverName.setText(receiverName);
        txtReceiverEmail.setText(receiverEmail);
        txtReceiverMobile.setText(receiverMobile);

        Picasso.with(ThankYouGiftVoucherDone.this)
                .load(Constants.GIFT_VOUCHER_IMAGE_PATH + strOperatorImage)
                .placeholder(R.drawable.placeholder)
                .into(rechargeOperatorImage);

        btnRechargeGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView menu_icon = toolbar.findViewById(R.id.menu_icon);
        menu_icon.setVisibility(View.GONE);


        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText("Thank you");

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behaviour
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

                default:
                return super.onOptionsItemSelected(item);
        }
    }
}
