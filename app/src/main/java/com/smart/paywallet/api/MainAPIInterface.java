package com.smart.paywallet.api;

import com.smart.paywallet.models.input.MobileRechargeModel;
import com.smart.paywallet.models.output.AddBeneficiaryOutput;
import com.smart.paywallet.models.output.AddProductToCartModel;
import com.smart.paywallet.models.output.AllBankTransfers;
import com.smart.paywallet.models.output.AllChildCategoryOutput;
import com.smart.paywallet.models.output.AllPlans;
import com.smart.paywallet.models.output.AppointmentBookingInputModel;
import com.smart.paywallet.models.output.AppointmentBookingOutputModel;
import com.smart.paywallet.models.output.BookOrderInputModel;
import com.smart.paywallet.models.output.BookOrderModel;
import com.smart.paywallet.models.output.CategoryListModel;
import com.smart.paywallet.models.output.CheckWalletOutput;
import com.smart.paywallet.models.output.CommonOutput;
import com.smart.paywallet.models.output.DetailProductInputModel;
import com.smart.paywallet.models.output.EditUserAccountInputModel;
import com.smart.paywallet.models.output.FlashDealsProductsOutputModel;
import com.smart.paywallet.models.output.ForgotPasswordOutputModel;
import com.smart.paywallet.models.output.GenerateChecksumOutput;
import com.smart.paywallet.models.output.GetAccountDetailsOutputModel;
import com.smart.paywallet.models.output.GetAllFashionOffersModel;
import com.smart.paywallet.models.output.GetAllFlashDealsModel;
import com.smart.paywallet.models.output.GetAllGiftVouchers;
import com.smart.paywallet.models.output.GetAllOperators;
import com.smart.paywallet.models.output.GetAppSettings;
import com.smart.paywallet.models.output.GetBrandsOutput;
import com.smart.paywallet.models.output.GetCategoryDetailOutputModel;
import com.smart.paywallet.models.output.GetHomepageSliderOutputModel;
import com.smart.paywallet.models.output.GetOnlineCartOutput;
import com.smart.paywallet.models.output.GetSellerModel;
import com.smart.paywallet.models.output.GetShipmentDetailsOutputModel;
import com.smart.paywallet.models.output.GetTopOfTheDayOutputModel;
import com.smart.paywallet.models.output.GetTopShoppingOffersModel;
import com.smart.paywallet.models.output.GetWalletTransactionsOutput;
import com.smart.paywallet.models.output.GiftVoucherCategories;
import com.smart.paywallet.models.output.GiftVoucherOrders;
import com.smart.paywallet.models.output.InboxBlogPost;
import com.smart.paywallet.models.output.IsSellerExist;
import com.smart.paywallet.models.output.MoneyTranferCustomerReg;
import com.smart.paywallet.models.output.MoneyTransferBeneficiaryValidation;
import com.smart.paywallet.models.output.MoneyTransferCustomerDetails;
import com.smart.paywallet.models.output.MoneyTransferToBankAccount;
import com.smart.paywallet.models.output.NewServiceListModel;
import com.smart.paywallet.models.output.NewServiceListOutputModel;
import com.smart.paywallet.models.output.NormalResponseBody;
import com.smart.paywallet.models.output.PostSellerReviewInputModel;
import com.smart.paywallet.models.output.PostSellerReviewOutputModel;
import com.smart.paywallet.models.output.ProductOrderNewModel;
import com.smart.paywallet.models.output.ProductOrderNewOutputModel;
import com.smart.paywallet.models.output.ProductReviewListInputModel;
import com.smart.paywallet.models.output.ProductReviewListOutputModel;
import com.smart.paywallet.models.output.ProductsListModel;
import com.smart.paywallet.models.output.RateProductInputModel;
import com.smart.paywallet.models.output.RechargeOrderListOutput;
import com.smart.paywallet.models.output.RechargeOutput;
import com.smart.paywallet.models.output.RequestListOutput;
import com.smart.paywallet.models.output.SearchProductOutputModel;
import com.smart.paywallet.models.output.SellerReviewListOutputModel;
import com.smart.paywallet.models.output.ServiceListModel;
import com.smart.paywallet.models.output.SupportTicketListOutput;
import com.smart.paywallet.models.output.SupportTicketMessagesList;
import com.smart.paywallet.models.output.TrackDeliveryInputModel;
import com.smart.paywallet.models.output.TrackDeliveryOutputModel;
import com.smart.paywallet.models.output.UpdateCartOutput;
import com.smart.paywallet.models.output.UserProfileOutput;
import com.smart.paywallet.models.output.YourAppointmentOutputModel;
import com.smart.paywallet.models.output.YourOrderOutputModel;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;


public interface MainAPIInterface<R extends Retrofit> {

    @Multipart
    @POST(Constants.USER_MOBILE_REGISTRATION)
    Call<UserProfileOutput> registerUserMobile(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part country_code,
            @Part MultipartBody.Part mobile
    );

    @Multipart
    @POST(Constants.VERIFY_USER_MOBILE)
    Call<NormalResponseBody> verifyUserMobile(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part mobile,
            @Part MultipartBody.Part otp
    );

    @Multipart
    @POST(Constants.USER_SIGNUP)
    Call<UserProfileOutput> userSignup(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part email,
            @Part MultipartBody.Part phone_no,
            @Part MultipartBody.Part username,
            @Part MultipartBody.Part password,
            @Part MultipartBody.Part fcm_id
    );


    @Multipart
    @POST(Constants.USER_LOGIN)
    Call<UserProfileOutput> userLogin(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part mobile,
            @Part MultipartBody.Part fcm_id,
            @Part MultipartBody.Part password
    );


    @GET(Constants.GET_ALL_CATEGORIES)
    Call<CategoryListModel> getCategories(@Header("X-API-KEY") String xAccessToken);

    @GET(Constants.GET_ALL_SUB_CAT)
    Call<CategoryListModel> getSubCategories(
            @Header("X-API-KEY") String xAccessToken,
            @Path("cat_id") String cat_id
    );


    @Multipart
    @POST(Constants.GET_SUBCAT_PRODUCTS)
    Call<ProductsListModel> getAllProducts(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part subcat_id
    );


    @POST(Constants.GET_SINGLE_PRODUCT_DETAIL)
    Call<ProductsListModel> getProductDetail(
            @Header("X-API-KEY") String xAccessToken,
            @Body DetailProductInputModel detailProductInputModel
    );


    @GET(Constants.GET_ALL_SERVICE_CATEGORIES)
    Call<ServiceListModel> getServiceCategories(@Header("X-API-KEY") String xAccessToken);


    @GET(Constants.GET_ALL_NEARBY_SELLERS)
    Call<GetSellerModel> getAllNearbySellers(
            @Header("X-API-KEY") String xAccessToken
    );


    @POST(Constants.GET_ALL_TRENDING_PRODUCTS)
    Call<ProductsListModel> getAllTrendingProducts(
            @Header("X-API-KEY") String xAccessToken
    );


    @Multipart
    @POST(Constants.GET_ALL_SELLER_SERVICES)
    Call<NewServiceListModel> getAllServices(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part seller_id
    );


//    @Multipart
//    @POST(Contants.GET_ALL_NEARBY_SELLERS_FROM_CATEGORY)
//    Call<SellerServiceListModel> getAllNearbySellers(
//            @Header("X-API-KEY") String xAccessToken,
//            @Part MultipartBody.Part cat_id
//    );


    @Multipart
    @POST(Constants.GET_ALL_YOUR_ORDERS)
    Call<YourOrderOutputModel> getAllYourOrders(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id
    );


    @POST(Constants.BOOK_ORDER)
    Call<BookOrderModel> bookOrder(
            @Header("X-API-KEY") String xAccessToken,
            @Body BookOrderInputModel bookOrderInputModell
    );


    @Multipart
    @POST(Constants.GET_ALL_YOUR_APPOINTMENTS)
    Call<YourAppointmentOutputModel> getAllYourAppointments(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id
    );

    @POST(Constants.BOOK_APPOINTMENT)
    Call<AppointmentBookingOutputModel> bookAppointment(
            @Header("X-API-KEY") String xAccessToken,
            @Body AppointmentBookingInputModel appointmentBookingInputModel

    );


    @POST(Constants.ADD_PRODUCT_TO_CART)
    Call<IsSellerExist> addProductToCart(
            @Header("X-API-KEY") String xAccessToken,
            @Body AddProductToCartModel addProductToCartModel

    );

    @Multipart
    @POST(Constants.GET_ONLINE_CART_LIST)
    Call<GetOnlineCartOutput> getOnlineCartList(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id
    );

    @Multipart
    @POST(Constants.UPDATE_INCREASE_CART)
    Call<UpdateCartOutput> updateCartIncrease(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id,
            @Part MultipartBody.Part seller_id,
            @Part MultipartBody.Part product_id
    );


    @Multipart
    @POST(Constants.UPDATE_DECREASE_CART)
    Call<UpdateCartOutput> updateCartDecrease(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id,
            @Part MultipartBody.Part seller_id,
            @Part MultipartBody.Part product_id
    );

    @Multipart
    @POST(Constants.UPDATE_DELETE_CART)
    Call<IsSellerExist> updateCartDelete(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id,
            @Part MultipartBody.Part seller_id,
            @Part MultipartBody.Part product_id
    );

    @POST(Constants.NEW_ORDER_BOOKING)
    Call<ProductOrderNewOutputModel> newOrderBookingRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Body ProductOrderNewModel productOrderNewModel


    );

    @POST(Constants.NEW_ORDER_BOOKING_BY_WALLET)
    Call<ProductOrderNewOutputModel> newOrderBookingByWalletRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Body ProductOrderNewModel productOrderNewModel


    );


    @Multipart
    @POST(Constants.GET_CATEGORY_DETAIL)
    Call<GetCategoryDetailOutputModel> getCategoryDetail(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part category_id
    );

    @Multipart
    @POST(Constants.GET_ALL_CHILD_CATEGORIES)
    Call<AllChildCategoryOutput> getAllChildCategories(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part category_id
    );


    @GET(Constants.GET_ALL_BRANDS)
    Call<GetBrandsOutput> getAllBrands(
            @Header("X-API-KEY") String xAccessToken
    );

    @GET(Constants.GET_TOP_BRANDS)
    Call<GetBrandsOutput> getAllTopBrands(
            @Header("X-API-KEY") String xAccessToken
    );


    @Multipart
    @POST(Constants.SEARCH_ALL_PRODUCTS)
    Call<SearchProductOutputModel> searchAllProducts(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part search_query
    );

    @POST(Constants.RATE_REVIEW_SELLER_URL)
    Call<PostSellerReviewOutputModel> postSellerReview(
            @Header("X-API-KEY") String xAccessToken,
            @Body PostSellerReviewInputModel postSellerReviewInputModel


    );


    @POST(Constants.ADD_PRODUCT_REVIEW)
    Call<PostSellerReviewOutputModel> postProductReview(
            @Header("X-API-KEY") String xAccessToken,
            @Body RateProductInputModel rateProductInputModel
    );


    @Multipart
    @POST(Constants.GET_SELLER_REVIEW_LIST)
    Call<SellerReviewListOutputModel> getSellerReviewList(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part seller_id
    );


    @POST(Constants.GET_PRODUCT_REVIEW)
    Call<ProductReviewListOutputModel> getProductReviewList(
            @Header("X-API-KEY") String xAccessToken,
            @Body ProductReviewListInputModel productReviewListInputModel
    );

    @Multipart
    @POST(Constants.GET_ACCOUNT_DETAILS)
    Call<GetAccountDetailsOutputModel> getUserAccountDetails(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id
    );

    @POST(Constants.EDIT_USER_ACCOUNT)
    Call<PostSellerReviewOutputModel> postEditUserAccount(
            @Header("X-API-KEY") String xAccessToken,
            @Body EditUserAccountInputModel editUserAccountInputModel
    );

    @POST(Constants.EDIT_USER_ACCOUNT_ADDRESS)
    Call<PostSellerReviewOutputModel> editUserAccountAddress(
            @Header("X-API-KEY") String xAccessToken,
            @Body EditUserAccountInputModel editUserAccountInputModel
    );


    @Multipart
    @POST(Constants.FORGOT_PASSWORD_REQUEST)
    Call<ForgotPasswordOutputModel> forgotPasswordRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part email
    );

    @Multipart
    @POST(Constants.SEND_PASSWORD_OTP)
    Call<ForgotPasswordOutputModel> sendPasswordChangeOtp(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part user_id,
            @Part MultipartBody.Part email_id
    );

    @Multipart
    @POST(Constants.CHANGE_PASSWORD_URL)
    Call<ForgotPasswordOutputModel> changePasswordRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part user_id,
            @Part MultipartBody.Part user_old_password,
            @Part MultipartBody.Part user_new_password,
            @Part MultipartBody.Part otp
    );


    @Multipart
    @POST(Constants.GET_SERVICES_BY_CATEGORY)
    Call<NewServiceListOutputModel> getAllServicesByCategory(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part category_id
    );

    @GET(Constants.GET_HOMEPAGE_SLIDER)
    Call<GetHomepageSliderOutputModel> getHomePageSlider(
            @Header("X-API-KEY") String xAccessToken
    );

    @GET(Constants.GET_HOMEPAGE_SLIDER2)
    Call<GetHomepageSliderOutputModel> getHomePageSlider2(
            @Header("X-API-KEY") String xAccessToken
    );

    @Multipart
    @POST(Constants.GET_SHIPMENT_DETAILS)
    Call<GetShipmentDetailsOutputModel> getShipmentDetails(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part order_id,
            @Part MultipartBody.Part product_id

    );

    @GET(Constants.GET_APP_SETTINGS)
    Call<GetAppSettings> getAppSettingsRequest(
            @Header("X-API-KEY") String xAccessToken
    );


    //Get All Flash Deals


    @GET(Constants.GET_ALL_FLASH_DEALS)
    Call<GetAllFlashDealsModel> getAllFlashDeals(
            @Header("X-API-KEY") String xAccessToken

    );

    //Get All Flash Deal Products


    @Multipart
    @POST(Constants.GET_FLASH_DEAL_PRODUCTS)
    Call<FlashDealsProductsOutputModel> getAllFlashDealProducts(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part products

    );

    //Get All Fashion Offers

    @GET(Constants.GET_ALL_FASHION_OFFERS)
    Call<GetAllFashionOffersModel> getAllFationOffers(
            @Header("X-API-KEY") String xAccessToken

    );


    // GET TOP OF THE DAY

    @GET(Constants.GET_ALL_TOP_OF_THE_DAY)
    Call<GetTopOfTheDayOutputModel> getAllTopOfTheDay(
            @Header("X-API-KEY") String xAccessToken

    );


    //Get Top Shopping Offers

    @GET(Constants.GET_TOP_SHOPPING_OFFERS)
    Call<GetTopShoppingOffersModel> getTopShoppingOffers(
            @Header("X-API-KEY") String xAccessToken

    );

    //Get Top Shopping Offers

    @Multipart
    @POST(Constants.GET_ALL_PRODUCTS_FROM_CHILD_CATEGORY)
    Call<ProductsListModel> getAllProductsFromChildCategory(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part child_category_id
    );

    //GET PRODUCTS BY BRAND

    @Multipart
    @POST(Constants.GET_ALL_PRODUCTS_FROM_BRAND)
    Call<ProductsListModel> getAllProductsFromBrand(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part brand_id
    );


    @Multipart
    @POST(Constants.GET_ALL_PRODUCTS_FROM_CHILD_CATEGORY_LIMITED)
    Call<ProductsListModel> getAllProductsFromChildCategoryLimited(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part child_category_id
    );


    @POST
    Call<TrackDeliveryOutputModel> getRealTimeTrackingData(
            @Header("Content-Type") String applcationJson,
            @Header("Trackingmore-Api-Key") String trackingKey,
            @Body TrackDeliveryInputModel trackDeliveryInputModel,
            @Url String url
    );


    //Smart Pay New API

    @Multipart
    @POST(Constants.ADD_MONEY_TO_WALLET)
    Call<IsSellerExist> addMoneyToWallet(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part wallet_id,
            @Part MultipartBody.Part amount
    );


    @Multipart
    @POST(Constants.SEND_MONEY_TO_USERS)
    Call<IsSellerExist> sendMoneyToWallet(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part sender_wallet_id,
            @Part MultipartBody.Part phone_no,
            @Part MultipartBody.Part amount
    );


    @Multipart
    @POST(Constants.GET_WALLET_TRANSACTIONS)
    Call<GetWalletTransactionsOutput> getWalletTransactions(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part wallet_id

    );


    @Multipart
    @POST(Constants.PLACE_MOBILE_RECHARGE_REQUEST)
    Call<RechargeOutput> placeMobileRechargeRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part mobile,
            @Part MultipartBody.Part amount,
            @Part MultipartBody.Part operatorId,
            @Part MultipartBody.Part user_id,
            @Part MultipartBody.Part wallet_id,
            @Part MultipartBody.Part recharge_type
    );

    @Multipart
    @POST(Constants.CHECK_RECHARGE_STATUS)
    Call<RechargeOutput> checkRechargeStatus(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part orderid
    );

    @Multipart
    @POST(Constants.GET_ALL_RECHARGES_BY_USER)
    Call<RechargeOrderListOutput> getAllUserRecharges(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part user_id
    );


    @GET(Constants.GET_FRONT_GIFT_VOUCHERS)
    Call<GetAllGiftVouchers> getAllGiftVouchers(@Header("X-API-KEY") String xAccessToken);

    @GET(Constants.GET_ALL_GIFT_VOUCHER_CATEGORIES)
    Call<GiftVoucherCategories> getAllGiftVoucherCategories(@Header("X-API-KEY") String xAccessToken);

    @GET(Constants.GET_ALL_GIFT_VOUCHERS)
    Call<GetAllGiftVouchers> getAllVouchers(@Header("X-API-KEY") String xAccessToken);


    @Multipart
    @POST(Constants.GET_GIFT_VOUCHER_BY_CATEGORY)
    Call<GetAllGiftVouchers> getGiftVoucherByCategory(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part category_id
    );


    @Multipart
    @POST(Constants.BUY_GIFT_VOUCHER)
    Call<RechargeOutput> buyGiftVoucherRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part amount,
            @Part MultipartBody.Part operator_code,
            @Part MultipartBody.Part senderName,
            @Part MultipartBody.Part receiverName,
            @Part MultipartBody.Part receiverEmail,
            @Part MultipartBody.Part senderMobile,
            @Part MultipartBody.Part receiverMobile,
            @Part MultipartBody.Part giftMessage,
            @Part MultipartBody.Part wallet_id,
            @Part MultipartBody.Part customer_id

    );

    @Multipart
    @POST(Constants.GET_ALL_GIFT_VOUCHER_ORDERS)
    Call<GiftVoucherOrders> getAllGiftVoucherOrders(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id

    );


    @Multipart
    @POST(Constants.GET_OPERATORS)
    Call<GetAllOperators> getAllOperators(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part operator_type
    );


    @Multipart
    @POST(Constants.CREATE_NEW_SUPPORT_TICKET)
    Call<IsSellerExist> createSupportTicketWithAttachment(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part subject,
            @Part MultipartBody.Part customer_id,
            @Part MultipartBody.Part customer_name,
            @Part MultipartBody.Part message_text,
            @Part MultipartBody.Part message_attachment
    );


    @Multipart
    @POST(Constants.CREATE_NEW_SUPPORT_TICKET)
    Call<IsSellerExist> createSupportTicketWithoutAttachment(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part subject,
            @Part MultipartBody.Part customer_id,
            @Part MultipartBody.Part customer_name,
            @Part MultipartBody.Part message_text
    );

    @Multipart
    @POST(Constants.GET_ALL_SUPPORT_TICKETS)
    Call<SupportTicketListOutput> getAllSupportTickets(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id
    );

    @Multipart
    @POST(Constants.SINGLE_TICKET_DETAIL)
    Call<SupportTicketMessagesList> getAllSupportTickeDetail(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part ticket_id
    );

    @Multipart
    @POST(Constants.UPDATE_SUPPORT_TICKET)
    Call<IsSellerExist> sendSupportTicketSendMessage(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id,
            @Part MultipartBody.Part customer_name,
            @Part MultipartBody.Part message_text,
            @Part MultipartBody.Part ticket_id

    );


    //Money Transfer API

    @Multipart
    @POST(Constants.API_GET_CUSTOMER_DETAILS_API)
    Call<MoneyTransferCustomerDetails> getCustomerDetailsRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customerMobile
    );


    @Multipart
    @POST(Constants.API_CUSTOMER_REGISTRATION)
    Call<MoneyTranferCustomerReg> moneyTransferCustomerRegistration(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customerMobile,
            @Part MultipartBody.Part customerName,
            @Part MultipartBody.Part customerPincode

    );


    @Multipart
    @POST(Constants.API_VERIFY_CUSTOMER)
    Call<MoneyTranferCustomerReg> moneyTransferVerifyCustomer(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customerMobile,
            @Part MultipartBody.Part otp

    );

    @Multipart
    @POST(Constants.API_ADD_BENEFICIARY)
    Call<AddBeneficiaryOutput> moneyTransferAddBeneficiary(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customerMobile,
            @Part MultipartBody.Part beneficiaryName,
            @Part MultipartBody.Part beneficiaryMobileNumber,
            @Part MultipartBody.Part beneficiaryAccountNumber,
            @Part MultipartBody.Part ifscCode

    );


    @Multipart
    @POST(Constants.API_VERIFY_BENEFICIARY_BY_OTP)
    Call<AddBeneficiaryOutput> moneyTransferVerifyBeneficiaryOtp(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customerMobile,
            @Part MultipartBody.Part otp,
            @Part MultipartBody.Part beneficiaryId

    );


    @Multipart
    @POST(Constants.API_DELETE_BENEFICIARY)
    Call<AddBeneficiaryOutput> moneyTransferDeleteBeneficiary(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customerMobile,
            @Part MultipartBody.Part beneficiaryId

    );


    @Multipart
    @POST(Constants.API_VERIFY_DELETE_BENEFICIARY)
    Call<AddBeneficiaryOutput> moneyTransferVerifyDeleteBeneficiary(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customerMobile,
            @Part MultipartBody.Part otp,
            @Part MultipartBody.Part beneficiaryId

    );


    @Multipart
    @POST(Constants.API_VALIDATE_BENEFICIARY)
    Call<MoneyTransferBeneficiaryValidation> moneyTransferBeneficiaryValidation(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customerMobile,
            @Part MultipartBody.Part beneficiaryAccountNumber,
            @Part MultipartBody.Part ifscCode

    );


    @Multipart
    @POST(Constants.API_TRANSFER_MONEY_TO_BANK_ACCOUNT)
    Call<MoneyTransferToBankAccount> moneyTransferToBankAccount(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customerMobile,
            @Part MultipartBody.Part beneficiaryId,
            @Part MultipartBody.Part amount,
            @Part MultipartBody.Part wallet_id,
            @Part MultipartBody.Part customer_id

    );

    @Multipart
    @POST(Constants.GET_ALL_BANK_TRANFERS_OF_CUSTOMER)
    Call<AllBankTransfers> getAllBankTransfers(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part customer_id

    );

    @Multipart
    @POST(Constants.ALL_RECHARGE_PLANS_API)
    Call<AllPlans> getAllRechargePlans(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part rechType,
            @Part MultipartBody.Part circleCode,
            @Part MultipartBody.Part opid


    );


    @Multipart
    @POST(Constants.CHECK_IF_WALLET_EXIST)
    Call<CheckWalletOutput> checkIfWalletExist(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part mobile

    );


    @Multipart
    @POST(Constants.ADD_PAYMENT_REQUEST)
    Call<IsSellerExist> addPaymentRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part sender_mobile,
            @Part MultipartBody.Part receiver_mobile,
            @Part MultipartBody.Part requested_amount

    );


    @Multipart
    @POST(Constants.GET_ALL_REQUEST_LIST)
    Call<RequestListOutput> getAllRequestList(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part mobile
    );

    @Multipart
    @POST(Constants.CHECK_PAYMENT_REQUEST_STATUS)
    Call<IsSellerExist> checkPaymentRequestStatus(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part request_id
    );


    @Multipart
    @POST(Constants.ACCEPT_PAYMENT_REQUEST)
    Call<IsSellerExist> acceptPaymentRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part request_id
    );

    @Multipart
    @POST(Constants.REJECT_PAYMENT_REQUEST)
    Call<IsSellerExist> rejectPaymentRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part request_id
    );


    @Multipart
    @POST(Constants.GENERATE_PAYTM_CHECKSUM)
    Call<GenerateChecksumOutput> generatePaytmChecksumRequest(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part MID,
            @Part MultipartBody.Part ORDER_ID,
            @Part MultipartBody.Part CUST_ID,
            @Part MultipartBody.Part CHANNEL_ID,
            @Part MultipartBody.Part TXN_AMOUNT,
            @Part MultipartBody.Part WEBSITE,
            @Part MultipartBody.Part CALLBACK_URL,
            @Part MultipartBody.Part INDUSTRY_TYPE_ID

    );

    @Multipart
    @POST(Constants.PAYTM_ADD_MONEY_ORDER)
    Call<ProductOrderNewOutputModel> paytmAddMoneyOrder(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part user_id,
            @Part MultipartBody.Part wallet_id,
            @Part MultipartBody.Part amount
    );


    @Multipart
    @POST(Constants.GET_ALL_BLOG_POSTS)
    Call<InboxBlogPost> getAllBlogPosts(
            @Header("X-API-KEY") String xAccessToken,
            @Part MultipartBody.Part user_id
    );

}
